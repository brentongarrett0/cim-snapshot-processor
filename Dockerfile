FROM openjdk:8-jdk-alpine
LABEL application="CIM Snapshot Processor" maintainer="bgarrett@gci.com" version="1.0.0"
EXPOSE 8080
WORKDIR /app
COPY config config
ARG JAR_FILE=build/libs/cim-snapshot-processor.jar
COPY ${JAR_FILE} /app/cim-snapshot-processor.jar
RUN addgroup -g 1001 -S cim_snapshot_processor_app_group \
&& adduser -u 1001 -S cim_snapshot_processor_app_user -G cim_snapshot_processor_app_group
USER cim_snapshot_processor_app_user
ENTRYPOINT ["java","-jar","/app/cim-snapshot-processor.jar"]
CMD ["--spring.profiles.active=default"]