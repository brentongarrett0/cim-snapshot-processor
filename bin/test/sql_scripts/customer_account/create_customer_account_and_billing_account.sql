-- Clear Customer Accounts and Billing Accounts
DELETE FROM cim_billing_account WHERE id is NOT NULL;
DELETE FROM cim_customer_account WHERE id IS NOT NULL;

-- Insert record into Customer Accounts table
INSERT INTO cim_customer_account (id, primary_contact_id, customer_account_number, status, name, category, parent_category, created_when, change_timestamp, is_test)
VALUES (
    '1234567',
    '9155603678013761473' ,
    '10636569',
    'Active',
    'Bilbo Baggins',
    'Residential',
    'Residential',
    '2019-11-07 10:14:48-09:00',
    '2019-11-07 10:44:06-09:00',
    1
);

-- Insert record into Billing Accounts table
INSERT INTO cim_billing_account (id, billing_contact_id, customer_account_id, billing_account_number, status, bill_handling_code, auto_payment_status, created_when, is_test)
VALUES (
    '12345',
    '9155603678013761473' ,
    '1234567',
    '67890',
    'Active' ,
    'NO PRINT',
    1,
    '2019-11-07 10:17:46-09:00',
    1
);
