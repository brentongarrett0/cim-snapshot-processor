-- Clear Contacts, Contact Methods, and Contact Roles
DELETE FROM cim_contact WHERE id is NOT NULL;
DELETE FROM cim_contact_method WHERE id IS NOT NULL;
DELETE FROM cim_contact_role WHERE id IS NOT NULL;

-- Insert record into Contact table
INSERT INTO cim_contact (id ,contact_guid, customer_account_id, title, first_name, last_name, status, created_when, change_timestamp, is_test)
VALUES(
    '9155603678013761473',
    '91556036-3688-1376-1562-96babdc6fe69',
    '9155603678013761469',
    'Database Architect',
    'Samwise',
    'Gamgee',
    'Retired',
    '2019-11-07T10:14:49-09:00',
    '2019-11-07 10:14:48-09:00',
    1
);