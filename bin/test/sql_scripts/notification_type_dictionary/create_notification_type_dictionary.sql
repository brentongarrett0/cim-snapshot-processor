-- Clear Notification Type Dictionary table
DELETE FROM cim_notification_type_dictionary WHERE id is NOT NULL;

-- Insert new record in Notification Type Dictionary table
INSERT INTO `cim_notification_type_dictionary` (`id`, `data`, `is_test`, `snapshot`, `change_timestamp`)
VALUES (
    'generated_76828178912',
    null,
    1,
    null,
    '2019-12-23 22:34:27+00:00'
);
