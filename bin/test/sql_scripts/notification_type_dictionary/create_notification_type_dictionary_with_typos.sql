-- Clear Notification Type Dictionary table
DELETE FROM cim_notification_type_dictionary WHERE id is NOT NULL;

-- Insert new record in Notification Type Dictionary table
INSERT INTO `cim_notification_type_dictionary` (`id`, `data`, `is_test`, `snapshot`, `change_timestamp`)
VALUES (
           'generated_76828178912',
           '{"hello":"world"}',
           1,
           '{"goodbye":"world"}',
           '2018-12-23 22:34:27+00:00'
       );
