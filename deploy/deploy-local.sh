#Spin up instance of dockerized Kafka
FILE=dockerized-kafka.yaml
if test -f "$FILE"; then
  echo "**$FILE exists"
  docker-compose -f dockerized-kafka.yaml down
  docker-compose -f dockerized-kafka.yaml pull
  docker-compose -f dockerized-kafka.yaml up -d
  docker container ls
else
  echo "`tput setaf 1`docker-compose file for dockerized Kafka does not exist `tput setaf 7`"
  exit
fi

##Build spring boot project
cd ..
pwd
./gradlew clean build
if [ $? -eq 0 ]; then
    echo "**Spring Boot project compiled"
else
  echo "`tput setaf 1`Spring Boot project failed to compile `tput setaf 7`"
  exit
fi

#build the image
docker build -t cim-snapshot-processor:local .
if [ $? -eq 0 ]; then
  echo "**New image created"
else
  echo "`tput setaf 1`New image could not be created `tput setaf 7`"
  exit
fi

#stop existing container
docker container stop cim-snapshot-processor
if [ $? -eq 0 ]; then
  echo "**Stopped existing container"
fi

#remove existing container
docker container rm cim-snapshot-processor
if [ $? -eq 0 ]; then
  echo "**Removed existing container"
fi

#run the container (set container name, set ports, set environment variables, set image)
docker run --name cim-snapshot-processor -p 8080:8080 --network deploy_default \
--env MYSQL_USERNAME=${MYSQL_USERNAME} \
--env MYSQL_PASSWORD=${MYSQL_PASSWORD} \
--env MYSQL_HOST=${MYSQL_HOST} \
--env MYSQL_PORT=${MYSQL_PORT} \
--env KAFKA_BOOTSTRAP_SERVER=${KAFKA_BOOTSTRAP_SERVER} \
--env KAFKA_BOOTSTRAP_SERVER_PORT=${KAFKA_BOOTSTRAP_SERVER_PORT} cim-snapshot-processor:local --spring.profiles.active=default

if [ $? -eq 0 ]; then
  echo "**Running new container"
  docker container ls
else
  echo "`tput setaf 1`Failed to run new container `tput setaf 7`"
  exit
fi
