#Spin up instance of dockerized Kafka
FILE=dockerized-kafka.yaml
if test -f "$FILE"; then
  echo "**$FILE exists"
  docker-compose -f dockerized-kafka.yaml down
  docker-compose -f dockerized-kafka.yaml pull
  docker-compose -f dockerized-kafka.yaml up -d
  docker container ls
else
  echo "`tput setaf 1`docker-compose file for dockerized Kafka does not exist `tput setaf 7`"
  exit
fi