# CIM Snapshot Processor

I had the privilege of engineering this application to address a need that the business was facing. The business
needed to customer related events in our billing system (i.e. new customer event, billing info change event, etc.), 
publishing the event to a Kafka topic in the form of snapshot, processing the snapshot, and persisting to our internal
database.

## Why The CIM Snapshot Processor Exists

Within GCI, we have had a need to be able to accept customer related events from our managed billing system and persist the 
customer information so that it is freely accessible for GCI to use in any upstream systems. The third-party vendor 
did not allow us to access their data (including database records) so there needed to be a way for GCI to move this data
from one system to the other. I designed the CIM Snapshot Processor as a solution that allowed us to do just that.
With the Snapshot Processor, we are able to receive customer related events in the  form of "snapshots", and pull them
into our environment where we can then persist the JSON object into various MySQL tables.

## Prerequisites

This documentation assumes you are using a linux based operating system (Red Hat, Mac, or Ubuntu). 
Here is what you will need to successfully run and manage this app:

* [Java 8 JDK](https://www.oracle.com/technetwork/java/javase/downloads/index.html) - To run and and build Gradle projects, you will need to install the Java Development Kit (JDK).
* [IntelliJ IDE](https://www.jetbrains.com/idea/download/) - The SDI team's preferred Java IDE is IntelliJ Community Edition.
* [Gradle](https://gradle.org/install/) - You will need Gradle 5.0 or higher. If you are using Mac you can easily install Gradle by running `brew install gradle`.


## Running this app

* To run locally:

        java -jar build/libs/cim-snapshot-processor.jar
        
* To run in e2e:

        java -jar -Dspring.profiles.active=e2e build/libs/cim-snapshot-processor.jar
        
## Running Tests

* To run all tests:

        gradle clean test

* To run tests in a specific package:

        #Run tests only in the com.gci.cim.snapshot.unit.* package
        gradle clean test --tests *unit*

* Pulling 'INSERT' statements from MySQL to prep integration tests

         mysqldump --complete-insert -u username_here -p database_here table_name_here
        
## Loading the Schema

* To load the schema for the CIM Snapshot Processor, do the following:

        mysql -u root -p < ~/Projects/cim-snapshot-processor/CIM.sql

## Using the Application

We are finally ready to experience the power of the CIM Snapshot Processor! The app is essentially a middle-mechanism
between the Kafka topic and the GCI database. In the following section, we will walk through the default behavior, 
along with some other helpful options.

### The CIM Snapshot Processor Endpoints

#### Validate a Snapshot

Submit requests to the following endpoint to validate a snapshot:

* Request

            request:
              method: POST
              url: /snapshot/v1/validate
              body: **snapshot body here**
              headers:
                Content-Type: text/plain
            
* Response:

              status: 200 OK
              body: '{
                         "snapshotType": "Customer Account",
                         "isValid": true,
                         "violations": []
                     }'
              headers:
                Content-Type: application/json;charset=UTF-8

* Invalid Snapshot - if the request body that was sent to the processor was not a valid
snapshot, a response much like the following would be returned:

              status: 200 OK
              body: ' {
                         "snapshotType": "Unparsable",
                         "isValid": false,
                         "violations": [
                             {
                                 "propertyPath": null,
                                 "violationMessage": "Cannot parse snapshot to JSON"
                             }
                         ]
                     }'
              headers:
                Content-Type: application/json;charset=UTF-8
                
#### Submit a Snapshot

Submit requests to the following endpoint to submit a snapshot:

* Request

            request:
              method: POST
              url: /snapshot/v1/submit
              body: **snapshot body here**
              headers:
                Content-Type: text/plain
            
* Response:

              status: 204 NO CONTENT

* NOTE: When a snapshot is invalid, the end use has no way of knowing as the 
only response they would every receive would be a `204 NO CONTENT` status code.
                
#### Kafka Producer Endpoint 

Submit requests to the following endpoint publish a snapshot directly to the kafka topic:

* Request

            request:
              method: POST
              url: /kafka/producer/v1/publish
              body: '**snapshot body here**'
              headers:
                Content-Type: text/plain
            
* Response:

              status: 204 NO CONTENT

* NOTE: This endpoint is handy for testing the communication between the end-user, kafka,
the snapshot processor, and the EIP database. Whenever you need to test all components from
end-to-end, use this endpoint.

#### Exposing Version

Spring Boot Actuator is enabled by default for the CIM Snapshot Processor. Submit a request
to the following endpoint to expose version for the app:

* Request

            request:
              method: GET
              url: /snapshot/admin/v1/version

* Response:

              status: 200 OK
              body: v0.1.0
              headers:
                Content-Type: text/plain;charset=UTF-8

## Gold Standard Internals

In this section, we will take a look at the anatomy of the CIM Snapshot Processor app, workflows, and what you'll need to do to 
properly extend this template when you are ready to modify the app.

### Logging

#### How Logging Works

Logs in CIM Snapshot Processor are by default sent to a file called `log` in the root of the project. By the magic of Spring, 
log rotating is automatic and occurs daily. You can identify rotated logs by thier `.gz` extensions. Have a look at 
the `log` folder and its contents by doing the following:

      cd cim-snapshot-processor/
      cd log
      ls -l

      total 1920
      -rw-r--r--  1 bgarrett  staff  412597 Feb 18 15:30 cim-snapshot-processor.log
      -rw-r--r--  1 bgarrett  staff    5003 Feb 15 12:31 cim-snapshot-processor.log.2019-02-14.0.gz
      -rw-r--r--  1 bgarrett  staff  217950 Feb 16 19:23 cim-snapshot-processor.log.2019-02-15.0.gz
      -rw-r--r--  1 bgarrett  staff  216246 Feb 17 00:03 cim-snapshot-processor.log.2019-02-16.0.gz
      -rw-r--r--  1 bgarrett  staff  121292 Feb 18 12:29 cim-snapshot-processor.log.2019-02-17.0.gz

All logging configuration information is stored in a file called `application.yml` located in the `config` directory of 
the project's root. There, we are able to change the behavior of our logging. Within `config/application.yml` you will 
find a section that looks like this:


        ...
        logging:
          file: log/cim-snapshot-processor.log
          level:
            root: DEBUG
        ...


#### How to Change Logging Level

Here are the options that are available to you for logging level:
* `DEBUG` - As the name implies, this logging level is useful for debugging issues and provides the full amount of output information to your log. NOTE: This log level is resource intensive as it may fill your logs faster than the other levels.
* `INFO` - ...
* `WARN` - ...
* `ERROR` - We typically use this level for servers with limited storage and production environments.

To change logging level, simply navigate to `config/application.yml` and set your logging level at `logging.level.root`:

      cd cim-snapshot-processor/
      cd config
      nano application.yml
      #Make the change to logging.level.root

### Testing

When you run the tests, here is where they end up:

`ProjectRoot/build/test-results/test`
        
## Versioning

I use [SemVer](http://semver.org/) for versioning.

## Authors

* **Brenton Garrett** - *Senior Software Engineer*



