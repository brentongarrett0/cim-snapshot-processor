-- Clear Notification Profiles
DELETE FROM cim_notification_profile WHERE id IS NOT NULL;

-- Insert new Notification Profile
INSERT INTO `cim_notification_profile` (
    `id`,
    `contact_id`,
    `notification_type_id`,
    `email_channel`,
    `mobile_channel`,
    `created_when`,
    `snapshot`,
    `change_timestamp`,
    `is_test`
)
VALUES (
    'generated_97778481474',
    'laces_out!',
    'test_only',
    1,
    1,
    '2019-12-23 22:33:32+00:00',
    NULL,
    '2019-12-23 22:33:32+00:00',
    1
);

