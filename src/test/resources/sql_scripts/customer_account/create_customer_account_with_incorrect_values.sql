-- Clear Customer Accounts and Billing Accounts
DELETE FROM cim_billing_account WHERE id is NOT NULL;
DELETE FROM cim_customer_account WHERE id IS NOT NULL;

-- Insert record into Customer Accounts table
INSERT INTO cim_customer_account (id, primary_contact_id, customer_account_number, status, name, category, parent_category, created_when, change_timestamp, is_test)
VALUES (
    '1234567',
    '9155603678013761473' ,
    '10636569',
    'Active',
    'Samewise Gamgee',
    'Business',
    'Residential',
    '2019-11-07 10:14:48-09:00',
    '2019-11-07 13:44:06.0',
    1
);