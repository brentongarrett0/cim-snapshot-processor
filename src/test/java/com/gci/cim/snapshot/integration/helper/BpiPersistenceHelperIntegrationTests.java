package com.gci.cim.snapshot.integration.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.bpi.Bpi;
import com.gci.cim.snapshot.entity.bpi.BpiSnapshotData;
import com.gci.cim.snapshot.helper.BpiPersistenceHelper;
import com.gci.cim.snapshot.repository.CIMBpiRepository;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@ActiveProfiles("test")
@SpringBootTest
public class BpiPersistenceHelperIntegrationTests {

    private static Bpi expectedBpi;
    private Bpi bpi;

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CIMBpiRepository cimBpiRepository;

    @Autowired
    BpiPersistenceHelper bpiPersistenceHelper;

    @BeforeEach
    public void setUp() throws Exception {

        expectedBpi =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/bpi.json"), Bpi.class);

        bpi =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/bpi.json"), Bpi.class);
    }

    //##VALIDATION
    @Test //null checking
    @Sql("/sql_scripts/bpi/create_bpi_with_child_bpi_and_grandchild_bpi.sql")
    public void persistBpiSnapshot_doesNothing_whenBpiInputIsNull() throws JsonProcessingException, ParseException {

        //save null bpi
        bpiPersistenceHelper.persistBpiSnapshot(null);

        //verify bpi table consists of only 3 records
        assertTrue(cimBpiRepository.count() == 3,
                "Expected bpi table to contain exactly 3 records, but it has some other value.");

        //verify bpi still exists by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());
        assertTrue(bpiExists, "Expected bpi to exist, but it does not.");

        //PERFORM TESTS
        BpiSnapshotData existingBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).get();

        assertAll("Saved BPI",
            () -> assertEquals(expectedBpi.getSnapshotData().getId(), existingBpi.getId(),
            "Expected id to be '"+expectedBpi.getSnapshotData().getId()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), existingBpi.getCustomerAccountId(),
                    "Expected id to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value."),

            () -> assertEquals(null, existingBpi.getBpiSnapshotData(),
                    "Expected bpiSnapshotData to be 'null', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getName(), existingBpi.getName(),
                    "Expected name to be '"+expectedBpi.getSnapshotData().getName()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getStatus(), existingBpi.getStatus(),
                    "Expected status to be '"+expectedBpi.getSnapshotData().getStatus()+"', but was some other value.") ,

            () -> assertEquals(expectedBpi.getSnapshotData().getOfferingId(), existingBpi.getOfferingId(),
                    "Expected offeringId to be '"+expectedBpi.getSnapshotData().getOfferingId()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getOfferingName(), existingBpi.getOfferingName(),
                    "Expected offeringName to be '"+expectedBpi.getSnapshotData().getOfferingName()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getProductCategory(), existingBpi.getProductCategory(),
                    "Expected productCategory to be '"+expectedBpi.getSnapshotData().getProductCategory()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getMarketId(), existingBpi.getMarketId(),
            "Expected marketId to be '"+expectedBpi.getSnapshotData().getMarketId()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getMarketName(), existingBpi.getMarketName(),
                    "Expected marketName to be '"+expectedBpi.getSnapshotData().getMarketName()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getCreatedWhen(), existingBpi.getCreatedWhen(),
                    "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getCreatedWhen()+"', but was some other value.") ,

            () -> assertEquals(expectedBpi.getSnapshotData().getBillingAccountId(), existingBpi.getBillingAccountId(),
                    "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getBillingAccountId()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getServiceName(), existingBpi.getServiceName(),
                    "Expected serviceName to be '"+expectedBpi.getSnapshotData().getServiceName()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getSandvineId(), existingBpi.getSandvineId(),
                    "Expected sandvineId to be '"+expectedBpi.getSnapshotData().getSandvineId()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getSandvinePlanId(), existingBpi.getSandvinePlanId(),
                    "Expected sandvinePlanId to be '"+expectedBpi.getSnapshotData().getSandvinePlanId()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getProductInstance(), existingBpi.getProductInstance(),
                    "Expected productInstance to be '"+expectedBpi.getSnapshotData().getProductInstance()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct(), existingBpi.getSeachangeSubscriptionProduct(),
                    "Expected seachangeSubscriptionProduct to be '"+expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriberId(), existingBpi.getSeachangeSubscriberId(),
                    "Expected seachangeSubscriberId to be '"+expectedBpi.getSnapshotData().getSeachangeSubscriberId()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getIncludedUsageAmountGb(), existingBpi.getIncludedUsageAmountGb(),
                    "Expected includedUsageAmoungGb to be '"+expectedBpi.getSnapshotData().getIncludedUsageAmountGb()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getIsLifeline(), existingBpi.getIsLifeline(),
                    "Expected isLifeline to be '"+expectedBpi.getSnapshotData().getIsLifeline()+"', but was some other value."),

            () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), existingBpi.getIsTest(),
                    "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value."),

            () -> assertEquals(null, existingBpi.getSnapshot()),

            () -> {
                assertTrue(existingBpi.getChildBpis().size() == 1);

                BpiSnapshotData childBpi = existingBpi.getChildBpis().get(0);
                assertAll("Saved Child BPI",
                    () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), childBpi.getId(),
                            "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getId()+"', but was some other value.") ,

                    () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), childBpi.getCustomerAccountId(),
                            "Expected customerAccountId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value."),

                    () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getName(), childBpi.getName(),
                            "Expected name to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getName()+"', but was some other value."),

                    () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus(), childBpi.getStatus(),
                            "Expected status to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus()+"', but was some other value."),

                    () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId(), childBpi.getOfferingId(),
                            "Expected offeringId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId()+"', but was some other value."),

                    () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName(), childBpi.getOfferingName(),
                            "Expected offeringName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName()+"', but was some other value."),

                    () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory(), childBpi.getProductCategory(),
                            "Expected productCategory to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory()+"', but was some other value."),

                    () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId(), childBpi.getMarketId(),
                            "Expected marketId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId()+"', but was some other value."),

                    () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName(), childBpi.getMarketName(),
                            "Expected marketName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName()+"', but was some other value."),

                    () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen(), childBpi.getCreatedWhen(),
                            "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen()+"', but was some other value." ),

                    () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId(), childBpi.getBillingAccountId(),
                            "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId()+"', but was some other value.") ,

                        () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), childBpi.getIsTest(),
                                "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value."),

                    () -> {
                        assertTrue(childBpi.getChildBpis().size() == 1);

                        BpiSnapshotData grandChildBpi = existingBpi.getChildBpis().get(0).getChildBpis().get(0);
                        assertAll("Saved Grand-Child BPI",
                                () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId(), grandChildBpi.getId(),
                                        "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), grandChildBpi.getCustomerAccountId(),
                                        "Expected customerAccountId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName(), grandChildBpi.getName(),
                                        "Expected name to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus(), grandChildBpi.getStatus(),
                                        "Expected status to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId(), grandChildBpi.getOfferingId(),
                                        "Expected offeringId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName(), grandChildBpi.getOfferingName(),
                                        "Expected offeringName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory(), grandChildBpi.getProductCategory(),
                                        "Expected productCategory to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId(), grandChildBpi.getMarketId(),
                                        "Expected marketId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName(), grandChildBpi.getMarketName(),
                                        "Expected marketName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen(), grandChildBpi.getCreatedWhen(),
                                        "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId(), grandChildBpi.getBillingAccountId(),
                                        "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId()+"', but was some other value."),

                                () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), grandChildBpi.getIsTest(),
                                        "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value."),

                                () -> assertTrue(grandChildBpi.getChildBpis().size() == 0)

                        );
                    }
                );
            }
        );
    }

    //##CREATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/bpi/clear_bpis.sql")
    public void persistBpiSnapshot_createsBpiSnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws JsonProcessingException, ParseException {

        //persist bpi
        bpiPersistenceHelper.persistBpiSnapshot(bpi);

        //retrieve bpi
        BpiSnapshotData savedBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).get();

        //verify bpi table consists of only 3 records
        assertTrue(cimBpiRepository.count() == 3,
                "Expected bpi table to contain exactly 3 records, but it has some other value.");

        //verify bpi still exists by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());
        assertTrue(bpiExists, "Expected bpi to exist, but it does not.");

        //PERFORM TESTS
        assertAll("Saved BPI",
                () -> assertEquals(expectedBpi.getSnapshotData().getId(), savedBpi.getId(),
                        "Expected id to be '"+expectedBpi.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getBpiSnapshotData(), null,
                        "Expected bpiSnapshotData to be '"+expectedBpi.getSnapshotData().getBpiSnapshotData()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), savedBpi.getCustomerAccountId(),
                        "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getName(), savedBpi.getName(),
                        "Expected name to be '"+expectedBpi.getSnapshotData().getName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getStatus(), savedBpi.getStatus(),
                        "Expected status to be '"+expectedBpi.getSnapshotData().getStatus()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingId(), savedBpi.getOfferingId(),
                        "Expected offeringId to be '"+expectedBpi.getSnapshotData().getOfferingId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingName(), savedBpi.getOfferingName(),
                        "Expected offeringName to be '"+expectedBpi.getSnapshotData().getOfferingName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductCategory(), savedBpi.getProductCategory(),
                        "Expected productCategory to be '"+expectedBpi.getSnapshotData().getProductCategory()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketId(), savedBpi.getMarketId(),
                        "Expected marketId to be '"+expectedBpi.getSnapshotData().getMarketId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketName(), savedBpi.getMarketName(),
                        "Expected marketName to be '"+expectedBpi.getSnapshotData().getMarketName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getCreatedWhen(), savedBpi.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getBillingAccountId(), savedBpi.getBillingAccountId(),
                        "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getBillingAccountId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getServiceName(), savedBpi.getServiceName(),
                        "Expected serviceName to be '"+expectedBpi.getSnapshotData().getServiceName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvineId(), savedBpi.getSandvineId(),
                        "Expected sandvineId to be '"+expectedBpi.getSnapshotData().getSandvineId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvinePlanId(), savedBpi.getSandvinePlanId(),
                        "Expected sandvinePlanId to be '"+expectedBpi.getSnapshotData().getSandvinePlanId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductInstance(), savedBpi.getProductInstance(),
                        "Expected productInstance to be '"+expectedBpi.getSnapshotData().getProductInstance()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct(), savedBpi.getSeachangeSubscriptionProduct(),
                        "Expected seachangeSubscriptionProduct to be '"+expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriberId(), savedBpi.getSeachangeSubscriberId(),
                        "Expected seachangeSubscriberId to be '"+expectedBpi.getSnapshotData().getSeachangeSubscriberId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIncludedUsageAmountGb(), savedBpi.getIncludedUsageAmountGb(),
                        "Expected includedUsageAmountGb to be '"+expectedBpi.getSnapshotData().getIncludedUsageAmountGb()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getChangeTimestamp(), savedBpi.getChangeTimestamp(),
                        "Expected changeTimestamp to be '"+expectedBpi.getChangeTimestamp()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getIsLifeline(), savedBpi.getIsLifeline(),
                        "Expected isLifeline to be '"+expectedBpi.getSnapshotData().getIsLifeline()+"', but was some other value.") ,

                () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), savedBpi.getIsTest(),
                        "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value."),

                () -> {
                    assertTrue(savedBpi.getChildBpis().size() == 1);

                    BpiSnapshotData childBpi = savedBpi.getChildBpis().get(0);
                    assertAll("Saved Child BPI",
                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), childBpi.getId(),
                                    "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), childBpi.getCustomerAccountId(),
                                    "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getId(), childBpi.getBpiSnapshotData().getId(),
                                    "Expected id to be '"+expectedBpi.getSnapshotData().getId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getName(), childBpi.getName(),
                                    "Expected name to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getName()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus(), childBpi.getStatus(),
                                    "Expected status to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId(), childBpi.getOfferingId(),
                                    "Expected offeringId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName(), childBpi.getOfferingName(),
                                    "Expected offeringName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory(), childBpi.getProductCategory(),
                                    "Expected productCategory to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId(), childBpi.getMarketId(),
                                    "Expected marketId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName(), childBpi.getMarketName(),
                                    "Expected marketName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen(), childBpi.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId(), childBpi.getBillingAccountId(),
                                    "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), childBpi.getIsTest(),
                                    "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(childBpi.getChildBpis().size() == 1);

                                BpiSnapshotData grandChildBpi = savedBpi.getChildBpis().get(0).getChildBpis().get(0);
                                assertAll("Saved Grand-Child BPI",
                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId(), grandChildBpi.getId(),
                                                "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), grandChildBpi.getCustomerAccountId(),
                                                "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), grandChildBpi.getBpiSnapshotData().getId(),
                                                "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName(), grandChildBpi.getName(),
                                                "Expected name to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus(), grandChildBpi.getStatus(),
                                                "Expected status to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId(), grandChildBpi.getOfferingId(),
                                                "Expected offering Id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName(), grandChildBpi.getOfferingName(),
                                                "Expected offeringName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory(), grandChildBpi.getProductCategory(),
                                                "Expected productCategory to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId(), grandChildBpi.getMarketId(),
                                                "Expected marketId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName(), grandChildBpi.getMarketName(),
                                                "Expected marketName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen(), grandChildBpi.getCreatedWhen(),
                                                "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId(), grandChildBpi.getBillingAccountId(),
                                                "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), grandChildBpi.getIsTest(),
                                                "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value."),

                                        () -> assertTrue(grandChildBpi.getChildBpis().size() == 0)

                                );
                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/bpi/clear_bpis.sql")
    public void persistBpiSnapshot_createsBpiSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws JsonProcessingException, ParseException {

        //setup bpi
        bpi.setOperationType("Update");
        expectedBpi.setOperationType("Update");

        //persist bpi
        bpiPersistenceHelper.persistBpiSnapshot(bpi);

        //retrieve bpi
        BpiSnapshotData savedBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).get();

        //verify bpi table consists of only 3 records
        assertTrue(cimBpiRepository.count() == 3,
                "Expected bpi table to contain exactly 3 records, but it has some other value.");

        //verify bpi still exists by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());
        assertTrue(bpiExists, "Expected bpi to exist, but it does not.");

        //PERFORM TESTS
        assertAll("Saved BPI",
                () -> assertEquals(expectedBpi.getSnapshotData().getId(), savedBpi.getId(),
                        "Expected id to be '"+expectedBpi.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getBpiSnapshotData(), null,
                        "Expected bpiSnapshotData to be '"+expectedBpi.getSnapshotData().getBpiSnapshotData()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), savedBpi.getCustomerAccountId(),
                        "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getName(), savedBpi.getName(),
               "Expected name to be '"+expectedBpi.getSnapshotData().getName()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getStatus(), savedBpi.getStatus(),
                        "Expected status to be '"+expectedBpi.getSnapshotData().getStatus()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingId(), savedBpi.getOfferingId(),
                        "Expected offeringId to be '"+expectedBpi.getSnapshotData().getOfferingId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingName(), savedBpi.getOfferingName(),
                        "Expected offeringName to be '"+expectedBpi.getSnapshotData().getOfferingName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductCategory(), savedBpi.getProductCategory(),
                        "Expected productCategory to be '"+expectedBpi.getSnapshotData().getProductCategory()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketId(), savedBpi.getMarketId(),
                        "Expected marketId to be '"+expectedBpi.getSnapshotData().getMarketId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketName(), savedBpi.getMarketName(),
                        "Expected marketName to be '"+expectedBpi.getSnapshotData().getMarketName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getCreatedWhen(), savedBpi.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getBillingAccountId(), savedBpi.getBillingAccountId(),
                        "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getBillingAccountId()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getServiceName(), savedBpi.getServiceName(),
                        "Expected serviceName to be '"+expectedBpi.getSnapshotData().getServiceName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvineId(), savedBpi.getSandvineId(),
                        "Expected sandvineId to be '"+expectedBpi.getSnapshotData().getSandvineId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvinePlanId(), savedBpi.getSandvinePlanId(),
                        "Expected sandvindePlanId to be '"+expectedBpi.getSnapshotData().getSandvinePlanId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductInstance(), savedBpi.getProductInstance(),
                        "Expected productInstance to be '"+expectedBpi.getSnapshotData().getProductInstance()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct(), savedBpi.getSeachangeSubscriptionProduct(),
                        "Expected seachangeSubscriptionProduct to be '"+expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriberId(), savedBpi.getSeachangeSubscriberId(),
                        "Expected seachagneSubscriberId to be '"+expectedBpi.getSnapshotData().getSeachangeSubscriberId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIncludedUsageAmountGb(), savedBpi.getIncludedUsageAmountGb(),
                        "Expected includedUsageAmountGb to be '"+expectedBpi.getSnapshotData().getIncludedUsageAmountGb()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getChangeTimestamp(), savedBpi.getChangeTimestamp(),
                        "Expected changeTimestamp to be '"+expectedBpi.getChangeTimestamp()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIsLifeline(), savedBpi.getIsLifeline(),
                        "Expected isLifeline to be '"+expectedBpi.getSnapshotData().getIsLifeline()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), savedBpi.getIsTest(),
                        "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value." ),

                () -> {
                    assertTrue(savedBpi.getChildBpis().size() == 1);

                    BpiSnapshotData childBpi = savedBpi.getChildBpis().get(0);
                    assertAll("Saved Child BPI",
                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), childBpi.getId(),
                                    "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), childBpi.getCustomerAccountId(),
                                    "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getId(), childBpi.getBpiSnapshotData().getId(),
                                    "Expected id to be '"+expectedBpi.getSnapshotData().getId()+"', but was some other value."  ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getName(), childBpi.getName(),
                                    "Expected name to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getName()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus(), childBpi.getStatus(),
                                    "Expected status to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus()+"', but was some other value."  ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId(), childBpi.getOfferingId(),
                                    "Expected offeringId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId()+"', but was some other value."  ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName(), childBpi.getOfferingName(),
                                    "Expected offeringName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory(), childBpi.getProductCategory(),
                                    "Expected productCategory to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory()+"', but was some other value."  ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId(), childBpi.getMarketId(),
                                    "Expected marketId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName(), childBpi.getMarketName(),
                                    "Expected marketName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName()+"', but was some other value."  ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen(), childBpi.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId(), childBpi.getBillingAccountId(),
                                    "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), childBpi.getIsTest(),
                                    "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value." ),

                            () -> {
                                assertTrue(childBpi.getChildBpis().size() == 1);

                                BpiSnapshotData grandChildBpi = savedBpi.getChildBpis().get(0).getChildBpis().get(0);
                                assertAll("Saved Grand-Child BPI",
                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId(), grandChildBpi.getId(),
                                                "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), grandChildBpi.getCustomerAccountId(),
                                                "Expected customerAccountId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), grandChildBpi.getBpiSnapshotData().getId(),
                                                "Expected ### to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getId()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName(), grandChildBpi.getName(),
                                                "Expected name to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus(), grandChildBpi.getStatus(),
                                                "Expected status to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId(), grandChildBpi.getOfferingId(),
                                                "Expected offeringId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName(), grandChildBpi.getOfferingName(),
                                                "Expected ### to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory(), grandChildBpi.getProductCategory(),
                                                "Expected productCategory to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId(), grandChildBpi.getMarketId(),
                                                "Expected marketId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName(), grandChildBpi.getMarketName(),
                                                "Expected marketName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen(), grandChildBpi.getCreatedWhen(),
                                                "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId(), grandChildBpi.getBillingAccountId(),
                                                "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), grandChildBpi.getIsTest(),
                                                "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value." ),

                                        () -> assertTrue(grandChildBpi.getChildBpis().size() == 0)
                                );
                            }
                    );
                }
        );

    }

    //##UPDATE OPERATION
    @Test
    @Sql("/sql_scripts/bpi/create_bpi_with_child_bpi_and_grandchild_bpi_with_typos.sql")
    public void persistBpiSnapshot_updatesBpiSnapshot_whenOperationTypeIsUpdateAndSnapshotExists () throws IOException, ParseException {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(bpi.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);
        bpi.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //setup bpi
        bpi.setOperationType("Update");
        expectedBpi.setOperationType("Update");
        expectedBpi.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //persist bpi
        bpiPersistenceHelper.persistBpiSnapshot(bpi);

        //retrieve bpi
        BpiSnapshotData updatedBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).get();

        //verify bpi table consists of only 3 records
        assertTrue(cimBpiRepository.count() == 3,
                "Expected bpi table to contain exactly 3 records, but it has some other value.");

        //verify bpi still exists by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());
        assertTrue(bpiExists, "Expected bpi to exist, but it does not.");

        //PERFORM TESTS
        assertAll("Updated BPI",
                () -> assertEquals(expectedBpi.getSnapshotData().getId(), updatedBpi.getId(),
                "Expected id to be '"+expectedBpi.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getBpiSnapshotData(), null,
                        "Expected bpiSnapshotData to be '"+expectedBpi.getSnapshotData().getBpiSnapshotData()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), updatedBpi.getCustomerAccountId(),
                        "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getName(), updatedBpi.getName(),
                        "Expected name to be '"+expectedBpi.getSnapshotData().getName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getStatus(), updatedBpi.getStatus(),
                        "Expected status to be '"+expectedBpi.getSnapshotData().getStatus()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingId(), updatedBpi.getOfferingId(),
                        "Expected offeringId to be '"+expectedBpi.getSnapshotData().getOfferingId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingName(), updatedBpi.getOfferingName(),
                        "Expected offeringName to be '"+expectedBpi.getSnapshotData().getOfferingName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductCategory(), updatedBpi.getProductCategory(),
                        "Expected productCategory to be '"+expectedBpi.getSnapshotData().getProductCategory()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketId(), updatedBpi.getMarketId(),
                        "Expected marketId to be '"+expectedBpi.getSnapshotData().getMarketId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketName(), updatedBpi.getMarketName(),
                        "Expected marketName to be '"+expectedBpi.getSnapshotData().getMarketName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getCreatedWhen(), updatedBpi.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getCreatedWhen()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getBillingAccountId(), updatedBpi.getBillingAccountId(),
                        "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getBillingAccountId()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getServiceName(), updatedBpi.getServiceName(),
                        "Expected serviceName to be '"+expectedBpi.getSnapshotData().getServiceName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvineId(), updatedBpi.getSandvineId(),
                        "Expected sandvineId to be '"+expectedBpi.getSnapshotData().getSandvineId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvinePlanId(), updatedBpi.getSandvinePlanId(),
                        "Expected sandvinePlanId to be '"+expectedBpi.getSnapshotData().getSandvinePlanId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductInstance(), updatedBpi.getProductInstance(),
                        "Expected productInstance to be '"+expectedBpi.getSnapshotData().getProductInstance()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct(), updatedBpi.getSeachangeSubscriptionProduct(),
                        "Expected seachangeSubscriptionProduct to be '"+expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriberId(), updatedBpi.getSeachangeSubscriberId(),
                        "Expected seachangeSubscriptionId to be '"+expectedBpi.getSnapshotData().getSeachangeSubscriberId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIncludedUsageAmountGb(), updatedBpi.getIncludedUsageAmountGb(),
                        "Expected includedUsageAmountGb to be '"+expectedBpi.getSnapshotData().getIncludedUsageAmountGb()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getChangeTimestamp(), updatedBpi.getChangeTimestamp(),
                        "Expected changeTimestamp to be '"+expectedBpi.getChangeTimestamp()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIsLifeline(), updatedBpi.getIsLifeline(),
                        "Expected isLifeline to be '"+expectedBpi.getSnapshotData().getIsLifeline()+"', but was some other value.") ,

                () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), updatedBpi.getIsTest(),
                        "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value."),

                () -> {
                    assertTrue(updatedBpi.getChildBpis().size() == 1);

                    BpiSnapshotData childBpi = updatedBpi.getChildBpis().get(0);
                    assertAll("Updated Child BPI",
                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), childBpi.getId(),
                                    "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), childBpi.getCustomerAccountId(),
                                    "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getId(), childBpi.getBpiSnapshotData().getId(),
                                    "Expected id to be '"+expectedBpi.getSnapshotData().getId()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getName(), childBpi.getName(),
                                    "Expected name to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getName()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus(), childBpi.getStatus(),
                                    "Expected status to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId(), childBpi.getOfferingId(),
                                    "Expected offeringId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName(), childBpi.getOfferingName(),
                                    "Expected ### to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory(), childBpi.getProductCategory(),
                                    "Expected productCategory to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId(), childBpi.getMarketId(),
                                    "Expected marketId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName(), childBpi.getMarketName(),
                                    "Expected marketName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen(), childBpi.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId(), childBpi.getBillingAccountId(),
                                    "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId()+"', but was some other value."),

                            () -> {
                                assertTrue(childBpi.getChildBpis().size() == 1);

                                BpiSnapshotData grandChildBpi = updatedBpi.getChildBpis().get(0).getChildBpis().get(0);
                                assertAll("Saved Grand-Child BPI",
                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId(), grandChildBpi.getId(),
                                                "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), grandChildBpi.getCustomerAccountId(),
                                                "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), grandChildBpi.getBpiSnapshotData().getId(),
                                                "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName(), grandChildBpi.getName(),
                                                "Expected name to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus(), grandChildBpi.getStatus(),
                                                "Expected status to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId(), grandChildBpi.getOfferingId(),
                                                "Expected offeringId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName(), grandChildBpi.getOfferingName(),
                                                "Expected offeringName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory(), grandChildBpi.getProductCategory(),
                                                "Expected productCategory to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId(), grandChildBpi.getMarketId(),
                                                "Expected marketId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName(), grandChildBpi.getMarketName(),
                                                "Expected marketName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen(), grandChildBpi.getCreatedWhen(),
                                                "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId(), grandChildBpi.getBillingAccountId(),
                                                "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId()+"', but was some other value." ),

                                        () -> assertTrue(grandChildBpi.getChildBpis().size() == 0)
                                );
                            }
                    );
                }
        );
    }


    @Test
    @Sql("/sql_scripts/bpi/create_bpi_with_child_bpi_and_grandchild_bpi_with_typos.sql")
    public void persistBpiSnapshot_updatesBpiSnapshot_whenOperationTypeIsCreateAndSnapshotExists () throws JsonProcessingException, ParseException {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(bpi.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);

        //setup bpi
        bpi.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));
        expectedBpi.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //persist bpi
        bpiPersistenceHelper.persistBpiSnapshot(bpi);

        //retrieve bpi
        BpiSnapshotData updatedBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).get();

        //verify bpi table consists of only 3 records
        assertTrue(cimBpiRepository.count() == 3,
                "Expected bpi table to contain exactly 3 records, but it has some other value.");

        //verify bpi still exists by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());
        assertTrue(bpiExists, "Expected bpi to exist, but it does not.");

        //PERFORM TESTS
        assertAll("Updated BPI",
                () -> assertEquals(expectedBpi.getSnapshotData().getId(), updatedBpi.getId(),
                        "Expected id to be '"+expectedBpi.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getBpiSnapshotData(), null,
                        "Expected bpiSnapshotData to be '"+expectedBpi.getSnapshotData().getBpiSnapshotData()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), updatedBpi.getCustomerAccountId(),
                        "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getName(), updatedBpi.getName(),
                        "Expected name to be '"+expectedBpi.getSnapshotData().getName()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getStatus(), updatedBpi.getStatus(),
                        "Expected status to be '"+expectedBpi.getSnapshotData().getStatus()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingId(), updatedBpi.getOfferingId(),
                        "Expected offeringId to be '"+expectedBpi.getSnapshotData().getOfferingId()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingName(), updatedBpi.getOfferingName(),
                        "Expected offeringName to be '"+expectedBpi.getSnapshotData().getOfferingName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductCategory(), updatedBpi.getProductCategory(),
                        "Expected productCategory to be '"+expectedBpi.getSnapshotData().getProductCategory()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketId(), updatedBpi.getMarketId(),
                        "Expected marketId to be '"+expectedBpi.getSnapshotData().getMarketId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketName(), updatedBpi.getMarketName(),
                        "Expected marketName to be '"+expectedBpi.getSnapshotData().getMarketName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getCreatedWhen(), updatedBpi.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getBillingAccountId(), updatedBpi.getBillingAccountId(),
                        "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getBillingAccountId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getServiceName(), updatedBpi.getServiceName(),
                        "Expected serviceName to be '"+expectedBpi.getSnapshotData().getServiceName()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvineId(), updatedBpi.getSandvineId(),
                        "Expected sandvineId to be '"+expectedBpi.getSnapshotData().getSandvineId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvinePlanId(), updatedBpi.getSandvinePlanId(),
                        "Expected sanvinePlanId to be '"+expectedBpi.getSnapshotData().getSandvinePlanId()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductInstance(), updatedBpi.getProductInstance(),
                        "Expected productInstance to be '"+expectedBpi.getSnapshotData().getProductInstance()+"', but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct(), updatedBpi.getSeachangeSubscriptionProduct(),
                        "Expected seachangeSubscriptionProduct to be '"+expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriberId(), updatedBpi.getSeachangeSubscriberId(),
                        "Expected seachangeSubscriberId to be '"+expectedBpi.getSnapshotData().getSeachangeSubscriberId()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIncludedUsageAmountGb(), updatedBpi.getIncludedUsageAmountGb(),
                        "Expected includedUsageAmountGb to be '"+expectedBpi.getSnapshotData().getIncludedUsageAmountGb()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getChangeTimestamp(), updatedBpi.getChangeTimestamp(),
                        "Expected changeTimestamp to be '"+expectedBpi.getChangeTimestamp()+"', but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIsLifeline(), updatedBpi.getIsLifeline(),
                        "Expected isLifeline to be '"+expectedBpi.getSnapshotData().getIsLifeline()+"', but was some other value.") ,

                () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), updatedBpi.getIsTest(),
                        "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"', but was some other value."),

                () -> {
                    assertTrue(updatedBpi.getChildBpis().size() == 1);

                    BpiSnapshotData childBpi = updatedBpi.getChildBpis().get(0);
                    assertAll("Saved Child BPI",
                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), childBpi.getId(),
                                    "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getId()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), childBpi.getCustomerAccountId(),
                                    "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getId(), childBpi.getBpiSnapshotData().getId(),
                                    "Expected id to be '"+expectedBpi.getSnapshotData().getId()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getName(), childBpi.getName(),
                                    "Expected name to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getName()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus(), childBpi.getStatus(),
                                    "Expected status to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId(), childBpi.getOfferingId(),
                                    "Expected offeringId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName(), childBpi.getOfferingName(),
                                    "Expected offeringName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory(), childBpi.getProductCategory(),
                                    "Expected productCategory to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory()+"', but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId(), childBpi.getMarketId(),
                                    "Expected marketId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName(), childBpi.getMarketName(),
                                    "Expected marketName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen(), childBpi.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen()+"', but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId(), childBpi.getBillingAccountId(),
                                    "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId()+"', but was some other value."),

                            () -> {
                                assertTrue(childBpi.getChildBpis().size() == 1);

                                BpiSnapshotData grandChildBpi = updatedBpi.getChildBpis().get(0).getChildBpis().get(0);
                                assertAll("Saved Grand-Child BPI",
                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId(), grandChildBpi.getId(),
                                                "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), grandChildBpi.getCustomerAccountId(),
                                                "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), grandChildBpi.getBpiSnapshotData().getId(),
                                                "Expected id to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName(), grandChildBpi.getName(),
                                                "Expected name to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus(), grandChildBpi.getStatus(),
                                                "Expected status to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId(), grandChildBpi.getOfferingId(),
                                                "Expected offeringId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName(), grandChildBpi.getOfferingName(),
                                                "Expected offeringName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName()+"', but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory(), grandChildBpi.getProductCategory(),
                                                "Expected productCategory to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId(), grandChildBpi.getMarketId(),
                                                "Expected marketId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName(), grandChildBpi.getMarketName(),
                                                "Expected marketName to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen(), grandChildBpi.getCreatedWhen(),
                                                "Expected createdWhen to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen()+"', but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId(), grandChildBpi.getBillingAccountId(),
                                                "Expected billingAccountId to be '"+expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId()+"', but was some other value." ),

                                        () -> assertTrue(grandChildBpi.getChildBpis().size() == 0)

                                );
                            }
                    );
                }
        );

    }


    //##DELETE OPERATION
    @Test
    @Sql("/sql_scripts/bpi/create_bpi_with_child_bpi_and_grandchild_bpi.sql")
    public void persistBpiSnapshot_deletesBpiSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws JsonProcessingException, ParseException {

        //change operation type to "Delete"
        bpi.setOperationType("Delete");

        //Delete customer account
        bpiPersistenceHelper.persistBpiSnapshot(bpi);

        //verify contact does not exist by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(bpiExists,
                "Expected bpi to not exist, but it still does.");

    }

    @Test
    @Sql("/sql_scripts/bpi/clear_bpis.sql")
    public void persistBpiSnapshot_deletesBpiSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExist () throws JsonProcessingException, ParseException {

        //change operation type to "Delete"
        bpi.setOperationType("Delete");

        //Delete bpi
        bpiPersistenceHelper.persistBpiSnapshot(bpi);

        //verify contact does not exist by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(bpiExists,
                "Expected bpi to not exist, but it still does.");

    }

}
