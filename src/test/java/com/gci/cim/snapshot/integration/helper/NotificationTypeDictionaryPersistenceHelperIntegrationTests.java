package com.gci.cim.snapshot.integration.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionary;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionarySnapshotData;
import com.gci.cim.snapshot.helper.NotificationTypeDictionaryPersistenceHelper;
import com.gci.cim.snapshot.repository.CIMNotificationTypeDictionaryRepository;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;


@Transactional
@ActiveProfiles("test")
@SpringBootTest
public class NotificationTypeDictionaryPersistenceHelperIntegrationTests {

    private NotificationTypeDictionary expectedNotificationTypeDictionary;
    private NotificationTypeDictionary notificationTypeDictionary;

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CIMNotificationTypeDictionaryRepository cimNotificationTypeDictionaryRepository;

    @Autowired
    NotificationTypeDictionaryPersistenceHelper notificationTypeDictionaryPersistenceHelper;

    @BeforeEach
    public void setUp() throws Exception {

        expectedNotificationTypeDictionary =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_type_dictionary.json"), NotificationTypeDictionary.class);

        notificationTypeDictionary =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_type_dictionary.json"), NotificationTypeDictionary.class);
    }

    //##VALIDATION
    @Test //null checking
    @Sql("/sql_scripts/notification_type_dictionary/create_notification_type_dictionary.sql")
    public void persistNotificationTypeDictionarySnapshot_doesNothing_whenNotificationTypeDictionaryInputIsNull() throws JsonProcessingException {

        //save notification type dictionary
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(null);

        //verify notification type dictionary table consists of only one record
        assertTrue(cimNotificationTypeDictionaryRepository.count() == 1,
                "Expected notification type dictionary table to contain exactly 1 record, but it has some other value.");

        //verify notification profile still exists by id
        boolean notificationTypeDictionaryExists = cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId());

        assertTrue(notificationTypeDictionaryExists,
                "Expected notification profile to exist, but it does not.");

        //Test existing contact
        NotificationTypeDictionarySnapshotData existingNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).get();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), existingNotificationTypeDictionary.getId(),
                "Expected id to be " + expectedNotificationTypeDictionary.getSnapshotData().getId() + ", but was some other value.") ,

                () -> assertEquals(null, existingNotificationTypeDictionary.getSnapshot(),
                        "Expected snapshot to be null, but was some other value.") ,

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), existingNotificationTypeDictionary.getChangeTimestamp(),
                        "Expected changeTimeStamp to be " + expectedNotificationTypeDictionary.getChangeTimestamp() + ", but was some other value."),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), existingNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be " + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + ", but was some other value.")

        );

    }

    //##CREATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/notification_type_dictionary/clear_notification_type_dictionary.sql")
    public void persistNotificationTypeDictionarySnapshot_createsNotificationTypeDictionary_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws IOException {

        //save notification type dictionary
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //Test saved notification type dictionary
        NotificationTypeDictionarySnapshotData savedNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).get();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), savedNotificationTypeDictionary.getId(),
                        "Expected id to be " + expectedNotificationTypeDictionary.getSnapshotData().getId() + ", but was some other value."),

                () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(savedNotificationTypeDictionary.getData()),
                        "Expected snapshotData to be " + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + ", but was some other value." ),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), savedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), savedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be " + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + ", but was some other value.")
        );

    }

    @Test
    @Sql("/sql_scripts/notification_type_dictionary/clear_notification_type_dictionary.sql")
    public void persistNotificationTypeDictionarySnapshot_createsNotificationTypeDictionary_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws IOException {

        //setup snapshot
        notificationTypeDictionary.setOperationType("Update");
        expectedNotificationTypeDictionary.setOperationType("Update");

        //save notification type dictionary
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //Test saved notification type dictionary
        NotificationTypeDictionarySnapshotData savedNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).get();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), savedNotificationTypeDictionary.getId(),
                        "Expected id to be " + expectedNotificationTypeDictionary.getSnapshotData().getId() + ", but was some other value."),

                () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(savedNotificationTypeDictionary.getData()),
                        "Expected snapshotData to be " + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + ", but was some other value." ),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), savedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), savedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be " + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + ", but was some other value.")

        );

    }


    //##UPDATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/notification_type_dictionary/create_notification_type_dictionary_with_typos.sql")
    public void persistNotificationTypeDictionarySnapshot_updatesNotificationTypeDictionary_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws IOException {

        //setup incoming notification type dictionary
        notificationTypeDictionary.setOperationType("Update");
        expectedNotificationTypeDictionary.setOperationType("Update");

        //persist notification type dictionary
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //Test saved notification type dictionary
        NotificationTypeDictionarySnapshotData updatedNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).get();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), updatedNotificationTypeDictionary.getId(),
                        "Expected id to be " + expectedNotificationTypeDictionary.getSnapshotData().getId() + ", but was some other value.") ,

                () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(updatedNotificationTypeDictionary.getData()),
                        "Expected snapshotData to be " + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + ", but was some other value."),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), updatedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), updatedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be " + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + ", but was some other value.")

        );

    }

    @Test
    @Sql("/sql_scripts/notification_type_dictionary/create_notification_type_dictionary_with_typos.sql")
    public void persistNotificationTypeDictionarySnapshot_updatesNotificationTypeDictionary_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws IOException {

        //persist notification type dictionary
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //Test saved notification type dictionary
        NotificationTypeDictionarySnapshotData updatedNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).get();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), updatedNotificationTypeDictionary.getId(),
                        "Expected id to be " + expectedNotificationTypeDictionary.getSnapshotData().getId() + ", but was some other value." ) ,

                () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(updatedNotificationTypeDictionary.getData()),
                        "Expected snapshotData to be " + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + ", but was some other value." ),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), updatedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), updatedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be " + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + ", but was some other value." )

        );

    }

    //##DELETE OPERATION
    @Test
    @Sql("/sql_scripts/notification_type_dictionary/create_notification_type_dictionary.sql")
    public void persistNotificationTypeDictionarySnapshot_deletesNotificationTypeDictionarySnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        notificationTypeDictionary.setOperationType("Delete");

        //Delete notification type dictionary
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //verify contact does not exist by id
        boolean notificationTypeDictionaryExists = cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(notificationTypeDictionaryExists,
                "Expected notification type dictionary to not exist, but it still does.");

    }

    @Test
    @Sql("/sql_scripts/notification_type_dictionary/create_notification_type_dictionary.sql")
    public void persistNotificationTypeDictionarySnapshot_deletesNotificationTypeDictionarySnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws JsonProcessingException {

       //change operation type to "Delete"
       notificationTypeDictionary.setOperationType("Delete");

       //Delete notification type dictionary
       notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

       //verify contact does not exist by id
       boolean notificationTypeDictionaryExists = cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId());

       //verify customer account does not exist
       assertFalse(notificationTypeDictionaryExists,
               "Expected notification type dictionary to not exist, but it still does.");
    }

}

