package com.gci.cim.snapshot.integration.helper;
import com.gci.cim.snapshot.service.CIMSnapshotAdminService;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CIMSnapshotAdminServiceIntegrationTests {

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    BuildProperties buildProperties;

    @Autowired
    CIMSnapshotAdminService cimSnapshotAdminService;

    @Test
    public void getVersion_getsVersion_whenMethodIsCalled ()  {

        //Get expected value
        String expectedVersion = "v" + buildProperties.getVersion();

        //Get version
        String actualVersion = cimSnapshotAdminService.getVersion();

        //Perform tests
        assertEquals(expectedVersion, actualVersion, "Expected version to be " + expectedVersion + " but was some other value");

    }

}
