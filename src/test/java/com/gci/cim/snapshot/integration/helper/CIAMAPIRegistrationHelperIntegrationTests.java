package com.gci.cim.snapshot.integration.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gci.cim.snapshot.helper.CIAMAPIRegistrationHelper;
import com.gci.sdk.logging.EIPLogger;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

@ActiveProfiles("test")
@SpringBootTest
public class CIAMAPIRegistrationHelperIntegrationTests {

    private WireMockServer wireMockServer;

    private final String REQUEST_URI = "/v2/mygci/invite";

    @Value("${com.gci.cim.wire-mock.port}")
    private int port;

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    CIAMAPIRegistrationHelper ciamapiRegistrationHelper;

    @BeforeEach
    public void setUp() throws Exception {

        //WireMock setup
        wireMockServer = new WireMockServer(options()
                .port(port)
        );
        wireMockServer.start();
    }

    @AfterEach
    public void tearDown() throws Exception{

        //wiremock teardown
        wireMockServer.stop();

    }

    @Test
    public void sendInviteRequest_doesNothing_whenContactIdIsNull() throws JsonProcessingException {

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo(REQUEST_URI))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        //send invitation request
        ciamapiRegistrationHelper.sendInviteRequest(null);

        //verify rest call was never invoked.
        wireMockServer.verify(0, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"12345\" }")));
    }

    @Test
    public void sendInviteRequest_registersMyGCIUser_whenContactIdIsValid() throws JsonProcessingException {

        final String CONTACT_ID = "12345";

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo(REQUEST_URI))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        //send invitation request
        ciamapiRegistrationHelper.sendInviteRequest(CONTACT_ID);

        //verify rest call was invoked 1 time
        wireMockServer.verify(1, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"" + CONTACT_ID + "\" }")));

    }

    }
