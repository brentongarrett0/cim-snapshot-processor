package com.gci.cim.snapshot.integration.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfile;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfileSnapshotData;
import com.gci.cim.snapshot.helper.NotificationProfilePersistenceHelper;
import com.gci.cim.snapshot.repository.CIMNotificationProfileRepository;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;

@Transactional
@ActiveProfiles("test")
@SpringBootTest
public class NotificationProfilePersistenceHelperIntegrationTests {

    private NotificationProfile expectedNotificationProfile;
    private NotificationProfile notificationProfile;

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CIMNotificationProfileRepository cimNotificationProfileRepository;

    @Autowired
    NotificationProfilePersistenceHelper notificationProfilePersistenceHelper;

    @BeforeAll
    public static void setupExpectedValues() throws Exception{

    }

    @BeforeEach
    public void setUp() throws Exception {

        expectedNotificationProfile =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_profile.json"), NotificationProfile.class);

        notificationProfile =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_profile.json"), NotificationProfile.class);
    }

    //##VALIDATION
    @Test //null checking
    @Sql("/sql_scripts/notification_profile/create_notification_profile.sql")
    public void persistNotificationProfileSnapshot_doesNothing_whenNotificationProfileInputIsNull() throws JsonProcessingException {

        //save notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(null);

        //verify notification profile table consists of only one record
        assertTrue(cimNotificationProfileRepository.count() == 1,
                "Expected notification profile table to contain exactly 1 record, but it has some other value.");

        //verify notification profile still exists by id
        boolean notificationProfileExists = cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId());

        assertTrue(notificationProfileExists,
                "Expected notification profile to exist, but it does not.");

        //Test existing contact
        NotificationProfileSnapshotData existingNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).get();

        //perform notification profile tests.
        assertAll("Saved Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), existingNotificationProfile.getId(),
                "Expected id to be '" + expectedNotificationProfile.getSnapshotData().getId()+ "' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), existingNotificationProfile.getContact(),
                        "Expected contact to be '" + expectedNotificationProfile.getSnapshotData().getContact()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), existingNotificationProfile.getNotificationType(),
                        "Expected notificationType to be '" + expectedNotificationProfile.getSnapshotData().getNotificationType()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), existingNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to be '" + expectedNotificationProfile.getSnapshotData().getEmailChannel()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), existingNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to be '" + expectedNotificationProfile.getSnapshotData().getMobileChannel()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), existingNotificationProfile.getCreatedWhen(),
                        "created when"),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), existingNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), existingNotificationProfile.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationProfile.getSnapshotData().getIsTest()+ "' but was some other value")

        );

    }

    //##CREATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/notification_profile/clear_notification_profiles.sql")
    public void persistNotificationProfileSnapshot_createsNotificationProfile_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws IOException {

        //persist notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //retrieve notification profile
        NotificationProfileSnapshotData savedNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).get();

        //perform notification profile tests.
        assertAll("Saved Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), savedNotificationProfile.getId(),
                        "Expected id to be '" + expectedNotificationProfile.getSnapshotData().getId()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), savedNotificationProfile.getContact(),
                        "Expected contact to be '" + expectedNotificationProfile.getSnapshotData().getContact()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), savedNotificationProfile.getNotificationType(),
                        "Expected notificationType to be '" + expectedNotificationProfile.getSnapshotData().getNotificationType()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), savedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to be '" + expectedNotificationProfile.getSnapshotData().getEmailChannel()+ "' but was some other value"  ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), savedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to be '" + expectedNotificationProfile.getSnapshotData().getMobileChannel()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), savedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to be '" + expectedNotificationProfile.getSnapshotData().getCreatedWhen()+ "' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), savedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), savedNotificationProfile.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationProfile.getSnapshotData().getIsTest()+ "' but was some other value")

        );

    }

    @Test
    @Sql("/sql_scripts/notification_profile/clear_notification_profiles.sql")
    public void persistContactSnapshot_createsContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws IOException {

        //setup snapshot
        notificationProfile.setOperationType("Update");
        expectedNotificationProfile.setOperationType("Update");

        //persist notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //retrieve notification profile
        NotificationProfileSnapshotData savedNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).get();

        //perform notification profile tests.
        assertAll("Saved Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), savedNotificationProfile.getId(),
                        "Expected id to be '" + expectedNotificationProfile.getSnapshotData().getId()+ "' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), savedNotificationProfile.getContact(),
                        "Expected contact to be '" + expectedNotificationProfile.getSnapshotData().getContact()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), savedNotificationProfile.getNotificationType(),
                        "Expected notificationType to be '" + expectedNotificationProfile.getSnapshotData().getNotificationType()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), savedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to be '" + expectedNotificationProfile.getSnapshotData().getEmailChannel()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), savedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to be '" + expectedNotificationProfile.getSnapshotData().getMobileChannel()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), savedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to be '" + expectedNotificationProfile.getSnapshotData().getCreatedWhen()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), savedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), savedNotificationProfile.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationProfile.getSnapshotData().getIsTest()+ "' but was some other value")

        );
    }

    //##UPDATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/notification_profile/create_notification_profile_with_typos.sql")
    public void persistNotificationProfileSnapshot_updatesNotificationProfileSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist () throws IOException {

        //setup incoming notification profile
        notificationProfile.setOperationType("Update");
        expectedNotificationProfile.setOperationType("Update");

        //persist notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //retrieve notification profile
        NotificationProfileSnapshotData updatedNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).get();

        //perform notification profile tests.
        assertAll("Updated Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), updatedNotificationProfile.getId(),
                        "Expected id to be '" + expectedNotificationProfile.getSnapshotData().getId()+ "' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), updatedNotificationProfile.getContact(),
                        "Expected contact to be '" + expectedNotificationProfile.getSnapshotData().getContact()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), updatedNotificationProfile.getNotificationType(),
                        "Expected notificationType to be '" + expectedNotificationProfile.getSnapshotData().getNotificationType()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), updatedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to be '" + expectedNotificationProfile.getSnapshotData().getEmailChannel()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), updatedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to be '" + expectedNotificationProfile.getSnapshotData().getMobileChannel()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), updatedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to be '" + expectedNotificationProfile.getSnapshotData().getCreatedWhen()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), updatedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), updatedNotificationProfile.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationProfile.getSnapshotData().getIsTest()+ "' but was some other value")

        );

    }

    @Test
    @Sql("/sql_scripts/notification_profile/create_notification_profile_with_typos.sql")
    public void persistNotificationProfileSnapshot_updatesNotificationProfileSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist () throws IOException {

        //persist notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //retrieve notification profile
        NotificationProfileSnapshotData updatedNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).get();

        //perform notification profile tests.
        assertAll("Updated Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), updatedNotificationProfile.getId(),
                        "Expected id to be '" + expectedNotificationProfile.getSnapshotData().getId()+ "' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), updatedNotificationProfile.getContact(),
                        "Expected contact to be '" + expectedNotificationProfile.getSnapshotData().getContact()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), updatedNotificationProfile.getNotificationType(),
                        "Expected notificationType to be '" + expectedNotificationProfile.getSnapshotData().getNotificationType()+ "' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), updatedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to be '" + expectedNotificationProfile.getSnapshotData().getEmailChannel()+ "' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), updatedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to be '" + expectedNotificationProfile.getSnapshotData().getMobileChannel()+ "' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), updatedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to be '" + expectedNotificationProfile.getSnapshotData().getCreatedWhen()+ "' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), updatedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), updatedNotificationProfile.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationProfile.getSnapshotData().getIsTest()+ "' but was some other value" )

        );
    }

    //##DELETE OPERATION
    @Test
    @Sql("/sql_scripts/notification_profile/create_notification_profile.sql")
    public void persistNotificationProfileSnapshot_deletesNotificationProfileSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        notificationProfile.setOperationType("Delete");

        //Delete customer account
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //verify contact does not exist by id
        boolean notificationProfileExists = cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(notificationProfileExists,
                "Expected notification profile to not exist, but it still does.");

    }

    @Test
    @Sql("/sql_scripts/notification_profile/clear_notification_profiles.sql")
    public void persistNotificationProfileSnapshot_deletesNotificationProfileSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        notificationProfile.setOperationType("Delete");

        //Delete customer account
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //verify contact does not exist by id
        boolean notificationProfileExists = cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(notificationProfileExists,
                "Expected notification profile to not exist, but it still does.");

    }

}
