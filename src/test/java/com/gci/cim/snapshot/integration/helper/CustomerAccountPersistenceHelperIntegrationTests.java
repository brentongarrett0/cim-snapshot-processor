package com.gci.cim.snapshot.integration.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccount;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccountSnapshotData;
import com.gci.cim.snapshot.helper.CustomerAccountPersistenceHelper;
import com.gci.cim.snapshot.repository.CimCustomerAccountRepository;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import javax.transaction.Transactional;
import java.io.File;
import java.sql.Timestamp;
import java.util.Calendar;
import static org.junit.jupiter.api.Assertions.*;

@Transactional
@ActiveProfiles("test")
@SpringBootTest
public class CustomerAccountPersistenceHelperIntegrationTests {

    private CustomerAccount customerAccount;

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CimCustomerAccountRepository cimCustomerAccountRepository;

    @Autowired
    CustomerAccountPersistenceHelper customerAccountPersistenceHelper;

    @BeforeEach
    public void setUp() throws Exception {
        customerAccount = objectMapper.readValue(new File("src/test/resources/fixtures/customer_account.json"), CustomerAccount.class);
    }

    //##VALIDATION
    @Test //null checking
    @Sql("/sql_scripts/customer_account/create_customer_account_and_billing_account.sql")
    public void persistCustomerAccountSnapshot_doesNothing_whenCustomerAccountInputIsNull() throws JsonProcessingException {

        String snapshot = new ObjectMapper().writeValueAsString(customerAccount);

        //save customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(null);

        //verify customer account table consists of only one record
        assertTrue(cimCustomerAccountRepository.count() == 1,
                "Expected customer account table to contain exactly 1 record, but it has some other value.");

        //verify customer account still exists by id
        boolean customerAccountExists = cimCustomerAccountRepository.existsById("1234567");

        assertTrue(customerAccountExists,
                "Expected customer account to exist, but it does not.");

        //Test existing customer account
        CustomerAccountSnapshotData existingCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        assertAll("Existing Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), existingCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getId(), existingCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), existingCustomerAccount.getCustomerAccountNumber(),
                "Expect ### to be " + 1 + ", but was some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), existingCustomerAccount.getStatus(),
                        "Expect status to be '" + customerAccount.getSnapshotData().getStatus() + ", but was some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), existingCustomerAccount.getParentCustomerCategory(),
                        "Expect parentCustomerCategory to be '" + customerAccount.getSnapshotData().getParentCustomerCategory() + "', but was some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), existingCustomerAccount.getPrimaryContact(),
                        "Expect primaryContact to be '" + customerAccount.getSnapshotData().getPrimaryContact() + "', but was some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), existingCustomerAccount.getCustomerCategory(),
                        "Expect customerCategory to be '" + customerAccount.getSnapshotData().getCustomerCategory() + "', but was some other value."),

                () -> assertEquals(customerAccount.getChangeTimestamp(), existingCustomerAccount.getChangeTimestamp(),
                        "Expect changeTimestamp to be '" + customerAccount.getChangeTimestamp() + "', but was some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), existingCustomerAccount.getCreatedWhen(),
                        "Expect createdWhen to be '" + customerAccount.getSnapshotData().getCreatedWhen() + "', but was some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), existingCustomerAccount.getIsTest(),
                        "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value." ),

                () -> {
                    assertTrue(existingCustomerAccount.getBillingAccounts().size() == 1,
                            "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.");

                    //Billing account tests (only tested if above assertion returns true)
                    assertAll("Billing Account Test",
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getId(), existingCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected billing account id to equal '12345', but returned some other value."),

                            //TODO: add messages for the below billing account tests.
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber(), existingCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expect billingAccountNumber to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), existingCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expect contact to be '" + customerAccount.getSnapshotData().getBillingContacts().get(0).getContact() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus(), existingCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expect status to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode(), existingCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expect billHandlingCode to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode() + "', but was some other value."),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus(), existingCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expect autoPaymentStatus to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen(), existingCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expect createdWhen to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), existingCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value." )

                    );
                }
        );
    }

    //## CREATE OPERATION
    @Test //Happy path
    @Sql("/sql_scripts/customer_account/clear_customer_accounts_and_billing_accounts.sql")
    public void persistCustomerAccountSnapshot_createsCustomerAccountSnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist() throws JsonProcessingException {

        //persist customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //retrieve customer account
        CustomerAccountSnapshotData savedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test saved customer account
        assertAll("Existing Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), savedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),
                () -> assertEquals(customerAccount.getSnapshotData().getId(), savedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), savedCustomerAccount.getCustomerAccountNumber(),
                        "Expect customerAccountNumber to be '" + customerAccount.getSnapshotData().getCustomerAccountNumber() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), savedCustomerAccount.getStatus(),
                        "Expect status to be '" + customerAccount.getSnapshotData().getStatus() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), savedCustomerAccount.getParentCustomerCategory(),
                        "Expect parentCustomerCategory to be '" + customerAccount.getSnapshotData().getParentCustomerCategory() + "', but was some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), savedCustomerAccount.getPrimaryContact(),
                        "Expect primaryContact to be '" + customerAccount.getSnapshotData().getPrimaryContact() + "', but was some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), savedCustomerAccount.getCustomerCategory(),
                        "Expect customerCategory to be '" + customerAccount.getSnapshotData().getCustomerCategory() + "', but was some other value."),

                () -> assertEquals(customerAccount.getChangeTimestamp(), savedCustomerAccount.getChangeTimestamp(),
                        "Expect changeTimestamp to be '" +customerAccount.getChangeTimestamp() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), savedCustomerAccount.getCreatedWhen(),
                        "Expect createdWhen to be '" + customerAccount.getSnapshotData().getCreatedWhen() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), savedCustomerAccount.getIsTest(),
                        "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value." ),

                () -> {
                    assertTrue(savedCustomerAccount.getBillingAccounts().size() == 1,
                            "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.");

                    //Billing account tests (only tested if above assertion returns true)
                    assertAll("Billing Account Test",
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getId(), savedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected billing account id to equal '12345', but returned some other value."),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber(), savedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expect billingAccountNumber to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), savedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expect contact to be '" + customerAccount.getSnapshotData().getBillingContacts().get(0).getContact()+ "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus(), savedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expect status to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode(), savedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expect billHandlingCode to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus(), savedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expect autoPaymentStatus to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen(), savedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expect createdWhen to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), savedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value." )

                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/customer_account/clear_customer_accounts_and_billing_accounts.sql")
    public void persistCustomerAccountSnapshot_createsCustomerAccountSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws JsonProcessingException {

        //setup incoming customer account
        customerAccount.setOperationType("Update");

        //persist customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //retrieve customer account
        CustomerAccountSnapshotData savedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test saved customer account
        assertAll("Saved Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), savedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),
                () -> assertEquals(customerAccount.getSnapshotData().getId(), savedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), savedCustomerAccount.getCustomerAccountNumber(),
                        "Expect customerAccountNumber to be '" + customerAccount.getSnapshotData().getCustomerAccountNumber() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), savedCustomerAccount.getStatus(),
                        "Expect status to be '" + customerAccount.getSnapshotData().getStatus() + "', but was some other value."  ),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), savedCustomerAccount.getParentCustomerCategory(),
                        "Expect parentCustomerCategory to be '" + customerAccount.getSnapshotData().getParentCustomerCategory() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), savedCustomerAccount.getPrimaryContact(),
                        "Expect primaryContact to be '" + customerAccount.getSnapshotData().getPrimaryContact() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), savedCustomerAccount.getCustomerCategory(),
                        "Expect customerCategory to be '" + customerAccount.getSnapshotData().getCustomerCategory() + "', but was some other value."  ),

                () -> assertEquals(customerAccount.getChangeTimestamp(), savedCustomerAccount.getChangeTimestamp(),
                        "Expect changeTimestamp to be '" + customerAccount.getChangeTimestamp() + "', but was some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), savedCustomerAccount.getCreatedWhen(),
                        "Expect createdWhen to be '" + customerAccount.getSnapshotData().getCreatedWhen() + "', but was some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), savedCustomerAccount.getIsTest(),
                        "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value." ),

                () -> {
                    assertTrue(savedCustomerAccount.getBillingAccounts().size() == 1,
                            "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.");

                    //Billing account tests (only tested if above assertion returns true)
                    assertAll("Billing Account Test",
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getId(), savedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected billing account id to equal '12345', but returned some other value."),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber(), savedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expect billingAccountNumber to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), savedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expect contact to be '" + customerAccount.getSnapshotData().getBillingContacts().get(0).getContact()+ "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus(), savedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expect status to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode(), savedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expect billHandlingCode to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus(), savedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expect autoPaymentStatus to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen(), savedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expect createdWhen to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), savedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value." )

                    );
                }
        );

    }

    //##UPDATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/customer_account/create_customer_account_with_incorrect_values.sql")
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist () throws JsonProcessingException {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(customerAccount.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);
        customerAccount.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //setup incoming customer account
        customerAccount.setOperationType("Update");

        //persist customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //retrieve customer account
        CustomerAccountSnapshotData updatedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test updated customer account
        assertAll("Existing Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), updatedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getId(), updatedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), updatedCustomerAccount.getCustomerAccountNumber(),
                        "Expect custoemrAccountNumber to be '" + customerAccount.getSnapshotData().getCustomerAccountNumber() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), updatedCustomerAccount.getStatus(),
                        "Expect status to be '" + customerAccount.getSnapshotData().getStatus() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), updatedCustomerAccount.getParentCustomerCategory(),
                        "Expect parentCustomerCategory to be '" + customerAccount.getSnapshotData().getParentCustomerCategory()+ "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), updatedCustomerAccount.getPrimaryContact(),
                        "Expect primaryContact to be '" + customerAccount.getSnapshotData().getPrimaryContact() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), updatedCustomerAccount.getCustomerCategory(),
                        "Expect customerCategory to be '" +customerAccount.getSnapshotData().getCustomerCategory() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getChangeTimestamp(), updatedCustomerAccount.getChangeTimestamp(),
                        "Expect changeTimestamp to be '" + customerAccount.getChangeTimestamp()+ "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), updatedCustomerAccount.getCreatedWhen(),
                        "Expect createdWhen to be '" + customerAccount.getSnapshotData().getCreatedWhen() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getIsTest(),
                        "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value." ),

                () -> {
                    assertTrue(updatedCustomerAccount.getBillingAccounts().size() == 1,
                            "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.");

                    //Billing account tests (only tested if above assertion returns true)
                    assertAll("Billing Account Test",
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getId(), updatedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected billing account id to equal '12345', but returned some other value."),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber(), updatedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expect billingAccountNumber to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber() + "', but was some other value."),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), updatedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expect contact to be '" + customerAccount.getSnapshotData().getBillingContacts().get(0).getContact() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus(), updatedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expect status to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode(), updatedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expect billHandlingCode to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus(), updatedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expect autoPaymentStatus to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen(), updatedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expect createdWhen to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value."   )

                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/customer_account/create_customer_and_billing_accounts_incorrect_values.sql")
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewBillingAccountsNull () throws JsonProcessingException {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(customerAccount.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);
        customerAccount.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //setup incoming customer account
        customerAccount.setOperationType("Update");
        customerAccount.getSnapshotData().setBillingAccounts(null);

        //persist customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //retrieve customer account
        CustomerAccountSnapshotData updatedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test saved customer account
        assertAll("Saved Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), updatedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getId(),updatedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), updatedCustomerAccount.getCustomerAccountNumber(),
                        "Expect customerAccountNumber to be '" + customerAccount.getSnapshotData().getCustomerAccountNumber() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), updatedCustomerAccount.getStatus(),
                        "Expect status to be '" + customerAccount.getSnapshotData().getStatus() + "', but was some other value."  ),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), updatedCustomerAccount.getParentCustomerCategory(),
                        "Expect parentCustomerCategory to be '" + customerAccount.getSnapshotData().getParentCustomerCategory() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), updatedCustomerAccount.getPrimaryContact(),
                        "Expect primaryContact to be '" + customerAccount.getSnapshotData().getPrimaryContact() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), updatedCustomerAccount.getCustomerCategory(),
                        "Expect customerCategory to be '" +customerAccount.getSnapshotData().getCustomerCategory() + "', but was some other value."  ),

                () -> assertEquals(customerAccount.getChangeTimestamp(), updatedCustomerAccount.getChangeTimestamp(),
                        "Expect changeTimestamp to be '" + customerAccount.getChangeTimestamp() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), updatedCustomerAccount.getCreatedWhen(),
                        "Expect createdWhen to be '" + customerAccount.getSnapshotData().getCreatedWhen() + "', but was some other value."  ),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getIsTest(),
                        "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value."  ),

                () -> assertTrue(updatedCustomerAccount.getBillingAccounts().size() == 0,
                            "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.")
        );
    }

    @Test
    @Sql("/sql_scripts/customer_account/create_customer_account_with_incorrect_values.sql")
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist () throws JsonProcessingException {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(customerAccount.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);
        customerAccount.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //persist customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //retrieve customer account
        CustomerAccountSnapshotData updatedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test updated customer account
        assertAll("Existing Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), updatedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getId(), updatedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), updatedCustomerAccount.getCustomerAccountNumber(),
                        "Expect customerAccountNumber to be '" + customerAccount.getSnapshotData().getCustomerAccountNumber() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), updatedCustomerAccount.getStatus(),
                        "Expect status to be '" + customerAccount.getSnapshotData().getStatus() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), updatedCustomerAccount.getParentCustomerCategory(),
                        "Expect customerCategory to be '" + customerAccount.getSnapshotData().getParentCustomerCategory() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), updatedCustomerAccount.getPrimaryContact(),
                        "Expect primaryContact to be '" + customerAccount.getSnapshotData().getPrimaryContact() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), updatedCustomerAccount.getCustomerCategory(),
                        "Expect customerCategory to be '" + customerAccount.getSnapshotData().getCustomerCategory() + "', but was some other value."  ),

                () -> assertEquals(customerAccount.getChangeTimestamp(), updatedCustomerAccount.getChangeTimestamp(),
                        "Expect changeTimestamp to be '" + customerAccount.getChangeTimestamp()+ "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), updatedCustomerAccount.getCreatedWhen(),
                        "Expect createdWhen to be '" + customerAccount.getSnapshotData().getCreatedWhen() + "', but was some other value."  ),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getIsTest(),
                        "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value."  ),

                () -> {
                    assertTrue(updatedCustomerAccount.getBillingAccounts().size() == 1,
                            "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.");

                    //Billing account tests (only tested if above assertion returns true)
                    assertAll("Billing Account Test",
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getId(), updatedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected billing account id to equal '12345', but returned some other value."),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber(), updatedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expect billingAccountNumber to be '" + 1 + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), updatedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expect contact to be '" + customerAccount.getSnapshotData().getBillingContacts().get(0).getContact() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus(), updatedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expect status to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode(), updatedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expect billHandlingCode to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus(), updatedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expect autoPaymentStatus to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus() + "', but was some other value." ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen(), updatedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expect createdWhen to be '" + customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen() + "', but was some other value."  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() +  "', but was some other value." )

                    );
                }
        );

    }

    @Test
    @Sql("/sql_scripts/customer_account/create_customer_and_billing_accounts_incorrect_values.sql")
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewBillingAccountsNull () throws JsonProcessingException {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(customerAccount.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);
        customerAccount.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //setup incoming customer account
        customerAccount.getSnapshotData().setBillingAccounts(null);

        //persist customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //retrieve customer account
        CustomerAccountSnapshotData updatedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test saved customer account
        assertAll("Saved Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), updatedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),
                () -> assertEquals(customerAccount.getSnapshotData().getId(),updatedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), updatedCustomerAccount.getCustomerAccountNumber(),
                        "Expect customerAccountNumber to be '" + customerAccount.getSnapshotData().getCustomerAccountNumber() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), updatedCustomerAccount.getStatus(),
                        "Expect status to be '" + customerAccount.getSnapshotData().getStatus() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), updatedCustomerAccount.getParentCustomerCategory(),
                        "Expect parentCustomerCategory to be '" + customerAccount.getSnapshotData().getParentCustomerCategory()+  "', but was some other value."  ),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), updatedCustomerAccount.getPrimaryContact(),
                        "Expect primaryContact to be '" + customerAccount.getSnapshotData().getPrimaryContact() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), updatedCustomerAccount.getCustomerCategory(),
                        "Expect customerCategory to be '" + customerAccount.getSnapshotData().getCustomerCategory() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getChangeTimestamp(), updatedCustomerAccount.getChangeTimestamp(),
                        "Expect changeTimestamp to be '" + customerAccount.getChangeTimestamp() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), updatedCustomerAccount.getCreatedWhen(),
                        "Expect createdWhen to be '" + customerAccount.getSnapshotData().getCreatedWhen() + "', but was some other value." ),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getIsTest(),
                        "Expect isTest to be '" + customerAccount.getSnapshotData().getIsTest() + "', but was some other value." ),

                () -> assertTrue(updatedCustomerAccount.getBillingAccounts().size() == 0,
                        "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.")
        );
    }

    //##DELETE OPERATION
    @Test
    @Sql("/sql_scripts/customer_account/create_customer_account_and_billing_account.sql")
    public void persistCustomerAccountSnapshot_deletesCustomerAccountSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        customerAccount.setOperationType("Delete");

        //Delete customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

       //verify customer account and billing account(s) does not exist by id
        boolean customerAccountExists = cimCustomerAccountRepository.existsById("1234567");

        //verify customer account does not exist
        assertFalse(customerAccountExists,
                "Expected customer account to not exist, but it still does.");

    }

    @Test
    @Sql("/sql_scripts/customer_account/clear_customer_accounts_and_billing_accounts.sql")
    public void persistCustomerAccountSnapshot_deletesCustomerAccountSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        customerAccount.setOperationType("Delete");

        //Delete customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //verify customer account does not exist
        boolean customerAccountExists = cimCustomerAccountRepository.existsById("1234567");

        //verify customer account does not exist
        assertFalse(customerAccountExists,
                "Expected customer account to not exist, but it still does.");

    }

}




