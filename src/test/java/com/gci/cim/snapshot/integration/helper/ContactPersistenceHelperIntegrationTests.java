package com.gci.cim.snapshot.integration.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.contact.Contact;
import com.gci.cim.snapshot.entity.contact.ContactMethod;
import com.gci.cim.snapshot.entity.contact.ContactRole;
import com.gci.cim.snapshot.entity.contact.ContactSnapshotData;
import com.gci.cim.snapshot.helper.CIAMAPIRegistrationHelper;
import com.gci.cim.snapshot.helper.ContactPersistenceHelper;
import com.gci.cim.snapshot.properties.CIMSnapshotCiamApiProperties;
import com.gci.cim.snapshot.repository.CIMContactRepository;
import com.gci.sdk.logging.EIPLogger;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.stubbing.ServeEvent;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.client.RestTemplate;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.*;

@Transactional
@ActiveProfiles("test")
@SpringBootTest
public class ContactPersistenceHelperIntegrationTests {

    private static Contact expectedContact;
    private Contact contact;

    private WireMockServer wireMockServer;

    @Value("${com.gci.cim.wire-mock.port}")
    private int port;

    private final String REQUEST_URI = "/v2/mygci/invite";

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    CIMSnapshotCiamApiProperties cimSnapshotCiamApiProperties;

    @Autowired
    RestTemplate restCall;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CIMContactRepository cimContactRepository;

    @Autowired
    ContactPersistenceHelper contactPersistenceHelper;

    @BeforeAll
    public static void setupExpectedValues() throws Exception{
        expectedContact =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
    }

    @BeforeEach
    public void setUp() throws Exception {
        contact = objectMapper.readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);

        //WireMock setup
        wireMockServer = new WireMockServer(options()
                .port(port)
        );
        wireMockServer.start();

    }

    @AfterEach
    public void tearDown() throws Exception{

        //wiremock teardown
        wireMockServer.stop();
    }

    //##VALIDATION
    @Test //null checking
    @Sql("/sql_scripts/contact/create_contact_with_contact_method_and_contact_role.sql")
    public void persistContactSnapshot_doesNothing_whenContactInputIsNull() throws JsonProcessingException {

        //save customer account
        contactPersistenceHelper.persistContactSnapshot(null);

        System.out.println("**contacts in database: " + cimContactRepository.count());
        //verify contact table consists of only one record
        assertTrue(cimContactRepository.count() == 1,
                "Expected contact table to contain exactly 1 record, but it has some other value.");

        //verify contact still exists by id
        boolean contactExists = cimContactRepository.existsById(contact.getSnapshotData().getId());

        assertTrue(contactExists,
                "Expected contact to exist, but it does not.");

        //Test existing contact
        ContactSnapshotData existingContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), existingContact.getId(),
                "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), existingContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), existingContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), existingContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), existingContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), existingContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), existingContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), existingContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), existingContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                () -> {
                    assertTrue(existingContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod existingEmailContactMethod = existingContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(existingEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), existingEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), existingEmailContactMethod.getType(),
                                    "Expected type to be '"+expectedEmailContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), existingEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), existingEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress()+"', but was some other value.l"),

                            () -> assertEquals((byte) 1, existingEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, existingEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, existingEmailContactMethod.getPreferred(),
                                    "Expected ### to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), existingEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be 'expectedEmailContactMethod.getCreatedWhen()', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), existingContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be 'expectedContact.getSnapshotData().getIsTest()', but was some other value."),

                            () -> {
                                assertTrue(existingContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), existingContact.getContactMethods().get(0).getContactSnapshotData().getId());
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod existingMobileContactMethod = existingContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(existingMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), existingMobileContactMethod.getId(),
                                    "Expected id to be '"+expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), existingMobileContactMethod.getType(),
                                    "Expected type to be '"+expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), existingMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), existingMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, existingMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, existingMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, existingMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), existingMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), existingContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(existingContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), existingContact.getContactMethods().get(0).getContactSnapshotData().getId());
                            }
                    );

                },
                () -> {
                    assertTrue(existingContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), existingContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), existingContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getRole(), existingContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+expectedContactRole.getRole()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), existingContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedContactRole.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), existingContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(existingContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), existingContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );
                }
        );
    }

    //## CREATE OPERATION
    @Test //Happy path
    @Sql("/sql_scripts/contact/clear_contacts_and_contact_methods_and_contact_roles.sql")
    public void persistContactSnapshot_createsContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist() throws JsonProcessingException {

        //persist contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        System.out.println("**contact id: " + contact.getSnapshotData().getContactId());

        //retrieve contact
        ContactSnapshotData savedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), savedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), savedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), savedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), savedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), savedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), savedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), savedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                () -> {
                    assertTrue(savedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod savedEmailContactMethod = savedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(savedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), savedEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), savedEmailContactMethod.getType(),
                                    "Expected type to be '"+expectedEmailContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), savedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), savedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals((byte) 1, savedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, savedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, savedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), savedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedEmailContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactMethods().get(0).getContactSnapshotData().getId());
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod savedMobileContactMethod = savedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(savedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), savedMobileContactMethod.getId(),
                                    "Expected id to be '"+expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), savedMobileContactMethod.getType(),
                                    "Expected type to be '"+expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), savedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), savedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, savedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, savedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, savedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), savedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactMethods().get(0).getContactSnapshotData().getId());
                            }
                    );

                },
                () -> {
                    assertTrue(savedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), savedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), savedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getRole(), savedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+expectedContactRole.getRole()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), savedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedContactRole.getCreatedWhen()+"', but was some other value." ),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value." );
                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/contact/clear_contacts_and_contact_methods_and_contact_roles.sql")
    public void persistContacttSnapshot_createsContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws JsonProcessingException {

        //setup incoming customer account
        contact.setOperationType("Update");

        //persist contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //retrieve contact
        ContactSnapshotData savedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), savedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), savedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), savedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), savedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), savedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), savedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+1+"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), savedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value." ),

                () -> {
                    assertTrue(savedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod savedEmailContactMethod = savedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(savedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), savedEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), savedEmailContactMethod.getType(),
                                    "Expected type to be '"+expectedEmailContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), savedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), savedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals((byte) 1, savedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, savedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, savedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), savedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedEmailContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactMethods().get(0).getContactSnapshotData().getId());
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod savedMobileContactMethod = savedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(savedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), savedMobileContactMethod.getId(),
                                    "Expected id to be '"+expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), savedMobileContactMethod.getType(),
                                    "Expected type to be '"+expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), savedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), savedMobileContactMethod.getEmailAddress(),
                                    "Expected ### to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, savedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, savedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '2', but was some other value."),

                            () -> assertEquals(null, savedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), savedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactMethods().get(0).getContactSnapshotData().getId());
                            }
                    );

                },
                () -> {
                    assertTrue(savedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), savedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), savedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getRole(), savedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+expectedContactRole.getRole()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), savedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedContactRole.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactRoles().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );
                }
        );

    }

    //##UPDATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/contact/create_contact_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleIsMyGciUserAndTriggersCiamApiCall () throws JsonProcessingException {

        //setup incoming customer account
        contact.setOperationType("Update");

        //persist contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //retrieve contact
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //TODO: Add messages for Saved Contact tests.
        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                () -> {
                    assertTrue(updatedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod updatedEmailContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(updatedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), updatedEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), updatedEmailContactMethod.getType(),
                                    "Expected type to be '"+expectedEmailContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), updatedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), updatedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPrimaryEmail(),
                                    "Expected priamryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, updatedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), updatedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedEmailContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId());
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod updatedMobileContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(updatedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), updatedMobileContactMethod.getId(),
                                    "Expected id to be '"+expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), updatedMobileContactMethod.getType(),
                                    "Expected type to be '"+expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), updatedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), updatedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPrimaryEmail(),
                                    "Expected priamryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), updatedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );

                },
                () -> {
                    assertTrue(updatedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), updatedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), updatedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getRole(), updatedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+expectedContactRole.getRole()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), updatedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedContactRole.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value." ),

                            () -> {
                                assertTrue(updatedContact.getContactRoles().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value." );

                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/contact/create_contact_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleNotIsMyGciUserAndDoesNotTriggersCiamApiCall () throws IOException {

        //setup incoming customer account
        contact.setOperationType("Update");

        contact.getSnapshotData().setContactRoles(new ArrayList<ContactRole>(){{
            add(ContactRole.builder()
                    .id("9155603678013761475")
                    .customerAccountId("9155603678013761469")
                    .role("ACS User")
                    .createdWhen(Timestamp.valueOf("2019-11-07 10:14:49"))
                    .build());
        }});

        //persist contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //retrieve contact
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                () -> {
                    assertTrue(updatedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod updatedEmailContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(updatedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), updatedEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), updatedEmailContactMethod.getType(),
                                    "Expected type to be '"+expectedEmailContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), updatedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), updatedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, updatedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), updatedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedEmailContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+1+"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod updatedMobileContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(updatedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), updatedMobileContactMethod.getId(),
                                    "Expected id to be '"+expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), updatedMobileContactMethod.getType(),
                                    "Expected type to be '"+expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), updatedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), updatedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), updatedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value." );
                            }
                    );

                },
                () -> {
                    assertTrue(updatedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), updatedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), updatedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value."),

                            () -> assertEquals("ACS User", updatedContact.getContactRoles().get(0).getRole(),
                                    "Expected ACS User to be 'ACS User', but was some other value."),

                            () -> assertEquals(Timestamp.valueOf("2019-11-07 10:14:49"), updatedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '2019-11-07 10:14:49', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value." ),

                            () -> {
                                assertTrue(updatedContact.getContactRoles().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/contact/create_contact_and_contact_methods_and_contact_role_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewContactMethodsNull_NewContactRolesNull_contactRoleDoesNotExistAndDoesNotTriggersCiamApiCall () throws JsonProcessingException {

        List<LoggedRequest> requests = wireMockServer.findAll(postRequestedFor(urlMatching("/v2/mygci/invite")));
        System.out.println("**request log before: " + requests);

        //setup incoming customer account
        contact.setOperationType("Update");
        contact.getSnapshotData().setContactMethods(null);
        contact.getSnapshotData().setContactRoles(null);

        //persist customer account
        contactPersistenceHelper.persistContactSnapshot(contact);

        List<LoggedRequest> requestsAfter = wireMockServer.findAll(postRequestedFor(urlMatching("/v2/mygci/invite")));
        System.out.println("**request log after: " + requestsAfter);

        //retrieve customer account
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value." ),

                () -> assertTrue(updatedContact.getContactMethods().size() == 0),
                () -> assertTrue(updatedContact.getContactRoles().size() == 0)
        );

    }

    @Test
    @Sql("/sql_scripts/contact/create_contact_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleIsMyGciUserAndTriggersCiamApiCall () throws JsonProcessingException {

        //persist contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //retrieve contact
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value."  ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value." ),

                () -> {
                    assertTrue(updatedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod updatedEmailContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(updatedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), updatedEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), updatedEmailContactMethod.getType(),
                                    "Expected type to be '"+expectedEmailContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), updatedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), updatedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, updatedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), updatedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedEmailContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod updatedMobileContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(updatedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), updatedMobileContactMethod.getId(),
                                    "Expected id to be '"+expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), updatedMobileContactMethod.getType(),
                                    "Expected type to be '"+expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), updatedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), updatedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), updatedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );

                },
                () -> {
                    assertTrue(updatedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), updatedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), updatedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getRole(), updatedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+expectedContactRole.getRole()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), updatedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedContactRole.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");

                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/contact/create_contact_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleIsNotMyGciUserAndDoesNotTriggersCiamApiCall () throws JsonProcessingException {

        //setup snapshot
        contact.getSnapshotData().setContactRoles(new ArrayList<ContactRole>(){{
            add(ContactRole.builder()
                    .id("9155603678013761475")
                    .customerAccountId("9155603678013761469")
                    .role("ACS User")
                    .createdWhen(Timestamp.valueOf("2019-11-07 10:14:49"))
                    .build());
        }});

        //persist contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //retrieve contact
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                () -> {
                    assertTrue(updatedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod updatedEmailContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(updatedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), updatedEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), updatedEmailContactMethod.getType(),
                                    "Expected type to be '"+expectedEmailContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), updatedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), updatedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, updatedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '"+1+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), updatedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedEmailContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod updatedMobileContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(updatedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), updatedMobileContactMethod.getId(),
                                    "Expected id to be '"+expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), updatedMobileContactMethod.getType(),
                                    "Expected type to be '"+expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), updatedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), updatedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), updatedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );

                },
                () -> {
                    assertTrue(updatedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), updatedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), updatedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId  to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value." ),

                            () -> assertEquals("ACS User", updatedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be 'ACS User', but was some other value." ),

                            () -> assertEquals(Timestamp.valueOf("2019-11-07 10:14:49"), updatedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '2019-11-07 10:14:49', but was some other value." ),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value." );

                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/contact/create_contact_and_contact_methods_and_contact_role_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewContactMethodsNull_NewContactRolesNull_contactRoleDoesNotExistAndDoesNotTriggersCiamApiCall () throws JsonProcessingException {

        //setup incoming customer account
        contact.getSnapshotData().setContactMethods(null);
        contact.getSnapshotData().setContactRoles(null);

        //persist customer account
        contactPersistenceHelper.persistContactSnapshot(contact);
        
        //retrieve customer account
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."  ),

                () -> assertTrue(updatedContact.getContactMethods().size() == 0),
                () -> assertTrue(updatedContact.getContactRoles().size() == 0)
        );
    }

    //##DELETE OPERATION
    @Test
    @Sql("/sql_scripts/contact/create_contact_with_contact_method_and_contact_role.sql")
    public void persistContactSnapshot_deletesContactSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        contact.setOperationType("Delete");

        //Delete customer account
        contactPersistenceHelper.persistContactSnapshot(contact);

       //verify contact does not exist by id
        boolean contactExists = cimContactRepository.existsById(contact.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(contactExists,
                "Expected contact to not exist, but it still does.");

    }

    @Test
    @Sql("/sql_scripts/contact/clear_contacts_and_contact_methods_and_contact_roles.sql")
    public void persistContactSnapshot_deletesContactSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        contact.setOperationType("Delete");

        //Delete customer account
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify contact does not exist by id
        boolean contactExists = cimContactRepository.existsById(contact.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(contactExists,
                "Expected contact to not exist, but it still does.");

    }

}




