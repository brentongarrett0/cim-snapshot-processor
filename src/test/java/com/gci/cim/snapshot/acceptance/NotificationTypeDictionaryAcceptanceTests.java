package com.gci.cim.snapshot.acceptance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.Application;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionary;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionarySnapshotData;
import com.gci.cim.snapshot.helper.NotificationTypeDictionaryPersistenceHelper;
import com.gci.cim.snapshot.repository.CIMBadSnapshotRepository;
import com.gci.cim.snapshot.repository.CIMNotificationTypeDictionaryRepository;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import javax.transaction.Transactional;
import java.io.File;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
public class NotificationTypeDictionaryAcceptanceTests {

    private NotificationTypeDictionary expectedNotificationTypeDictionary;
    private NotificationTypeDictionary notificationTypeDictionary;

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Autowired
    CIMNotificationTypeDictionaryRepository cimNotificationTypeDictionaryRepository;

    @Autowired
    NotificationTypeDictionaryPersistenceHelper notificationTypeDictionaryPersistenceHelper;

    @Autowired
    CIMBadSnapshotRepository cimBadSnapshotRepository;


    @BeforeEach
    public void setUp() throws Exception {

        expectedNotificationTypeDictionary =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_type_dictionary.json"), NotificationTypeDictionary.class);

        notificationTypeDictionary =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_type_dictionary.json"), NotificationTypeDictionary.class);
    }

    //##VALIDATION
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postNotificationTypeDictionarySnapshot_sendsSnapshotToBadSnapshotTable_whenNotificationTypeDictionaryIdIsMissing() throws Exception {

        //set id field in snapshot data section to null
        notificationTypeDictionary.getSnapshotData().setId(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationTypeDictionary)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postNotificationTypeDictionarySnapshot_sendsSnapshotToBadSnapshotTable_whenOperationTypeIsNotCreateUpdateOrDelete() throws Exception {

        notificationTypeDictionary.setOperationType("Select");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationTypeDictionary)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }


    //##CREATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/notification_type_dictionary/clear_notification_type_dictionary.sql")
    public void persistNotificationTypeDictionarySnapshot_createsNotificationTypeDictionary_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws Exception {

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationTypeDictionary)))
                .andExpect(status().isNoContent());

        //Test saved notification type dictionary
        NotificationTypeDictionarySnapshotData savedNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).get();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), savedNotificationTypeDictionary.getId(),
                        "Expected id to be '" + expectedNotificationTypeDictionary.getSnapshotData().getId() + "' but was some other value ") ,

               () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(savedNotificationTypeDictionary.getData()),
                       "Expected snapshotData to be '" + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + "' but was some other value "),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), savedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), savedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + "' but was some other value ")
        );

    }

    @Test
    @Sql("/sql_scripts/notification_type_dictionary/clear_notification_type_dictionary.sql")
    public void persistNotificationTypeDictionarySnapshot_createsNotificationTypeDictionary_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws Exception {

        //setup snapshot
        notificationTypeDictionary.setOperationType("Update");
        expectedNotificationTypeDictionary.setOperationType("Update");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationTypeDictionary)))
                .andExpect(status().isNoContent());

        //Test saved notification type dictionary
        NotificationTypeDictionarySnapshotData savedNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).get();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), savedNotificationTypeDictionary.getId(),
                        "Expected id to be '" + expectedNotificationTypeDictionary.getSnapshotData().getId() + "' but was some other value ") ,

                () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(savedNotificationTypeDictionary.getData()),
                        "Expected snapshotData to be '" + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + "' but was some other value "),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), savedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), savedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + "' but was some other value " )
        );

    }

    //##UPDATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/notification_type_dictionary/create_notification_type_dictionary_with_typos.sql")
    public void persistNotificationTypeDictionarySnapshot_updatesNotificationTypeDictionary_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws Exception {

        //setup incoming notification type dictionary
        notificationTypeDictionary.setOperationType("Update");
        expectedNotificationTypeDictionary.setOperationType("Update");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationTypeDictionary)))
                .andExpect(status().isNoContent());

        //Test saved notification type dictionary
        NotificationTypeDictionarySnapshotData updatedNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).get();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), updatedNotificationTypeDictionary.getId(),
                        "Expected id to be '" + expectedNotificationTypeDictionary.getSnapshotData().getId() + "' but was some other value ") ,

                () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(updatedNotificationTypeDictionary.getData()),
                        "Expected snapshotData to be '" + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + "' but was some other value "),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), updatedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), updatedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + "' but was some other value ")
        );

    }

    @Test
    @Sql("/sql_scripts/notification_type_dictionary/create_notification_type_dictionary_with_typos.sql")
    public void persistNotificationTypeDictionarySnapshot_updatesNotificationTypeDictionary_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws Exception {

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationTypeDictionary)))
                .andExpect(status().isNoContent());

        //Test saved notification type dictionary
        NotificationTypeDictionarySnapshotData updatedNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).get();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), updatedNotificationTypeDictionary.getId(),
                        "Expected id to be '" + expectedNotificationTypeDictionary.getSnapshotData().getId() + "' but was some other value ") ,

                () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(updatedNotificationTypeDictionary.getData()),
                        "Expected snapshotData to be '" + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + "' but was some other value "),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), updatedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), updatedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + "' but was some other value " )

        );

    }

    //##DELETE OPERATION
    @Test
    @Sql("/sql_scripts/notification_type_dictionary/create_notification_type_dictionary.sql")
    public void persistNotificationTypeDictionarySnapshot_deletesNotificationTypeDictionarySnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws Exception {

        //change operation type to "Delete"
        notificationTypeDictionary.setOperationType("Delete");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationTypeDictionary)))
                .andExpect(status().isNoContent());

        //verify contact does not exist by id
        boolean notificationTypeDictionaryExists = cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(notificationTypeDictionaryExists,
                "Expected notification type dictionary to not exist, but it still does.");

    }

    @Test
    @Sql("/sql_scripts/notification_type_dictionary/create_notification_type_dictionary.sql")
    public void persistNotificationTypeDictionarySnapshot_deletesNotificationTypeDictionarySnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws Exception {

        //change operation type to "Delete"
        notificationTypeDictionary.setOperationType("Delete");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationTypeDictionary)))
                .andExpect(status().isNoContent());

        //verify contact does not exist by id
        boolean notificationTypeDictionaryExists = cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(notificationTypeDictionaryExists,
                "Expected notification type dictionary to not exist, but it still does.");
    }

    private String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}



