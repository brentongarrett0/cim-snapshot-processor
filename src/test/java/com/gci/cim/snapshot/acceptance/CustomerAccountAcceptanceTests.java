package com.gci.cim.snapshot.acceptance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.Application;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccount;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccountSnapshotData;
import com.gci.cim.snapshot.repository.CIMBadSnapshotRepository;
import com.gci.cim.snapshot.repository.CimCustomerAccountRepository;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;
import java.io.File;
import java.sql.Timestamp;
import java.util.Calendar;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
public class CustomerAccountAcceptanceTests {

    private CustomerAccount customerAccount;

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private CimCustomerAccountRepository cimCustomerAccountRepository;

    @Autowired
    private CIMBadSnapshotRepository cimBadSnapshotRepository;

    @BeforeEach
    public void setUp() throws Exception {
        customerAccount = objectMapper.readValue(new File("src/test/resources/fixtures/customer_account.json"), CustomerAccount.class);
    }

    //##VALIDATION
    //snapshotdata section missing
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postCustomerAccountSnapshot_sendsSnapshotToBadSnapshotTable_whenSnapshotDataSectionIsNull() throws Exception {

        //remove snapshotdata section
        customerAccount.setSnapshotData(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    //missing id in snapshotdata section
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postCustomerAccountSnapshot_sendsSnapshotToBadSnapshotTable_whenCustomerAccountIdIsMissing() throws Exception {

        //set id field in snapshot data section to null
        customerAccount.getSnapshotData().setId(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    //operation type not create, delete, or update.
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postCustomerAccountSnapshot_sendsSnapshotToBadSnapshotTable_whenSOperationTypeIsNotCreateUpdateOrDelete() throws Exception {

        //set id field in snapshot data section to null
        customerAccount.setOperationType("Select");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    //missing fields within billingAccount object
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postCustomerAccountSnapshot_sendsSnapshotToBadSnapshotTable_whenFieldsInBillingAccountAreNull_Id() throws Exception {

        //remove fields from billing account object
        customerAccount.getSnapshotData().getBillingAccounts().get(0).setId(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postCustomerAccountSnapshot_sendsSnapshotToBadSnapshotTable_whenFieldsInBillingAccountAreNull_billingAccountNumber() throws Exception {

        //remove fields from billing account object
        customerAccount.getSnapshotData().getBillingAccounts().get(0).setBillingAccountNumber(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postCustomerAccountSnapshot_sendsSnapshotToBadSnapshotTable_whenFieldsInBillingAccountAreNull_billingContact() throws Exception {

        //remove fields from billing account object
        customerAccount.getSnapshotData().getBillingAccounts().get(0).setBillingContact(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postCustomerAccountSnapshot_sendsSnapshotToBadSnapshotTable_whenFieldsInBillingAccountAreNull_status() throws Exception {

        //remove fields from billing account object
        customerAccount.getSnapshotData().getBillingAccounts().get(0).setStatus(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postCustomerAccountSnapshot_sendsSnapshotToBadSnapshotTable_whenFieldsInBillingAccountAreNull_billHandlingCode() throws Exception {

        //remove fields from billing account object
        customerAccount.getSnapshotData().getBillingAccounts().get(0).setBillHandlingCode(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postCustomerAccountSnapshot_sendsSnapshotToBadSnapshotTable_whenFieldsInBillingAccountAreNull_createdWhen() throws Exception {
        
        //remove fields from billing account object
        customerAccount.getSnapshotData().getBillingAccounts().get(0).setCreatedWhen(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    //## CREATE OPERATION
    @Test //Happy Path
    @Sql("/sql_scripts/customer_account/clear_customer_accounts_and_billing_accounts.sql")
    public void postCustomerAccountSnapshot_createsCustomerAccountSnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist() throws Exception {

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //Verify that the snapshot persisted to the database.
        assertTrue(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId()),
                "Expected customer account to exist, but it did not.");

        //retrieve customer account
        CustomerAccountSnapshotData savedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test saved customer account
        assertAll("Existing Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), savedCustomerAccount.getName(),

                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),
                () -> assertEquals(customerAccount.getSnapshotData().getId(), savedCustomerAccount.getId(),

                        "Expected customer account id to equal '1234567', but returned some other value."),
                //TODO: Add messages for the below customer account tests.
                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), savedCustomerAccount.getCustomerAccountNumber(),
                "Expected customerAccountNumber to be " + customerAccount.getSnapshotData().getCustomerAccountNumber() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), savedCustomerAccount.getStatus(),
                        "Expected status to be " + customerAccount.getSnapshotData().getStatus() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), savedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be " + customerAccount.getSnapshotData().getParentCustomerCategory() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), savedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be " + customerAccount.getSnapshotData().getPrimaryContact() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), savedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be " +customerAccount.getSnapshotData().getCustomerCategory() + ", but was some other value"),

                () -> assertEquals(customerAccount.getChangeTimestamp(), savedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be " + customerAccount.getChangeTimestamp() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), savedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be " + customerAccount.getSnapshotData().getCreatedWhen() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), savedCustomerAccount.getIsTest(),
                        "Expected isTest to be " + customerAccount.getSnapshotData().getIsTest() + ", but was some other value"),

                () -> {
                    assertTrue(savedCustomerAccount.getBillingAccounts().size() == 1,
                            "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.");

                    //Billing account tests (only tested if above assertion returns true)
                    assertAll("Billing Account Test",
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getId(), savedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected billing account id to equal '12345', but returned some other value."),
                            //TODO: add messages for the below billing account tests.
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber(), savedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expected billingAccountNumber to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber() + ", but was some other value"),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), savedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expected contact to be " + customerAccount.getSnapshotData().getBillingContacts().get(0).getContact() + ", but was some other value" ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus(), savedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expected status to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus() + ", but was some other value" ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode(), savedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expected billHandlingCode to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode()+ ", but was some other value"),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus(), savedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expected autoPaymentStatus to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus() + ", but was some other value"),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen(), savedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen() + ", but was some other value"),

                            () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), savedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expected isTest to be " + customerAccount.getSnapshotData().getIsTest() + ", but was some other value")

                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/customer_account/clear_customer_accounts_and_billing_accounts.sql")
    public void persistCustomerAccountSnapshot_createsCustomerAccountSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws Exception {

        //Change operation type to 'Update'
        customerAccount.setOperationType("Update");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //Verify that the snapshot persisted to the database.
        assertTrue(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId()),
                "Expected customer account to exist, but it did not.");

        //retrieve customer account
        CustomerAccountSnapshotData savedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test saved customer account
        assertAll("Existing Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), savedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),
                () -> assertEquals(customerAccount.getSnapshotData().getId(), savedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), savedCustomerAccount.getCustomerAccountNumber(),
                        "Expected customerAccountNumber to be " + customerAccount.getSnapshotData().getCustomerAccountNumber() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), savedCustomerAccount.getStatus(),
                        "Expected status to be " + customerAccount.getSnapshotData().getStatus() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), savedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be " + customerAccount.getSnapshotData().getParentCustomerCategory() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), savedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be " + customerAccount.getSnapshotData().getPrimaryContact() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), savedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be " + customerAccount.getSnapshotData().getCustomerCategory() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getChangeTimestamp(), savedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be " + customerAccount.getChangeTimestamp() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), savedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be " + customerAccount.getSnapshotData().getCreatedWhen() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), savedCustomerAccount.getIsTest(),
                        "Expected isTest to be " + customerAccount.getSnapshotData().getIsTest() + ", but was some other value"),

                () -> {
                    assertTrue(savedCustomerAccount.getBillingAccounts().size() == 1,
                            "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.");

                    //Billing account tests (only tested if above assertion returns true)
                    assertAll("Billing Account Test",
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getId(), savedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected billing account id to equal '12345', but returned some other value."),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber(), savedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expected billingAccountNumber to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber() + ", but was some other value" ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), savedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expected contact to be " + customerAccount.getSnapshotData().getBillingContacts().get(0).getContact() + ", but was some other value" ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus(), savedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expected status to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus() + ", but was some other value" ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode(), savedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expected billHandlingCode to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode() + ", but was some other value"  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus(), savedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expected autoPaymentStatus to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus()+ ", but was some other value"  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen(), savedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen() + ", but was some other value" ),

                            () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), savedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expected isTest to be " + customerAccount.getSnapshotData().getIsTest() + ", but was some other value" )

                    );
                }
        );
    }

    //## UPDATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/customer_account/create_customer_account_with_incorrect_values.sql")
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist () throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(customerAccount.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);
        customerAccount.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //setup incoming customer account
        customerAccount.setOperationType("Update");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //Verify that the snapshot persisted to the database.
        assertTrue(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId()),
                "Expected customer account to exist, but it did not.");

        //retrieve customer account
        CustomerAccountSnapshotData updatedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test updated customer account
        assertAll("Existing Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), updatedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getId(), updatedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), updatedCustomerAccount.getCustomerAccountNumber(),
                        "Expected customerAccountNumber to be " + customerAccount.getSnapshotData().getCustomerAccountNumber() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), updatedCustomerAccount.getStatus(),
                        "Expected status to be " + customerAccount.getSnapshotData().getStatus() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), updatedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be " + customerAccount.getSnapshotData().getParentCustomerCategory() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), updatedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be " + customerAccount.getSnapshotData().getPrimaryContact() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), updatedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be " + customerAccount.getSnapshotData().getCustomerCategory() + ", but was some other value"),

                () -> assertEquals(customerAccount.getChangeTimestamp(), updatedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be " + customerAccount.getChangeTimestamp() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), updatedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be " + customerAccount.getSnapshotData().getCreatedWhen() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getIsTest(),
                        "Expected isTest to be " + customerAccount.getSnapshotData().getIsTest() + ", but was some other value" ),

                () -> {
                    assertTrue(updatedCustomerAccount.getBillingAccounts().size() == 1,
                            "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.");

                    //Billing account tests (only tested if above assertion returns true)
                    assertAll("Billing Account Test",
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getId(), updatedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected billing account id to equal '12345', but returned some other value."),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber(), updatedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expected billingAccountNumber to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber() + ", but was some other value" ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), updatedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expected contact to be " + customerAccount.getSnapshotData().getBillingContacts().get(0).getContact() + ", but was some other value" ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus(), updatedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expected status to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus() + ", but was some other value" ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode(), updatedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expected billHandingCode to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode() + ", but was some other value"),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus(), updatedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expected autoPaymentStatus to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus() + ", but was some other value"),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen(), updatedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen()+ ", but was some other value"  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expected isTest to be " + customerAccount.getSnapshotData().getIsTest() + ", but was some other value"  )

                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/customer_account/create_customer_and_billing_accounts_incorrect_values.sql")
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewBillingAccountsNull () throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(customerAccount.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);
        customerAccount.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //setup incoming customer account
        customerAccount.setOperationType("Update");
        customerAccount.getSnapshotData().setBillingAccounts(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //Verify that the snapshot persisted to the database.
        assertTrue(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId()),
                "Expected customer account to exist, but it did not.");

        //retrieve customer account
        CustomerAccountSnapshotData updatedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test saved customer account
        assertAll("Saved Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), updatedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),
                () -> assertEquals(customerAccount.getSnapshotData().getId(),updatedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), updatedCustomerAccount.getCustomerAccountNumber(),
                        "Expected customerAccountNumber to be " + customerAccount.getSnapshotData().getCustomerAccountNumber() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), updatedCustomerAccount.getStatus(),
                        "Expected status to be " + customerAccount.getSnapshotData().getStatus() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), updatedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be " + customerAccount.getSnapshotData().getParentCustomerCategory() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), updatedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be " + customerAccount.getSnapshotData().getPrimaryContact() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), updatedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be " + customerAccount.getSnapshotData().getCustomerCategory() + ", but was some other value"),

                () -> assertEquals(customerAccount.getChangeTimestamp(), updatedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be " + customerAccount.getChangeTimestamp()+ ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), updatedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be " + customerAccount.getSnapshotData().getCreatedWhen() + ", but was some other value"),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getIsTest(),
                        "Expected isTest to be " + customerAccount.getSnapshotData().getIsTest() + ", but was some other value"),

                () -> assertTrue(updatedCustomerAccount.getBillingAccounts().size() == 0,
                        "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.")
        );
    }

    @Test
    @Sql("/sql_scripts/customer_account/create_customer_account_with_incorrect_values.sql")
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist () throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(customerAccount.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);
        customerAccount.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //Verify that the snapshot persisted to the database.
        assertTrue(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId()),
                "Expected customer account to exist, but it did not.");

        //retrieve customer account
        CustomerAccountSnapshotData updatedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test updated customer account
        assertAll("Existing Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), updatedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),
                () -> assertEquals(customerAccount.getSnapshotData().getId(), updatedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), updatedCustomerAccount.getCustomerAccountNumber(),
                        "Expected customerAccountNumber to be " + customerAccount.getSnapshotData().getCustomerAccountNumber() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), updatedCustomerAccount.getStatus(),
                        "Expected status to be " + customerAccount.getSnapshotData().getStatus() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), updatedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be " + customerAccount.getSnapshotData().getParentCustomerCategory() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), updatedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be " + customerAccount.getSnapshotData().getPrimaryContact() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), updatedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be " + customerAccount.getSnapshotData().getCustomerCategory() + ", but was some other value"),

                () -> assertEquals(customerAccount.getChangeTimestamp(), updatedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be " + customerAccount.getChangeTimestamp() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), updatedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be " + customerAccount.getSnapshotData().getCreatedWhen() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getIsTest(),
                        "Expected isTest to be " + customerAccount.getSnapshotData().getIsTest() + ", but was some other value" ),

                () -> {
                    assertTrue(updatedCustomerAccount.getBillingAccounts().size() == 1,
                            "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.");

                    //Billing account tests (only tested if above assertion returns true)
                    assertAll("Billing Account Test",
                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getId(), updatedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected billing account id to equal '12345', but returned some other value."),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber(), updatedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expected billingAccountNumber to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillingAccountNumber() + ", but was some other value"  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), updatedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expected contact to be " +customerAccount.getSnapshotData().getBillingContacts().get(0).getContact() + ", but was some other value"  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus(), updatedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expected status to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getStatus() + ", but was some other value"   ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode(), updatedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expected billHandlingCode to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getBillHandlingCode() + ", but was some other value"   ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus(), updatedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expected autoPaymentStatus to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getAutoPaymentStatus() + ", but was some other value"  ),

                            () -> assertEquals(customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen(), updatedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be " + customerAccount.getSnapshotData().getBillingAccounts().get(0).getCreatedWhen() + ", but was some other value" ),

                            () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expected isTest to be " + customerAccount.getSnapshotData().getIsTest() + ", but was some other value" )

                    );
                }
        );

    }

    @Test
    @Sql("/sql_scripts/customer_account/create_customer_and_billing_accounts_incorrect_values.sql")
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewBillingAccountsNull () throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(customerAccount.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);
        customerAccount.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //setup incoming customer account
        customerAccount.getSnapshotData().setBillingAccounts(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //Verify that the snapshot persisted to the database.
        assertTrue(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId()),
                "Expected customer account to exist, but it did not.");

        //retrieve customer account
        CustomerAccountSnapshotData updatedCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //Test saved customer account
        assertAll("Saved Customer Account",
                () -> assertEquals(customerAccount.getSnapshotData().getName(), updatedCustomerAccount.getName(),
                        "Expected customer account name to equal 'Bilbo Baggins', but returned some other value."),
                () -> assertEquals(customerAccount.getSnapshotData().getId(),updatedCustomerAccount.getId(),
                        "Expected customer account id to equal '1234567', but returned some other value."),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerAccountNumber(), updatedCustomerAccount.getCustomerAccountNumber(),
                        "Expected customerAccountNumber to be " + customerAccount.getSnapshotData().getCustomerAccountNumber() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getStatus(), updatedCustomerAccount.getStatus(),
                        "Expected status to be " + customerAccount.getSnapshotData().getStatus() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getParentCustomerCategory(), updatedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be " + customerAccount.getSnapshotData().getParentCustomerCategory() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getPrimaryContact(), updatedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be " + customerAccount.getSnapshotData().getPrimaryContact() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getCustomerCategory(), updatedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be " + customerAccount.getSnapshotData().getCustomerCategory() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getChangeTimestamp(), updatedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be " + customerAccount.getChangeTimestamp() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getCreatedWhen(), updatedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be " + customerAccount.getSnapshotData().getCreatedWhen() + ", but was some other value" ),

                () -> assertEquals(customerAccount.getSnapshotData().getIsTest(), updatedCustomerAccount.getIsTest(),
                        "Expected isTest to be " + customerAccount.getSnapshotData().getIsTest() + ", but was some other value" ),

                () -> assertTrue(updatedCustomerAccount.getBillingAccounts().size() == 0,
                        "Expected 'billingAccounts' array to contain 1 billing account, but returned some other size.")
        );

    }
    //## DELETE OPERATION
    @Test
    @Sql("/sql_scripts/customer_account/create_customer_account_and_billing_account.sql")
    public void persistCustomerAccountSnapshot_deletesCustomerAccountSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws Exception {

        //change operation type to "Delete"
        customerAccount.setOperationType("Delete");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //verify customer account and billing account(s) does not exist by id
        boolean customerAccountExists = cimCustomerAccountRepository.existsById("1234567");

        //verify customer account does not exist
        assertFalse(customerAccountExists,
                "Expected customer account to not exist, but it still does.");

    }

    @Test
    @Sql("/sql_scripts/customer_account/clear_customer_accounts_and_billing_accounts.sql")
    public void persistCustomerAccountSnapshot_deletesCustomerAccountSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws Exception {

        //change operation type to "Delete"
        customerAccount.setOperationType("Delete");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(customerAccount)))
                .andExpect(status().isNoContent());

        //verify customer account and billing account(s) does not exist by id
        boolean customerAccountExists = cimCustomerAccountRepository.existsById("1234567");

        //verify customer account does not exist
        assertFalse(customerAccountExists,
                "Expected customer account to not exist, but it still does.");

    }

    private String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
