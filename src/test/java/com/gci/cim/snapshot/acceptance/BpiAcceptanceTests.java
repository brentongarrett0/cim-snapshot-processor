package com.gci.cim.snapshot.acceptance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.Application;
import com.gci.cim.snapshot.entity.bpi.Bpi;
import com.gci.cim.snapshot.entity.bpi.BpiSnapshotData;
import com.gci.cim.snapshot.helper.BpiPersistenceHelper;
import com.gci.cim.snapshot.repository.CIMBadSnapshotRepository;
import com.gci.cim.snapshot.repository.CIMBpiRepository;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import javax.transaction.Transactional;
import java.io.File;
import java.sql.Timestamp;
import java.util.Calendar;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
public class BpiAcceptanceTests {

    private static Bpi expectedBpi;
    private Bpi bpi;

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Autowired
    CIMBpiRepository cimBpiRepository;

    @Autowired
    BpiPersistenceHelper bpiPersistenceHelper;

    @Autowired
    CIMBadSnapshotRepository cimBadSnapshotRepository;

    @BeforeEach
    public void setUp() throws Exception {

        expectedBpi =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/bpi.json"), Bpi.class);

        bpi =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/bpi.json"), Bpi.class);
    }


    //##VALIDATION
    //snapshotdata section missing
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postBpiSnapshot_sendsSnapshotToBadSnapshotTable_whenSnapshotDataSectionIsNull() throws Exception {

        //remove snapshotdata section
        bpi.setSnapshotData(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(bpi)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    //missing id in snapshotdata section
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postBpiSnapshot_sendsSnapshotToBadSnapshotTable_whenBpiIdIsMissing() throws Exception {

        //set id field in snapshot data section to null
        bpi.getSnapshotData().setId(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(bpi)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    //operation type not create, delete, or update.
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postBpiSnapshot_sendsSnapshotToBadSnapshotTable_whenSOperationTypeIsNotCreateUpdateOrDelete() throws Exception {

        //set id field in snapshot data section to null
        bpi.setOperationType("Select");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(bpi)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");
    }

    //##CREATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/bpi/clear_bpis.sql")
    public void persistBpiSnapshot_createsBpiSnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws Exception {

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
        .content(asJsonString(bpi)))
        .andExpect(status().isNoContent());

        //retrieve bpi
        BpiSnapshotData savedBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).get();

        //verify bpi table consists of only 3 records
        assertTrue(cimBpiRepository.count() == 3,
                "Expected bpi table to contain exactly 3 records, but it has some other value.");

        //verify bpi still exists by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());
        assertTrue(bpiExists, "Expected bpi to exist, but it does not.");

        //TODO: Add messages for Bpi tests.
        //PERFORM TESTS
        assertAll("Saved BPI",
                () -> assertEquals(expectedBpi.getSnapshotData().getId(), savedBpi.getId(),
                        "Expected id to be '" + expectedBpi.getSnapshotData().getId() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), savedBpi.getCustomerAccountId(),
                        "Expected parent id to be '" + expectedBpi.getSnapshotData().getParentId() + "' but was some other value."),

                () -> assertEquals(null, savedBpi.getBpiSnapshotData(),
                        "Expected id to be null but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getName(), savedBpi.getName(),
                        "Expected name to be '" + expectedBpi.getSnapshotData().getName() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getStatus(), savedBpi.getStatus(),
                        "Expected status to be '" + expectedBpi.getSnapshotData().getStatus() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingId(), savedBpi.getOfferingId(),
                        "Expected offeringId to be '" + expectedBpi.getSnapshotData().getOfferingId() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingName(), savedBpi.getOfferingName(),
                        "Expected offeringName to be '" + expectedBpi.getSnapshotData().getOfferingName() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductCategory(), savedBpi.getProductCategory(),
                        "Expected productCategory to be '" + expectedBpi.getSnapshotData().getProductCategory() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketId(), savedBpi.getMarketId(),
                        "Expected marketId to be '" + expectedBpi.getSnapshotData().getMarketId() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketName(), savedBpi.getMarketName(),
                        "Expected marketName to be '" + expectedBpi.getSnapshotData().getMarketName() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getCreatedWhen(), savedBpi.getCreatedWhen(),
                        "Expected createdWhen to be '" + expectedBpi.getSnapshotData().getCreatedWhen() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getBillingAccountId(), savedBpi.getBillingAccountId(),
                        "Expected billingAccountId to be '" + expectedBpi.getSnapshotData().getBillingAccountId() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getServiceName(), savedBpi.getServiceName(),
                        "Expected ServiceName to be '" + expectedBpi.getSnapshotData().getServiceName() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvineId(), savedBpi.getSandvineId(),
                        "Expected sanvineId to be '" + expectedBpi.getSnapshotData().getSandvineId() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvinePlanId(), savedBpi.getSandvinePlanId(),
                        "Expected sanvinePlanId to be '" + expectedBpi.getSnapshotData().getSandvinePlanId() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductInstance(), savedBpi.getProductInstance(),
                        "Expected productInstance to be '" + expectedBpi.getSnapshotData().getProductInstance() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct(), savedBpi.getSeachangeSubscriptionProduct(),
                        "Expected seachangeSubscriptionProduct to be '" + expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriberId(), savedBpi.getSeachangeSubscriberId(),
                        "Expected id to be '" + expectedBpi.getSnapshotData().getSeachangeSubscriberId() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIncludedUsageAmountGb(), savedBpi.getIncludedUsageAmountGb(),
                        "Expected includeUsageAmountGb to be '" + expectedBpi.getSnapshotData().getIncludedUsageAmountGb() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getChangeTimestamp(), savedBpi.getChangeTimestamp(),
                        "Expected changeTimestamp to be '" + expectedBpi.getSnapshotData().getChangeTimestamp() + "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIsLifeline(), savedBpi.getIsLifeline(),
                        "Expected isLifeline to be '" + expectedBpi.getSnapshotData().getIsLifeline() + "' but was some other value.") ,

                () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), savedBpi.getIsTest(),
                        "Expected isTest to be '" + expectedBpi.getSnapshotData().getIsTest() + "' but was some other value."),

                () -> {
                    assertTrue(savedBpi.getChildBpis().size() == 1);

                    BpiSnapshotData childBpi = savedBpi.getChildBpis().get(0);
                    assertAll("Saved Child BPI",
                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), childBpi.getId(),
                            "Expected id to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getId() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getId(), childBpi.getBpiSnapshotData().getId(),
                            "Expected id to be '" + expectedBpi.getSnapshotData().getId() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), childBpi.getCustomerAccountId(),
                            "Expected parent id to be '" + expectedBpi.getSnapshotData().getParentId() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getName(), childBpi.getName(),
                                    "Expected name to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getName() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus(), childBpi.getStatus(),
                                    "Expected status to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId(), childBpi.getOfferingId(),
                                    "Expected id to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName(), childBpi.getOfferingName(),
                                    "Expected offering name to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory(), childBpi.getProductCategory(),
                                    "Expected product category to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId(), childBpi.getMarketId(),
                                    "Expected market id to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName(), childBpi.getMarketName(),
                                    "Expected market name to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen(), childBpi.getCreatedWhen(),
                                    "Expected created when to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId(), childBpi.getBillingAccountId(),
                                    "Expected billing account id to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), childBpi.getIsTest(),
                                    "Expected isTest to be '" + expectedBpi.getSnapshotData().getIsTest() + "' but was some other value."),

                            () -> {
                                assertTrue(childBpi.getChildBpis().size() == 1);

                                BpiSnapshotData grandChildBpi = savedBpi.getChildBpis().get(0).getChildBpis().get(0);
                                assertAll("Saved Grand-Child BPI",
                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId(), grandChildBpi.getId(),
                                        "Expected id to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), grandChildBpi.getBpiSnapshotData().getId(),
                                        "Expected id to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getId() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), grandChildBpi.getCustomerAccountId(),
                                        "Expected parent id to be '" + expectedBpi.getSnapshotData().getParentId() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName(), grandChildBpi.getName(),
                                        "Expected name to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus(), grandChildBpi.getStatus(),
                                        "Expected status to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId(), grandChildBpi.getOfferingId(),
                                         "Expected offering id to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId() + "' but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName(), grandChildBpi.getOfferingName(),
                                        "Expected offering name to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory(), grandChildBpi.getProductCategory(),
                                        "Expected product category to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId(), grandChildBpi.getMarketId(),
                                        "Expected market id to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName(), grandChildBpi.getMarketName(),
                                        "Expected market name to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen(), grandChildBpi.getCreatedWhen(),
                                        "Expected created when to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId(), grandChildBpi.getBillingAccountId(),
                                        "Expected billing account id to be '" + expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), grandChildBpi.getIsTest(),
                                       "Expected isTest to be '" + expectedBpi.getSnapshotData().getIsTest() + "' but was some other value." ),

                                        () -> assertTrue(grandChildBpi.getChildBpis().size() == 0)
                                );
                            }
                    );
                }
        );

    }

    @Test
    @Sql("/sql_scripts/bpi/clear_bpis.sql")
    public void persistBpiSnapshot_createsBpiSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws Exception {

        //setup bpi
        bpi.setOperationType("Update");
        expectedBpi.setOperationType("Update");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(bpi)))
                .andExpect(status().isNoContent());

        //retrieve bpi
        BpiSnapshotData savedBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).get();

        //verify bpi table consists of only 3 records
        assertTrue(cimBpiRepository.count() == 3,
                "Expected bpi table to contain exactly 3 records, but it has some other value.");

        //verify bpi still exists by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());
        assertTrue(bpiExists, "Expected bpi to exist, but it does not.");

        //TODO: Add messages for Bpi tests.
        //PERFORM TESTS
        assertAll("Saved BPI",
                () -> assertEquals(expectedBpi.getSnapshotData().getId(), savedBpi.getId(),
                "Expected id to be '" + expectedBpi.getSnapshotData().getId()+ "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), savedBpi.getCustomerAccountId(),
                "Expected parent id to be '"+ expectedBpi.getSnapshotData().getParentId()+"' but was some other value."),

                () -> assertEquals(null, savedBpi.getBpiSnapshotData(),
                "Expected bpiSnapshotData  to be 'null' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getName(), savedBpi.getName(),
                "Expected name to be '"+ expectedBpi.getSnapshotData().getName() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getStatus(), savedBpi.getStatus(),
                "Expected status to be '"+ expectedBpi.getSnapshotData().getStatus() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingId(), savedBpi.getOfferingId(),
                "Expected offeringId to be '"+ expectedBpi.getSnapshotData().getOfferingId() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getOfferingName(), savedBpi.getOfferingName(),
                "Expected offeringName to be '"+ expectedBpi.getSnapshotData().getOfferingName() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductCategory(), savedBpi.getProductCategory(),
               "Expected productCategory to be '"+ expectedBpi.getSnapshotData().getProductCategory() +"' but was some other value." ),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketId(), savedBpi.getMarketId(),
                "Expected marketId to be '"+ expectedBpi.getSnapshotData().getMarketId() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getMarketName(), savedBpi.getMarketName(),
                "Expected marketName to be '"+ expectedBpi.getSnapshotData().getMarketName() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getCreatedWhen(), savedBpi.getCreatedWhen(),
                "Expected createdWhen to be '"+ expectedBpi.getSnapshotData().getCreatedWhen() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getBillingAccountId(), savedBpi.getBillingAccountId(),
                "Expected billingAccountId to be '"+ expectedBpi.getSnapshotData().getBillingAccountId() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getServiceName(), savedBpi.getServiceName(),
                "Expected serviceName to be '"+ expectedBpi.getSnapshotData().getServiceName() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvineId(), savedBpi.getSandvineId(),
                "Expected sandvineId to be '"+ expectedBpi.getSnapshotData().getSandvineId() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSandvinePlanId(), savedBpi.getSandvinePlanId(),
                "Expected sandvinePlanId to be '"+ expectedBpi.getSnapshotData().getSandvinePlanId() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getProductInstance(), savedBpi.getProductInstance(),
                "Expected productInstance to be '"+ expectedBpi.getSnapshotData().getProductInstance() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct(), savedBpi.getSeachangeSubscriptionProduct(),
                "Expected seachangeSubscriptionProduct to be '"+ expectedBpi.getSnapshotData().getSeachangeSubscriptionProduct() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getSeachangeSubscriberId(), savedBpi.getSeachangeSubscriberId(),
                "Expected seachangeSubscriberId to be '"+ expectedBpi.getSnapshotData().getSeachangeSubscriberId() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIncludedUsageAmountGb(), savedBpi.getIncludedUsageAmountGb(),
                "Expected includedUsageAmoountGb to be '"+ expectedBpi.getSnapshotData().getIncludedUsageAmountGb() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getChangeTimestamp(), savedBpi.getChangeTimestamp(),
                "Expected changeTimestamp to be '"+ expectedBpi.getChangeTimestamp() +"' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIsLifeline(), savedBpi.getIsLifeline(),
                "Expected isLifeline to be '"+ expectedBpi.getSnapshotData().getIsLifeline() +"' but was some other value.") ,

                () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), savedBpi.getIsTest(),
                "Expected isTest to be '"+ expectedBpi.getSnapshotData().getIsTest() +"' but was some other value."),

                () -> {
                    assertTrue(savedBpi.getChildBpis().size() == 1);

                    BpiSnapshotData childBpi = savedBpi.getChildBpis().get(0);
                    assertAll("Saved Child BPI",
                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), childBpi.getId(),
                            "Expected id to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getId() +"' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getId(), childBpi.getBpiSnapshotData().getId(),
                           "Expected id to be '"+ expectedBpi.getSnapshotData().getId() +"' but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), childBpi.getCustomerAccountId(),
                            "Expected parent id to be '"+ expectedBpi.getSnapshotData().getParentId() +"' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getName(), childBpi.getName(),
                            "Expected name to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getName() +"' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus(), childBpi.getStatus(),
                           "Expected status to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getStatus() +"' but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId(), childBpi.getOfferingId(),
                            "Expected offeringId to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingId() +"' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName(), childBpi.getOfferingName(),
                            "Expected offeringName to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getOfferingName() +"' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory(), childBpi.getProductCategory(),
                            "Expected productCategory to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getProductCategory() +"' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId(), childBpi.getMarketId(),
                           "Expected marketId to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketId() +"' but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName(), childBpi.getMarketName(),
                            "Expected marketName to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getMarketName() +"' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen(), childBpi.getCreatedWhen(),
                            "Expected createdWhen to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen() +"' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId(), childBpi.getBillingAccountId(),
                           "Expected billingAccountId to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId() +"' but was some other value." ),

                            () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), childBpi.getIsTest(),
                            "Expected isTest to be '"+ expectedBpi.getSnapshotData().getIsTest() +"' but was some other value."),

                            () -> {
                                assertTrue(childBpi.getChildBpis().size() == 1);

                                BpiSnapshotData grandChildBpi = savedBpi.getChildBpis().get(0).getChildBpis().get(0);
                                assertAll("Saved Grand-Child BPI",
                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId(), grandChildBpi.getId(),
                                        "Expected id to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId() +"' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getId(), grandChildBpi.getBpiSnapshotData().getId(),
                                       "Expected id to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getId() +"' but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), grandChildBpi.getCustomerAccountId(),
                                       "Expected parent id to be '"+ expectedBpi.getSnapshotData().getParentId() +"' but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName(), grandChildBpi.getName(),
                                        "Expected name to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName() +"' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus(), grandChildBpi.getStatus(),
                                        "Expected status to be '"+ 1 +"' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId(), grandChildBpi.getOfferingId(),
                                       "Expected offering id to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId() +"' but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName(), grandChildBpi.getOfferingName(),
                                        "Expected offering name to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName() +"' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory(), grandChildBpi.getProductCategory(),
                                        "Expected product category to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory() +"' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId(), grandChildBpi.getMarketId(),
                                        "Expected market id to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId() +"' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName(), grandChildBpi.getMarketName(),
                                       "Expected market name to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName() +"' but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen(), grandChildBpi.getCreatedWhen(),
                                       "Expected created when to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen() +"' but was some other value." ),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId(), grandChildBpi.getBillingAccountId(),
                                        "Expected billingAccountId to be '"+ expectedBpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId() +"' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), grandChildBpi.getIsTest(),
                                        "Expected isTest to be '"+ expectedBpi.getSnapshotData().getIsTest() +"' but was some other value."),

                                        () -> assertTrue(grandChildBpi.getChildBpis().size() == 0)

                                );
                            }
                    );
                }
        );

    }

    //##UPDATE OPERATION
    @Test
    @Sql("/sql_scripts/bpi/create_bpi_with_child_bpi_and_grandchild_bpi_with_typos.sql")
    public void persistBpiSnapshot_updatesBpiSnapshot_whenOperationTypeIsUpdateAndSnapshotExists () throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(bpi.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);
        bpi.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //setup bpi
        bpi.setOperationType("Update");
        expectedBpi.setOperationType("Update");
        expectedBpi.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(bpi)))
                .andExpect(status().isNoContent());

        //retrieve bpi
        BpiSnapshotData updatedBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).get();

        //verify bpi table consists of only 3 records
        assertTrue(cimBpiRepository.count() == 3,
                "Expected bpi table to contain exactly 3 records, but it has some other value.");

        //verify bpi still exists by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());
        assertTrue(bpiExists, "Expected bpi to exist, but it does not.");

        System.out.println("**updated bpi snapshot: " + updatedBpi.getSnapshot());
        //TODO: Add messages for Bpi tests.
        //PERFORM TESTS
        assertAll("Updated BPI",
                () -> assertEquals(bpi.getSnapshotData().getId(), updatedBpi.getId(),
                "Expected id to be '"+bpi.getSnapshotData().getId()+"' but was some other value"),

                () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), updatedBpi.getCustomerAccountId(),
                "Expected parent id to be '"+expectedBpi.getSnapshotData().getParentId()+"' but was some other value"),

                () -> assertEquals(null, updatedBpi.getBpiSnapshotData(),
                "Expected bpiSnapshotData to be 'null' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getName(), updatedBpi.getName(),
                "Expected name to be '"+bpi.getSnapshotData().getName()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getStatus(), updatedBpi.getStatus(),
               "Expected status to be '"+bpi.getSnapshotData().getStatus()+"' but was some other value" ),

                () -> assertEquals(bpi.getSnapshotData().getOfferingId(), updatedBpi.getOfferingId(),
                "Expected offeringId to be '"+bpi.getSnapshotData().getOfferingId()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getOfferingName(), updatedBpi.getOfferingName(),
                "Expected offeringName to be '"+bpi.getSnapshotData().getOfferingName()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getProductCategory(), updatedBpi.getProductCategory(),
                "Expected productCategory to be '"+bpi.getSnapshotData().getProductCategory()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getMarketId(), updatedBpi.getMarketId(),
                "Expected marketId to be '"+bpi.getSnapshotData().getMarketId()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getMarketName(), updatedBpi.getMarketName(),
               "Expected marketName to be '"+bpi.getSnapshotData().getMarketName()+"' but was some other value" ),

                () -> assertEquals(bpi.getSnapshotData().getCreatedWhen(), updatedBpi.getCreatedWhen(),
               "Expected createdWhen to be '"+bpi.getSnapshotData().getCreatedWhen()+"' but was some other value" ),

                () -> assertEquals(bpi.getSnapshotData().getBillingAccountId(), updatedBpi.getBillingAccountId(),
               "Expected billingAccountId to be '"+bpi.getSnapshotData().getBillingAccountId()+"' but was some other value" ),

                () -> assertEquals(bpi.getSnapshotData().getServiceName(), updatedBpi.getServiceName(),
                "Expected serviceName to be '"+bpi.getSnapshotData().getServiceName()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getSandvineId(), updatedBpi.getSandvineId(),
                "Expected sandvineId to be '"+bpi.getSnapshotData().getSandvineId()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getSandvinePlanId(), updatedBpi.getSandvinePlanId(),
                "Expected sandvinePlanId to be '"+bpi.getSnapshotData().getSandvinePlanId()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getProductInstance(), updatedBpi.getProductInstance(),
                "Expected productInstnace to be '"+bpi.getSnapshotData().getProductInstance()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getSeachangeSubscriptionProduct(), updatedBpi.getSeachangeSubscriptionProduct(),
                "Expected seachangeSubscriptionProduct to be '"+bpi.getSnapshotData().getSeachangeSubscriptionProduct()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getSeachangeSubscriberId(), updatedBpi.getSeachangeSubscriberId(),
                "Expected seachangeSubscriberId to be '"+bpi.getSnapshotData().getSeachangeSubscriberId()+"' but was some other value"),

                () -> assertEquals(bpi.getSnapshotData().getIncludedUsageAmountGb(), updatedBpi.getIncludedUsageAmountGb(),
                "Expected includedUsageAmountGb to be '"+bpi.getSnapshotData().getIncludedUsageAmountGb()+"' but was some other value"),

                () -> assertEquals(expectedBpi.getChangeTimestamp(), updatedBpi.getChangeTimestamp(),
                "Expected changeTimestamp to be '"+expectedBpi.getChangeTimestamp()+"' but was some other value"),

                () -> assertEquals(expectedBpi.getSnapshotData().getIsLifeline(), updatedBpi.getIsLifeline(),
                "Expected isLifeline to be '"+expectedBpi.getSnapshotData().getIsLifeline()+"' but was some other value") ,

                () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), updatedBpi.getIsTest(),
                "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"' but was some other value"),

                () -> {
                    assertTrue(updatedBpi.getChildBpis().size() == 1);

                    BpiSnapshotData childBpi = updatedBpi.getChildBpis().get(0);
                    assertAll("Saved Child BPI",
                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getId(), childBpi.getId(),
                            "Expected id to be '"+bpi.getSnapshotData().getChildBpis().get(0).getId()+"' but was some other value"),

                            () -> assertEquals(bpi.getSnapshotData().getId(), childBpi.getBpiSnapshotData().getId(),
                            "Expected id to be '"+bpi.getSnapshotData().getId()+"' but was some other value"),

                            () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), childBpi.getCustomerAccountId(),
                            "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"' but was some other value"),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getName(), childBpi.getName(),
                            "Expected name to be '"+bpi.getSnapshotData().getChildBpis().get(0).getName()+"' but was some other value"),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getStatus(), childBpi.getStatus(),
                            "Expected status to be '"+bpi.getSnapshotData().getChildBpis().get(0).getStatus()+"' but was some other value"),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getOfferingId(), childBpi.getOfferingId(),
                            "Expected offeringId to be '"+bpi.getSnapshotData().getChildBpis().get(0).getOfferingId()+"' but was some other value"),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getOfferingName(), childBpi.getOfferingName(),
                            "Expected offeringName to be '"+bpi.getSnapshotData().getChildBpis().get(0).getOfferingName()+"' but was some other value"),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getProductCategory(), childBpi.getProductCategory(),
                            "Expected productCategory to be '"+bpi.getSnapshotData().getChildBpis().get(0).getProductCategory()+"' but was some other value"),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getMarketId(), childBpi.getMarketId(),
                            "Expected marketId to be '"+bpi.getSnapshotData().getChildBpis().get(0).getMarketId()+"' but was some other value"),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getMarketName(), childBpi.getMarketName(),
                            "Expected marketName to be '"+bpi.getSnapshotData().getChildBpis().get(0).getMarketName()+"' but was some other value"),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen(), childBpi.getCreatedWhen(),
                            "Expected createdWhen to be '"+bpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen()+"' but was some other value"),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId(), childBpi.getBillingAccountId(),
                            "Expected billingAccountId to be '"+bpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId()+"' but was some other value"),

                            () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), childBpi.getIsTest(),
                            "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"' but was some other value"),

                            () -> {
                                assertTrue(childBpi.getChildBpis().size() == 1);

                                BpiSnapshotData grandChildBpi = updatedBpi.getChildBpis().get(0).getChildBpis().get(0);
                                assertAll("Saved Grand-Child BPI",
                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId(), grandChildBpi.getId(),
                                        "Expected id to be '"+bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId()+"' but was some other value"),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getId(), grandChildBpi.getBpiSnapshotData().getId(),
                                        "Expected id to be '"+bpi.getSnapshotData().getChildBpis().get(0).getId()+"' but was some other value"),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), grandChildBpi.getCustomerAccountId(),
                                        "Expected parentId to be '"+expectedBpi.getSnapshotData().getParentId()+"' but was some other value"),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName(), grandChildBpi.getName(),
                                        "Expected name to be '"+bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName()+"' but was some other value"),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus(), grandChildBpi.getStatus(),
                                        "Expected status to be '"+bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus()+"' but was some other value"),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId(), grandChildBpi.getOfferingId(),
                                        "Expected offeringId to be '"+bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId()+"' but was some other value"),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName(), grandChildBpi.getOfferingName(),
                                        "Expected offeringName to be '"+bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName()+"' but was some other value"),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory(), grandChildBpi.getProductCategory(),
                                       "Expected productCategory to be '"+bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory()+"' but was some other value" ),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId(), grandChildBpi.getMarketId(),
                                        "Expected marketId to be '"+bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId()+"' but was some other value"),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName(), grandChildBpi.getMarketName(),
                                        "Expected marketName to be '"+bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName()+"' but was some other value"),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen(), grandChildBpi.getCreatedWhen(),
                                        "Expected creatdWhen to be '"+bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen()+"' but was some other value"),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId(), grandChildBpi.getBillingAccountId(),
                                        "Expected billingAccountId to be '"+bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId()+"' but was some other value"),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), grandChildBpi.getIsTest(),
                                        "Expected isTest to be '"+expectedBpi.getSnapshotData().getIsTest()+"' but was some other value"),

                                        () -> assertTrue(grandChildBpi.getChildBpis().size() == 0)
                                );
                            }
                    );
                }
        );
    }


    @Test
    @Sql("/sql_scripts/bpi/create_bpi_with_child_bpi_and_grandchild_bpi_with_typos.sql")
    public void persistBpiSnapshot_updatesBpiSnapshot_whenOperationTypeIsCreateAndSnapshotExists () throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(bpi.getChangeTimestamp().getTime());
        cal.add(Calendar.YEAR, 1);

        //setup bpi
        bpi.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));
        expectedBpi.setChangeTimestamp(new Timestamp(cal.getTime().getTime()));

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(bpi)))
                .andExpect(status().isNoContent());

        //retrieve bpi
        BpiSnapshotData updatedBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).get();

        //verify bpi table consists of only 3 records
        assertTrue(cimBpiRepository.count() == 3,
                "Expected bpi table to contain exactly 3 records, but it has some other value.");

        //verify bpi still exists by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());
        assertTrue(bpiExists, "Expected bpi to exist, but it does not.");

        //TODO: Add messages for Bpi tests.
        //PERFORM TESTS
        assertAll("Updated BPI",
                () -> assertEquals(bpi.getSnapshotData().getId(), updatedBpi.getId(),
                "Expected id to be '"+ bpi.getSnapshotData().getId()+ "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), updatedBpi.getCustomerAccountId(),
                "Expected id to be '"+ expectedBpi.getSnapshotData().getParentId()+ "' but was some other value."),

                () -> assertEquals(null, updatedBpi.getBpiSnapshotData(),
                "Expected bpiSnapshotData to be 'null' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getName(), updatedBpi.getName(),
                "Expected name to be '"+ bpi.getSnapshotData().getName()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getStatus(), updatedBpi.getStatus(),
                "Expected status to be '"+ bpi.getSnapshotData().getStatus()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getOfferingId(), updatedBpi.getOfferingId(),
                "Expected offeringId to be '"+ bpi.getSnapshotData().getOfferingId()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getOfferingName(), updatedBpi.getOfferingName(),
                "Expected offeringName to be '"+ bpi.getSnapshotData().getOfferingName()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getProductCategory(), updatedBpi.getProductCategory(),
                "Expected productCategory to be '"+ bpi.getSnapshotData().getProductCategory()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getMarketId(), updatedBpi.getMarketId(),
                "Expected marketId to be '"+ bpi.getSnapshotData().getMarketId()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getMarketName(), updatedBpi.getMarketName(),
                "Expected marketName to be '"+ bpi.getSnapshotData().getMarketName()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getCreatedWhen(), updatedBpi.getCreatedWhen(),
                "Expected createdWhen to be '"+ bpi.getSnapshotData().getCreatedWhen()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getBillingAccountId(), updatedBpi.getBillingAccountId(),
                "Expected billingAccountId to be '"+ bpi.getSnapshotData().getBillingAccountId()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getServiceName(), updatedBpi.getServiceName(),
                "Expected serviceName to be '"+ bpi.getSnapshotData().getServiceName()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getSandvineId(), updatedBpi.getSandvineId(),
                        "Expected sandvineId to be '"+ bpi.getSnapshotData().getSandvineId()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getSandvinePlanId(), updatedBpi.getSandvinePlanId(),
                "Expected sandvinePlanId to be '"+ bpi.getSnapshotData().getSandvinePlanId()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getProductInstance(), updatedBpi.getProductInstance(),
                "Expected productInstance to be '"+ bpi.getSnapshotData().getProductInstance()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getSeachangeSubscriptionProduct(), updatedBpi.getSeachangeSubscriptionProduct(),
                "Expected seachangeSubscriptionProduct to be '"+ bpi.getSnapshotData().getSeachangeSubscriptionProduct()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getSeachangeSubscriberId(), updatedBpi.getSeachangeSubscriberId(),
                "Expected seachangeSubcribeId to be '"+ bpi.getSnapshotData().getSeachangeSubscriberId()+ "' but was some other value."),

                () -> assertEquals(bpi.getSnapshotData().getIncludedUsageAmountGb(), updatedBpi.getIncludedUsageAmountGb(),
                "Expected includedUsageAmountGb to be '"+ bpi.getSnapshotData().getIncludedUsageAmountGb()+ "' but was some other value."),

                () -> assertEquals(expectedBpi.getChangeTimestamp(), updatedBpi.getChangeTimestamp(),
                "Expected changeTimestamp to be '"+ expectedBpi.getChangeTimestamp()+ "' but was some other value."),

                () -> assertEquals(expectedBpi.getSnapshotData().getIsLifeline(), updatedBpi.getIsLifeline(),
               "Expected isLifeline to be '"+ expectedBpi.getSnapshotData().getIsLifeline()+ "' but was some other value." ) ,

                () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), updatedBpi.getIsTest(),
                "Expected isTest to be '"+ expectedBpi.getSnapshotData().getIsTest()+ "' but was some other value."),

                () -> {
                    assertTrue(updatedBpi.getChildBpis().size() == 1);

                    BpiSnapshotData childBpi = updatedBpi.getChildBpis().get(0);
                    assertAll("Saved Child BPI",
                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getId(), childBpi.getId(),
                            "Expected id to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getId()+ "' but was some other value."),

                            () -> assertEquals(bpi.getSnapshotData().getId(), childBpi.getBpiSnapshotData().getId(),
                            "Expected id to be '"+ bpi.getSnapshotData().getId()+ "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), childBpi.getCustomerAccountId(),
                            "Expected parentId to be '"+ expectedBpi.getSnapshotData().getParentId()+ "' but was some other value."),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getName(), childBpi.getName(),
                            "Expected name to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getName()+ "' but was some other value."),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getStatus(), childBpi.getStatus(),
                            "Expected status to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getStatus()+ "' but was some other value."),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getOfferingId(), childBpi.getOfferingId(),
                            "Expected ### to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getOfferingId()+ "' but was some other value."),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getOfferingName(), childBpi.getOfferingName(),
                            "Expected offeringName to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getOfferingName()+ "' but was some other value."),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getProductCategory(), childBpi.getProductCategory(),
                            "Expected productCategory to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getProductCategory()+ "' but was some other value."),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getMarketId(), childBpi.getMarketId(),
                            "Expected marketId to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getMarketId()+ "' but was some other value."),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getMarketName(), childBpi.getMarketName(),
                            "Expected marketName to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getMarketName()+ "' but was some other value."),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen(), childBpi.getCreatedWhen(),
                            "Expected createdWhen to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getCreatedWhen()+ "' but was some other value."),

                            () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId(), childBpi.getBillingAccountId(),
                            "Expected billingAccountId to be '" + bpi.getSnapshotData().getChildBpis().get(0).getBillingAccountId() + "' but was some other value."),

                            () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), childBpi.getIsTest(),
                            "Expected isTest to be '" + expectedBpi.getSnapshotData().getIsTest() + "' but was some other value."),

                            () -> {
                                assertTrue(childBpi.getChildBpis().size() == 1);

                                BpiSnapshotData grandChildBpi = updatedBpi.getChildBpis().get(0).getChildBpis().get(0);
                                assertAll("Saved Grand-Child BPI",
                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId(), grandChildBpi.getId(),
                                        "Expected id to be '" + bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getId() + "' but was some other value."),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getId(), grandChildBpi.getBpiSnapshotData().getId(),
                                        "Expected id to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getId()+ "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getParentId(), grandChildBpi.getCustomerAccountId(),
                                        "Expected customerAccountId to be '" + expectedBpi.getSnapshotData().getParentId() + "' but was some other value."),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName(), grandChildBpi.getName(),
                                        "Expected name to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getName() + "' but was some other value."),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus(), grandChildBpi.getStatus(),
                                        "Expected status to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getStatus() + "' but was some other value."),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId(), grandChildBpi.getOfferingId(),
                                        "Expected offeringId to be '" + bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingId() + "' but was some other value."),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName(), grandChildBpi.getOfferingName(),
                                        "Expected offeringName to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getOfferingName() + "' but was some other value."),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory(), grandChildBpi.getProductCategory(),
                                       "Expected productCategory to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getProductCategory() + "' but was some other value."),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId(), grandChildBpi.getMarketId(),
                                        "Expected marketId to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketId() + "' but was some other value."),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName(), grandChildBpi.getMarketName(),
                                        "Expected marketName to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getMarketName() + "' but was some other value."),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen(), grandChildBpi.getCreatedWhen(),
                                        "Expected createdWhen to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getCreatedWhen() + "' but was some other value."),

                                        () -> assertEquals(bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId(), grandChildBpi.getBillingAccountId(),
                                        "Expected billingAccountId to be '"+ bpi.getSnapshotData().getChildBpis().get(0).getChildBpis().get(0).getBillingAccountId() + "' but was some other value."),

                                        () -> assertEquals(expectedBpi.getSnapshotData().getIsTest(), grandChildBpi.getIsTest(),
                                        "Expected isTest to be '"+ expectedBpi.getSnapshotData().getIsTest() + "' but was some other value."),

                                        () -> assertTrue(grandChildBpi.getChildBpis().size() == 0)
                                );
                            }
                    );
                }
        );

    }


    //##DELETE OPERATION
    @Test
    @Sql("/sql_scripts/bpi/create_bpi_with_child_bpi_and_grandchild_bpi.sql")
    public void persistBpiSnapshot_deletesBpiSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws Exception {

        //change operation type to "Delete"
        bpi.setOperationType("Delete");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(bpi)))
                .andExpect(status().isNoContent());

        //verify contact does not exist by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(bpiExists,
                "Expected bpi to not exist, but it still does.");

    }

    @Test
    @Sql("/sql_scripts/bpi/clear_bpis.sql")
    public void persistBpiSnapshot_deletesBpiSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExist () throws Exception {

        //change operation type to "Delete"
        bpi.setOperationType("Delete");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(bpi)))
                .andExpect(status().isNoContent());

        //verify contact does not exist by id
        boolean bpiExists = cimBpiRepository.existsById(bpi.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(bpiExists,
                "Expected bpi to not exist, but it still does.");

    }

    private String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
