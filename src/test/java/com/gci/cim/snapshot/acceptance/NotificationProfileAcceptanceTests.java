package com.gci.cim.snapshot.acceptance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.Application;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfile;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfileSnapshotData;
import com.gci.cim.snapshot.helper.NotificationProfilePersistenceHelper;
import com.gci.cim.snapshot.repository.CIMBadSnapshotRepository;
import com.gci.cim.snapshot.repository.CIMNotificationProfileRepository;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import javax.transaction.Transactional;
import java.io.File;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.jupiter.api.Assertions.*;

@Transactional
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
public class NotificationProfileAcceptanceTests {

    private NotificationProfile expectedNotificationProfile;
    private NotificationProfile notificationProfile;

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Autowired
    CIMNotificationProfileRepository cimNotificationProfileRepository;

    @Autowired
    NotificationProfilePersistenceHelper notificationProfilePersistenceHelper;

    @Autowired
    CIMBadSnapshotRepository cimBadSnapshotRepository;

    @BeforeEach
    public void setUp() throws Exception {

        expectedNotificationProfile =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_profile.json"), NotificationProfile.class);

        notificationProfile =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_profile.json"), NotificationProfile.class);
    }

    //##VALIDATION
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postBpiSnapshot_sendsSnapshotToBadSnapshotTable_whenBpiIdIsMissing() throws Exception {

        //set id field in snapshot data section to null
        notificationProfile.getSnapshotData().setId(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationProfile)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postBpiSnapshot_sendsSnapshotToBadSnapshotTable_whenOperationTypeIsNotCreateUpdateOrDelete() throws Exception {

        notificationProfile.setOperationType("Select");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationProfile)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postBpiSnapshot_sendsSnapshotToBadSnapshotTable_whenEmailChannelIsNotYesOrNo() throws Exception {

        notificationProfile.getSnapshotData().setEmailChannel("Maybe");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationProfile)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postBpiSnapshot_sendsSnapshotToBadSnapshotTable_whenMobileChannelIsNotYesOrNo() throws Exception {

        notificationProfile.getSnapshotData().setMobileChannel("Maybe");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationProfile)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    //##CREATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/notification_profile/clear_notification_profiles.sql")
    public void persistNotificationProfileSnapshot_createsNotificationProfile_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws Exception {

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationProfile)))
                .andExpect(status().isNoContent());

        //retrieve notification profile
        NotificationProfileSnapshotData savedNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).get();

        //TODO: Add messages for Saved Notification profile.
        //perform notification profile tests.
        assertAll("Saved Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), savedNotificationProfile.getId(),
                "Expected id to '"+ expectedNotificationProfile.getSnapshotData().getId() +"' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), savedNotificationProfile.getContact(),
                        "Expected contact to '"+ expectedNotificationProfile.getSnapshotData().getContact() +"' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), savedNotificationProfile.getNotificationType(),
                        "Expected notificationType to '"+ expectedNotificationProfile.getSnapshotData().getNotificationType() +"' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), savedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to '"+ expectedNotificationProfile.getSnapshotData().getEmailChannel() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), savedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to '"+ expectedNotificationProfile.getSnapshotData().getMobileChannel() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), savedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to '"+ expectedNotificationProfile.getSnapshotData().getCreatedWhen() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), savedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), savedNotificationProfile.getIsTest(),
                        "Expected isTest to '"+ expectedNotificationProfile.getSnapshotData().getIsTest() +"' but was some other value" )

        );

    }

    @Test
    @Sql("/sql_scripts/notification_profile/clear_notification_profiles.sql")
    public void persistContactSnapshot_createsContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws Exception {

        //setup snapshot
        notificationProfile.setOperationType("Update");
        expectedNotificationProfile.setOperationType("Update");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationProfile)))
                .andExpect(status().isNoContent());

        //retrieve notification profile
        NotificationProfileSnapshotData savedNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).get();

        //perform notification profile tests.
        assertAll("Saved Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), savedNotificationProfile.getId(),
                        "Expected id to '"+ expectedNotificationProfile.getSnapshotData().getId() +"' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), savedNotificationProfile.getContact(),
                        "Expected contact to '"+ expectedNotificationProfile.getSnapshotData().getContact() +"' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), savedNotificationProfile.getNotificationType(),
                        "Expected notificationType to '"+ expectedNotificationProfile.getSnapshotData().getNotificationType() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), savedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to '"+ expectedNotificationProfile.getSnapshotData().getEmailChannel() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), savedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to '"+ expectedNotificationProfile.getSnapshotData().getMobileChannel() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), savedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to '"+ expectedNotificationProfile.getSnapshotData().getCreatedWhen() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), savedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), savedNotificationProfile.getIsTest(),
                        "Expected isTest to '"+ expectedNotificationProfile.getSnapshotData().getIsTest() +"' but was some other value" )

        );
    }

    //##UPDATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/notification_profile/create_notification_profile_with_typos.sql")
    public void persistNotificationProfileSnapshot_updatesNotificationProfileSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist () throws Exception {

        //setup incoming notification profile
        notificationProfile.setOperationType("Update");
        expectedNotificationProfile.setOperationType("Update");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationProfile)))
                .andExpect(status().isNoContent());

        //retrieve notification profile
        NotificationProfileSnapshotData updatedNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).get();

        //perform notification profile tests.
        assertAll("Updated Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), updatedNotificationProfile.getId(),
                        "Expected id to '"+ expectedNotificationProfile.getSnapshotData().getId() +"' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), updatedNotificationProfile.getContact(),
                        "Expected contact to '"+ expectedNotificationProfile.getSnapshotData().getContact() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), updatedNotificationProfile.getNotificationType(),
                        "Expected notificationType to '"+ expectedNotificationProfile.getSnapshotData().getNotificationType() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), updatedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to '"+ expectedNotificationProfile.getSnapshotData().getEmailChannel() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), updatedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to '"+ expectedNotificationProfile.getSnapshotData().getMobileChannel() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), updatedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to '"+ expectedNotificationProfile.getSnapshotData().getCreatedWhen() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), updatedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), updatedNotificationProfile.getIsTest(),
                        "Expected isTest to '"+ expectedNotificationProfile.getSnapshotData().getIsTest() +"' but was some other value" )

        );

    }

    @Test
    @Sql("/sql_scripts/notification_profile/create_notification_profile_with_typos.sql")
    public void persistNotificationProfileSnapshot_updatesNotificationProfileSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist () throws Exception {

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationProfile)))
                .andExpect(status().isNoContent());

        //retrieve notification profile
        NotificationProfileSnapshotData updatedNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).get();

        //perform notification profile tests.
        assertAll("Updated Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), updatedNotificationProfile.getId(),
                        "Expected id to '"+ expectedNotificationProfile.getSnapshotData().getId() +"' but was some other value"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), updatedNotificationProfile.getContact(),
                        "Expected contact to '"+ expectedNotificationProfile.getSnapshotData().getContact() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), updatedNotificationProfile.getNotificationType(),
                        "Expected notificationType to '"+ expectedNotificationProfile.getSnapshotData().getNotificationType() +"' but was some other value"  ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), updatedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to '"+ expectedNotificationProfile.getSnapshotData().getEmailChannel() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), updatedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to '"+ expectedNotificationProfile.getSnapshotData().getMobileChannel() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), updatedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to '"+ expectedNotificationProfile.getSnapshotData().getCreatedWhen() +"' but was some other value" ),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), updatedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), updatedNotificationProfile.getIsTest(),
                        "Expected isTest to '"+ expectedNotificationProfile.getSnapshotData().getIsTest() +"' but was some other value" )

        );
    }

    //##DELETE OPERATION
    @Test
    @Sql("/sql_scripts/notification_profile/create_notification_profile.sql")
    public void persistNotificationProfileSnapshot_deletesNotificationProfileSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws Exception {

        //change operation type to "Delete"
        notificationProfile.setOperationType("Delete");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationProfile)))
                .andExpect(status().isNoContent());

        //verify contact does not exist by id
        boolean notificationProfileExists = cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(notificationProfileExists,
                "Expected notification profile to not exist, but it still does.");
    }

    @Test
    @Sql("/sql_scripts/notification_profile/clear_notification_profiles.sql")
    public void persistNotificationProfileSnapshot_deletesNotificationProfileSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws Exception {

        //change operation type to "Delete"
        notificationProfile.setOperationType("Delete");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(notificationProfile)))
                .andExpect(status().isNoContent());

        //verify contact does not exist by id
        boolean notificationProfileExists = cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(notificationProfileExists,
                "Expected notification profile to not exist, but it still does.");

    }

    private String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
