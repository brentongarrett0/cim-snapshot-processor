package com.gci.cim.snapshot.acceptance;
import com.gci.cim.snapshot.Application;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.MimeType;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
public class AdminAcceptanceTests {

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    private MockMvc mvc;

    @Test
    public void getVersion_getsVersion_whenMethodIsCalled () throws Exception {

        //Send REST request to /snapshot/admin/v1/version
        mvc.perform(MockMvcRequestBuilders.get("/snapshot/admin/v1/version"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.asMediaType(MimeType.valueOf("text/plain;charset=UTF-8"))))
                .andExpect(content().string("v0.1.0"));
    }

}
