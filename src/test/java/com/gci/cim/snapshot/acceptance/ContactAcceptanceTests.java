package com.gci.cim.snapshot.acceptance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.Application;
import com.gci.cim.snapshot.entity.contact.Contact;
import com.gci.cim.snapshot.entity.contact.ContactMethod;
import com.gci.cim.snapshot.entity.contact.ContactRole;
import com.gci.cim.snapshot.entity.contact.ContactSnapshotData;
import com.gci.cim.snapshot.repository.CIMBadSnapshotRepository;
import com.gci.cim.snapshot.repository.CIMContactRepository;
import com.gci.sdk.logging.EIPLogger;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import javax.transaction.Transactional;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
public class ContactAcceptanceTests {

    private static Contact expectedContact;
    private Contact contact;

    private WireMockServer wireMockServer;

    @Value("${com.gci.cim.wire-mock.port}")
    private int port;

    private final String REQUEST_URI = "/v2/mygci/invite";

    @MockBean
    EIPLogger eipLogger;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private CIMContactRepository cimContactRepository;

    @Autowired
    private CIMBadSnapshotRepository cimBadSnapshotRepository;

    @BeforeAll
    public static void setupExpectedValues() throws Exception{
        expectedContact =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
    }

    @BeforeEach
    public void setUp() throws Exception {

        contact = objectMapper.readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);

        //WireMock setup
        wireMockServer = new WireMockServer(options()
                .port(port)
        );
        wireMockServer.start();

    }

    @AfterEach
    public void tearDown() throws Exception{

        //wiremock teardown
        wireMockServer.stop();
    }


    //##VALIDATION
    //snapshotdata section missing
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postContactSnapshot_sendsSnapshotToBadSnapshotTable_whenSnapshotDataSectionIsNull() throws Exception {

        //remove snapshotdata section
        contact.setSnapshotData(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    //missing id in snapshotdata section
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postContactSnapshot_sendsSnapshotToBadSnapshotTable_whenCustomerAccountIdIsMissing() throws Exception {

        //set id field in snapshot data section to null
        contact.getSnapshotData().setId(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    //operation type not create, delete, or update.
    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postContactSnapshot_sendsSnapshotToBadSnapshotTable_whenSOperationTypeIsNotCreateUpdateOrDelete() throws Exception {

        //set id field in snapshot data section to null
        contact.setOperationType("Select");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postContactSnapshot_sendsSnapshotToBadSnapshotTable_whenFieldsInContactMethodAreNull_Id() throws Exception {

        //remove fields from billing account object
        contact.getSnapshotData().getContactMethods().get(0).setId(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postContactSnapshot_sendsSnapshotToBadSnapshotTable_whenFieldsInContactMethodAreNull_type() throws Exception {

        //remove fields from billing account object
        contact.getSnapshotData().getContactMethods().get(0).setType(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }

    @Test
    @Sql("/sql_scripts/clear_bad_snapshot_table.sql")
    public void postContactSnapshot_sendsSnapshotToBadSnapshotTable_whenFieldsInContactRoleAreNull_Id() throws Exception {

        //remove fields from billing account object
        contact.getSnapshotData().getContactRoles().get(0).setId(null);

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //check to see that bad snapshot table contains one record
        assertTrue(cimBadSnapshotRepository.count() == 1,
                "Expected bad snapshot table to contain exactly 1 record, but contained some other value.");

    }


    //## CREATE OPERATION
    @Test //Happy Path
    @Sql("/sql_scripts/contact/clear_contacts_and_contact_methods_and_contact_roles.sql")
    public void postContactSnapshot_createsContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist() throws Exception {

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo("/v2/mygci/invite"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //verify rest call was invoked 1 time
        wireMockServer.verify(1, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"" + contact.getSnapshotData().getContactId() + "\" }")));


        //Verify that the snapshot persisted to the database.
        assertTrue(cimContactRepository.existsById(contact.getSnapshotData().getId()),
                "Expected customer account to exist, but it did not.");

        //retrieve contact
        ContactSnapshotData savedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getId(),
                "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), savedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), savedContact.getStatus(),
                        "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), savedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), savedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), savedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), savedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), savedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getIsTest(),
                        "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                () -> {
                    assertTrue(savedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod savedEmailContactMethod = savedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(savedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), savedEmailContactMethod.getId(),
                                    "Expected id to be '"+ expectedEmailContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), savedEmailContactMethod.getType(),
                                    "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), savedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), savedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals((byte) 1, savedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, savedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, savedEmailContactMethod.getPreferred(),
                                    "Expected preffered to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), savedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactMethods().get(0).getContactSnapshotData().getId());
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod savedMobileContactMethod = savedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(savedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), savedMobileContactMethod.getId(),
                                    "Expected id to be '"+ expectedMobileContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), savedMobileContactMethod.getType(),
                                    "Expected type to be '"+ expectedMobileContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), savedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedMobileContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), savedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedMobileContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals(null, savedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, savedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, savedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), savedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedMobileContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactMethods().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                               "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value." );

                            }
                    );

                },
                () -> {
                    assertTrue(savedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), savedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+ expectedContactRole.getId() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), savedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+ expectedContactRole.getCustomerAccountId() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getRole(), savedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+ expectedContactRole.getRole() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), savedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedContactRole.getCreatedWhen() +"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactRoles().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                               "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value." );
                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/contact/clear_contacts_and_contact_methods_and_contact_roles.sql")
    public void persistContactSnapshot_createsCustomerAccountSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws Exception {

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo("/v2/mygci/invite"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        //Change operation type to 'Update'
        contact.setOperationType("Update");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //verify rest call was invoked 1 time
        wireMockServer.verify(1, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"" + contact.getSnapshotData().getContactId() + "\" }")));

        //retrieve contact
        ContactSnapshotData savedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //TODO: Add messages for Saved Contact tests.
        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), savedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), savedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), savedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), savedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), savedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), savedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), savedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value." ),

                () -> {
                    assertTrue(savedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod savedEmailContactMethod = savedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(savedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), savedEmailContactMethod.getId(),
                                    "Expected id to be '"+ expectedEmailContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), savedEmailContactMethod.getType(),
                                    "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), savedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), savedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals((byte) 1, savedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, savedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, savedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), savedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactMethods().get(0).getContactSnapshotData().getId());
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod savedMobileContactMethod = savedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(savedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), savedMobileContactMethod.getId(),
                                    "Expected id to be '"+ expectedMobileContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), savedMobileContactMethod.getType(),
                                    "Expected type to be '"+ expectedMobileContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), savedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedMobileContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), savedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedMobileContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals(null, savedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, savedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, savedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), savedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedMobileContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), savedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactMethods().get(0).getContactSnapshotData().getId());
                            }
                    );

                },
                () -> {
                    assertTrue(savedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), savedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+ expectedContactRole.getId() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), savedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+ expectedContactRole.getCustomerAccountId() +"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getRole(), savedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+ expectedContactRole.getRole() +"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), savedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedContactRole.getCreatedWhen() +"', but was some other value."),

                            () -> {
                                assertTrue(savedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );
                }
        );
    }

    //##UPDATE OPERATION
    @Test //**Happy Path
    @Sql("/sql_scripts/contact/create_contact_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleIsMyGciUserAndTriggersCiamApiCall () throws Exception {

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo("/v2/mygci/invite"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        //setup incoming customer account
        contact.setOperationType("Update");

        //persist contact
        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
            .content(asJsonString(contact)))
            .andExpect(status().isNoContent());

        //verify rest call was invoked 1 time
        wireMockServer.verify(1, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"" + contact.getSnapshotData().getContactId() + "\" }")));


        //retrieve contact
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value." ) ,

                /*title*/() -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value." ),

                () -> {
                    assertTrue(updatedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod updatedEmailContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(updatedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), updatedEmailContactMethod.getId(),
                                    "Expected id to be '"+ expectedEmailContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), updatedEmailContactMethod.getType(),
                                    "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), updatedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), updatedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, updatedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), updatedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                "Expected contactMethods to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod updatedMobileContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(updatedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), updatedMobileContactMethod.getId(),
                                    "Expected id to be '"+ expectedMobileContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), updatedMobileContactMethod.getType(),
                                    "Expected type to be '"+ expectedMobileContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), updatedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+1+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), updatedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), updatedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");

                            }
                    );

                },
                () -> {
                    assertTrue(updatedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), updatedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), updatedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getRole(), updatedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+expectedContactRole.getRole()+"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), updatedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedContactRole.getCreatedWhen()+"', but was some other value." ),

                            () -> {
                                assertTrue(updatedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/contact/create_contact_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleNotIsMyGciUserAndDoesNotTriggersCiamApiCall () throws Exception {

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo("/v2/mygci/invite"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        //setup incoming customer account
        contact.setOperationType("Update");

        contact.getSnapshotData().setContactRoles(new ArrayList<ContactRole>(){{
            add(ContactRole.builder()
                    .id("9155603678013761475")
                    .customerAccountId("9155603678013761469")
                    .role("ACS User")
                    .createdWhen(Timestamp.valueOf("2019-11-07 10:14:49"))
                    .build());
        }});

        //persist contact
        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //verify rest call was invoked 1 time
        wireMockServer.verify(0, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"" + contact.getSnapshotData().getContactId() + "\" }")));


        //retrieve contact
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value." ),

                () -> {
                    assertTrue(updatedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod updatedEmailContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(updatedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), updatedEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), updatedEmailContactMethod.getType(),
                                    "Expected type to be '"+expectedEmailContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), updatedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), updatedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, updatedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), updatedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be 'expectedEmailContactMethod.getCreatedWhen()', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be 'expectedContact.getSnapshotData().getIsTest()', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod updatedMobileContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(updatedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), updatedMobileContactMethod.getId(),
                                    "Expected id to be '"+expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), updatedMobileContactMethod.getType(),
                                    "Expected type to be '"+expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), updatedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), updatedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), updatedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");

                            }
                    );

                },
                () -> {
                    assertTrue(updatedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), updatedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), updatedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value." ),

                            () -> assertEquals("ACS User", updatedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be 'ACS User', but was some other value."),

                            () -> assertEquals(Timestamp.valueOf("2019-11-07 10:14:49"), updatedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '2019-11-07 10:14:49', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/contact/create_contact_and_contact_methods_and_contact_role_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewContactMethodsNull_NewContactRolesNull_contactRoleDoesNotExistAndDoesNotTriggersCiamApiCall () throws Exception {

        List<LoggedRequest> requests = wireMockServer.findAll(postRequestedFor(urlMatching("/v2/mygci/invite")));
        System.out.println("**request log before: " + requests);

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo("/v2/mygci/invite"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        //setup incoming customer account
        contact.setOperationType("Update");
        contact.getSnapshotData().setContactMethods(null);
        contact.getSnapshotData().setContactRoles(null);

        //persist customer account
        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        List<LoggedRequest> requestsAfter = wireMockServer.findAll(postRequestedFor(urlMatching("/v2/mygci/invite")));
        System.out.println("**request log after: " + requestsAfter);

        //verify rest call was invoked 0 times
        wireMockServer.verify(0, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"" + contact.getSnapshotData().getContactId() + "\" }")));


        //retrieve customer account
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value." ),

                () -> assertTrue(updatedContact.getContactMethods().size() == 0),
                () -> assertTrue(updatedContact.getContactRoles().size() == 0)
        );

    }

    @Test
    @Sql("/sql_scripts/contact/create_contact_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleIsMyGciUserAndTriggersCiamApiCall () throws Exception {

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo("/v2/mygci/invite"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        System.out.println("** [Contact Acceptance Test - line 987] wire mock is running " + wireMockServer.isRunning());

        //persist contact
        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //verify rest call was invoked 1 time
        wireMockServer.verify(1, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"" + contact.getSnapshotData().getContactId() + "\" }")));


        //retrieve contact
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value." ),

                () -> {
                    assertTrue(updatedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod updatedEmailContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(updatedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), updatedEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), updatedEmailContactMethod.getType(),
                                    "Expected type to be '"+expectedEmailContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), updatedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), updatedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPrimaryEmail(),
                                    "Expected ### to be '1', but was some other value."),

                            () -> assertEquals(null, updatedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), updatedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedEmailContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod updatedMobileContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(updatedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), updatedMobileContactMethod.getId(),
                                    "Expected id to be '"+expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), updatedMobileContactMethod.getType(),
                                    "Expected type to be '"+expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), updatedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), updatedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), updatedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                               "" );
                            }
                    );

                },
                () -> {
                    assertTrue(updatedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), updatedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), updatedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getRole(), updatedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+expectedContactRole.getRole()+"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), updatedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedContactRole.getCreatedWhen()+"', but was some other value." ),

                            () -> {
                                assertTrue(updatedContact.getContactRoles().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."  );
                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/contact/create_contact_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleIsNotMyGciUserAndDoesNotTriggersCiamApiCall () throws Exception {

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo("/v2/mygci/invite"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        //setup snapshot
        contact.getSnapshotData().setContactRoles(new ArrayList<ContactRole>(){{
            add(ContactRole.builder()
                    .id("9155603678013761475")
                    .customerAccountId("9155603678013761469")
                    .role("ACS User")
                    .createdWhen(Timestamp.valueOf("2019-11-07 10:14:49"))
                    .build());
        }});

        //persist contact
        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //verify rest call was invoked 1 time
        wireMockServer.verify(0, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"" + contact.getSnapshotData().getContactId() + "\" }")));


        //retrieve contact
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value." ),

                () -> {
                    assertTrue(updatedContact.getContactMethods().size() == 2, "2 contact methods expected");

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod updatedEmailContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(updatedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), updatedEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), updatedEmailContactMethod.getType(),
                                    "Expected type to be '"+expectedEmailContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), updatedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), updatedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, updatedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), updatedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedEmailContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod updatedMobileContactMethod = updatedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(updatedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), updatedMobileContactMethod.getId(),
                                    "Expected id to be '"+expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), updatedMobileContactMethod.getType(),
                                    "Expected type to be '"+expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), updatedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), updatedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, updatedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, updatedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), updatedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+expectedMobileContactMethod.getCreatedWhen()+"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(updatedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );

                },
                () -> {
                    assertTrue(updatedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), updatedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+expectedContactRole.getId()+"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), updatedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId()+"', but was some other value." ),

                            () -> assertEquals("ACS User", updatedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be 'ACS User', but was some other value." ),

                            () -> assertEquals(Timestamp.valueOf("2019-11-07 10:14:49"), updatedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '2019-11-07 10:14:49', but was some other value." ),

                            () -> {
                                assertTrue(updatedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value.");
                            }
                    );
                }
        );
    }

    @Test
    @Sql("/sql_scripts/contact/create_contact_and_contact_methods_and_contact_role_with_incorrect_values.sql")
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewContactMethodsNull_NewContactRolesNull_contactRoleDoesNotExistAndDoesNotTriggersCiamApiCall () throws Exception {

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo("/v2/mygci/invite"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        //setup incoming customer account
        contact.getSnapshotData().setContactMethods(null);
        contact.getSnapshotData().setContactRoles(null);

        //persist customer account
        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //verify rest call was invoked 1 time
        wireMockServer.verify(0, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"" + contact.getSnapshotData().getContactId() + "\" }")));


        //retrieve customer account
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), updatedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), updatedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), updatedContact.getStatus(),
                        "Expected status to be '" + expectedContact.getSnapshotData().getStatus() + "', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), updatedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), updatedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), updatedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), updatedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), updatedContact.getTitle(),
                        "Expected title to be '"+expectedContact.getSnapshotData().getTitle()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), updatedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                () -> assertTrue(updatedContact.getContactMethods().size() == 0),
                () -> assertTrue(updatedContact.getContactRoles().size() == 0)
        );
    }

    //##DELETE OPERATION
    @Test
    @Sql("/sql_scripts/contact/create_contact_with_contact_method_and_contact_role.sql")
    public void persistContactSnapshot_deletesContactSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws Exception {

        //change operation type to "Delete"
        contact.setOperationType("Delete");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //verify contact does not exist by id
        boolean contactExists = cimContactRepository.existsById(contact.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(contactExists,
                "Expected contact to not exist, but it still does.");

    }

    @Test
    @Sql("/sql_scripts/contact/clear_contacts_and_contact_methods_and_contact_roles.sql")
    public void persistContactSnapshot_deletesContactSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws Exception {

        //change operation type to "Delete"
        contact.setOperationType("Delete");

        //Send REST request to /snapshot/v1/submit endpoint
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //verify contact does not exist by id
        boolean contactExists = cimContactRepository.existsById(contact.getSnapshotData().getId());

        //verify customer account does not exist
        assertFalse(contactExists,
                "Expected contact to not exist, but it still does.");

    }

    @Test //Happy Path
    @Sql("/sql_scripts/contact/clear_contacts_and_contact_methods_and_contact_roles.sql")
    public void postTwoContactSnapshotsWithOldChangeTimestamp_createsFirstContactButNotUpdatesSecond_whenSecondSnapshotChangeTimestampIsOlderThanFirst() throws Exception {

        // We need a second contact, this is same line that creates the first contact just cut and paste
        Contact contact2 = objectMapper.readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
        contact2.getChangeTimestamp().setTime((contact.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        contact2.getSnapshotData().setFirstName("Oldie");
        contact2.getSnapshotData().setLastName("Oldbert");

        //setup call to ciam api
        wireMockServer.stubFor(post(urlEqualTo("/v2/mygci/invite"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"hello\":\"world\"}")));

        //Send REST request to /snapshot/v1/submit endpoint - contact
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact)))
                .andExpect(status().isNoContent());

        //Send REST request to /snapshot/v1/submit endpoint - contact2
        mvc.perform(MockMvcRequestBuilders.post("/snapshot/v1/submit")
                .content(asJsonString(contact2)))
                .andExpect(status().isNoContent());

        //verify rest call was invoked 1 time
        wireMockServer.verify(1, postRequestedFor(urlEqualTo(REQUEST_URI))
                .withHeader("Content-Type",containing("application/json"))
                .withRequestBody(equalToJson("{ \"contactId\": \"" + contact.getSnapshotData().getContactId() + "\" }")));

        //Verify that the snapshot persisted to the database.
        assertTrue(cimContactRepository.existsById(contact.getSnapshotData().getId()),
                "Expected customer account to exist, but it did not.");

        //retrieve contact
        ContactSnapshotData savedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), savedContact.getId(),
                        "Expected id to be '"+expectedContact.getSnapshotData().getId()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), savedContact.getContactId(),
                        "Expected contactId to be '"+expectedContact.getSnapshotData().getContactId()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), savedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), savedContact.getFirstName(),
                        "Expected firstName to be '"+expectedContact.getSnapshotData().getFirstName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), savedContact.getLastName(),
                        "Expected lastName to be '"+expectedContact.getSnapshotData().getLastName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), savedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), savedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value.")

        );
    }


    private String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
