package com.gci.cim.snapshot.unit.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.contact.Contact;
import com.gci.cim.snapshot.entity.contact.ContactMethod;
import com.gci.cim.snapshot.entity.contact.ContactRole;
import com.gci.cim.snapshot.entity.contact.ContactSnapshotData;
import com.gci.cim.snapshot.helper.CIAMAPIRegistrationHelper;
import com.gci.cim.snapshot.helper.ContactPersistenceHelper;
import com.gci.cim.snapshot.repository.CIMContactRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import static java.util.Optional.ofNullable;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContactPersistenceHelperUnitTests {

    private static Contact expectedContact;
    private Contact contact;

    @Mock
    ObjectMapper objectMapper;

    @Mock //dependency to mock
    CIMContactRepository cimContactRepository;

    @Mock
    private CIAMAPIRegistrationHelper ciamapiRegistrationHelper;

    @InjectMocks //class you are testing
    ContactPersistenceHelper contactPersistenceHelper;

    @Captor //used to capture the value sent to a method into a pojo.
    private ArgumentCaptor<ContactSnapshotData> savedContact;

    @BeforeAll
    public static void setupExpectedValues() throws Exception{
        expectedContact =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
    }

    @BeforeEach
    public void setUp() throws Exception {
        contact =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
    }

    //##VALIDATION
    @Test //null checking
    public void persistContactSnapshot_doesNothing_whenContactInputIsNull() throws JsonProcessingException {

        //save contact
        contactPersistenceHelper.persistContactSnapshot(null);

        //verify .save() was never invoked
        verify(cimContactRepository, times(0)).save(any(ContactSnapshotData.class));

        //verify .delete() was never invoked
        verify(cimContactRepository, times(0)).delete(any(ContactSnapshotData.class));
    }

    //##CREATE OPERATION
    @Test //**Happy Path
    public void persistContactSnapshot_createsContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist_contactRoleIsMyGciUserAndTriggersCiamApiCall () throws IOException {

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(contact);
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(false);
        when(cimContactRepository.save(any(ContactSnapshotData.class))).thenReturn(null);
        when(cimContactRepository.findById(contact.getSnapshotData().getId())).thenReturn(ofNullable(contact.getSnapshotData())); //**

        //save customer account
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .save() was invoked 1 time
        verify(cimContactRepository, times(1)).save(any(ContactSnapshotData.class));

        //capture contact sent to .save()
        verify(cimContactRepository).save(savedContact.capture());
        ContactSnapshotData capturedContact = savedContact.getValue();

        //perform contact tests.
        assertAll("Saved Contact",
            () -> assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getId(),
            "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."),

            () -> assertEquals(expectedContact.getSnapshotData().getContactId(), capturedContact.getContactId(),
                    "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value."),

            () -> assertEquals(expectedContact.getSnapshotData().getStatus(), capturedContact.getStatus(),
                    "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value."),

            () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), capturedContact.getFirstName(),
                    "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value."),

            () -> assertEquals(expectedContact.getSnapshotData().getLastName(), capturedContact.getLastName(),
                    "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value." ),

            () -> assertEquals(expectedContact.getSnapshotData().getPreferredContactMethod(), capturedContact.getPreferredContactMethod(),
                    "Expected preferredContactMethod to be '"+ expectedContact.getSnapshotData().getPreferredContactMethod() +"', but was some other value." ),

            () -> assertEquals(expectedContact.getSnapshotData().getNotificationEmail(), capturedContact.getNotificationEmail(),
                    "Expected notificationEmail to be '"+ expectedContact.getSnapshotData().getNotificationEmail() +"', but was some other value." ),

            () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), capturedContact.getCreatedWhen(),
                    "Expected createdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value." ),

            () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), capturedContact.getCustomerAccountId(),
                    "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value." ) ,

            () -> assertEquals(expectedContact.getSnapshotData().getTitle(), capturedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value."     ),

            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getIsTest(),
                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value." ),

            () -> {
                assertTrue(capturedContact.getContactMethods().size() == 2);

                //Email contact method
                final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                        .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                final ContactMethod capturedEmailContactMethod = capturedContact.getContactMethods()
                        .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                assertTrue(capturedEmailContactMethod != null);

                assertAll("Email Contact Method (Preferred)",
                    () -> assertEquals(expectedEmailContactMethod.getId(), capturedEmailContactMethod.getId(),
                            "Expected id to be '"+ expectedEmailContactMethod.getId() +"', but was some other value."),

                    () -> assertEquals(expectedEmailContactMethod.getType(), capturedEmailContactMethod.getType(),
                            "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                    () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), capturedEmailContactMethod.getPhoneNumber(),
                            "Expected phoneNumber to be '"+ expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                    () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), capturedEmailContactMethod.getEmailAddress(),
                            "Expected emailAddress to be '"+ expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                    () -> assertEquals((byte) 1, capturedEmailContactMethod.getPrimaryEmail(),
                            "Expected primaryEmail to be '1', but was some other value."),

                    () -> assertEquals(null, capturedEmailContactMethod.getPrimaryPhone(),
                            "Expected primaryPhone to be 'null', but was some other value."),

                    () -> assertEquals((byte) 1, capturedEmailContactMethod.getPreferred(),
                            "Expected preferred to be '1', but was some other value."),

                    () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), capturedEmailContactMethod.getCreatedWhen(),
                            "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                    () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactMethods().get(0).getIsTest(),
                            "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                    () -> {
                        assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                        assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId());
                    }
                );

                //Mobile Contact Method
                final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                        .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                final ContactMethod capturedMobileContactMethod = capturedContact.getContactMethods()
                        .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                assertTrue(capturedMobileContactMethod != null);

                assertAll("Contact Mobile Method Tests",
                        () -> assertEquals(expectedMobileContactMethod.getId(), capturedMobileContactMethod.getId(),
                                "Expected id to be '"+ expectedMobileContactMethod.getId() +"', but was some other value."),

                        () -> assertEquals(expectedMobileContactMethod.getType(), capturedMobileContactMethod.getType(),
                                "Expected type to be '"+ expectedMobileContactMethod.getType() +"', but was some other value."),

                        () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), capturedMobileContactMethod.getPhoneNumber(),
                                "Expected phoneNumber to be '"+ expectedMobileContactMethod.getPhoneNumber() +"', but was some other value."),

                        () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), capturedMobileContactMethod.getEmailAddress(),
                                "Expected emailAddress to be '"+ expectedMobileContactMethod.getEmailAddress() +"', but was some other value."),

                        () -> assertEquals(null, capturedMobileContactMethod.getPrimaryEmail(),
                                "Expected primaryEmail to be 'null', but was some other value."),

                        () -> assertEquals((byte) 1, capturedMobileContactMethod.getPrimaryPhone(),
                                "Expected primaryPhone to be '1', but was some other value."),

                        () -> assertEquals(null, capturedMobileContactMethod.getPreferred(),
                                "Expected preferred to be 'null', but was some other value."),

                        () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), capturedMobileContactMethod.getCreatedWhen(),
                                "Expected createdWhen to be 'expectedMobileContactMethod.getCreatedWhen()', but was some other value."),

                        () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedMobileContactMethod.getIsTest(),
                                "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                        () -> {
                            assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                            assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId());
                        }
                );

            },
            () -> {
                assertTrue(capturedContact.getContactRoles().size() == 1);

                //Contact Roles tests (only tested if above assertion returns true)
                final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                assertAll("Contact Role Tests",
                        () -> assertEquals(expectedContactRole.getId(), capturedContact.getContactRoles().get(0).getId(),
                                "Expected id to be '"+ expectedContactRole.getId() +"', but was some other value."),

                        () -> assertEquals(expectedContactRole.getCustomerAccountId(), capturedContact.getContactRoles().get(0).getCustomerAccountId(),
                                "Expected customerAccountId to be '"+ expectedContactRole.getCustomerAccountId() +"', but was some other value."),

                        () -> assertEquals(expectedContactRole.getRole(), capturedContact.getContactRoles().get(0).getRole(),
                                "Expected role to be '"+ expectedContactRole.getRole() +"', but was some other value."),

                        () -> assertEquals(expectedContactRole.getCreatedWhen(), capturedContact.getContactRoles().get(0).getCreatedWhen(),
                                "Expected createdWhen to be '"+ expectedContactRole.getCreatedWhen() +"', but was some other value."),

                        () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactRoles().get(0).getIsTest(),
                                "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                        () -> {
                            assertTrue(capturedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                            assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                    "Expected contactRoles to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."
                            );
                        }
                );
            }
        );

    }

    @Test
    public void persistContactSnapshot_createsContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist_contactRoleIsNotMyGciUserAndDoesNotTriggersCiamApiCall () throws IOException {

        //setup contact
        contact.getSnapshotData().setContactRoles(new ArrayList<ContactRole>(){{
            add(ContactRole.builder()
                    .id("9155603678013761475")
                    .customerAccountId("9155603678013761469")
                    .role("ACS User")
                    .createdWhen(Timestamp.valueOf("2019-11-07 10:14:49"))
                    .build());
        }});

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(contact);
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(false);
        when(cimContactRepository.save(any(ContactSnapshotData.class))).thenReturn(null);
        when(cimContactRepository.findById(contact.getSnapshotData().getId())).thenReturn(ofNullable(contact.getSnapshotData())); //**


        //save customer account
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .save() was invoked 1 time
        verify(cimContactRepository, times(1)).save(any(ContactSnapshotData.class));

        //verify ciam api registation helper maethod was invoked 1 time
        //verify(ciamapiRegistrationHelper, times(0)).sendInviteRequest(any(String.class));

        //capture contact sent to .save()
        verify(cimContactRepository).save(savedContact.capture());
        ContactSnapshotData capturedContact = savedContact.getValue();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), capturedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), capturedContact.getStatus(),
                        "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), capturedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), capturedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getPreferredContactMethod(), capturedContact.getPreferredContactMethod(),
                        "Expected preferedContactMethod to be '"+ expectedContact.getSnapshotData().getPreferredContactMethod() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getNotificationEmail(), capturedContact.getNotificationEmail(),
                        "Expected notificationEmail to be '"+ expectedContact.getSnapshotData().getNotificationEmail() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), capturedContact.getCreatedWhen(),
                        "Expected createdwhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), capturedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), capturedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getIsTest(),
                        "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                () -> {
                    assertTrue(capturedContact.getContactMethods().size() == 2);

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod capturedEmailContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(capturedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), capturedEmailContactMethod.getId(),
                                    "Expected id to be '"+ expectedEmailContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), capturedEmailContactMethod.getType(),
                                    "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), capturedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), capturedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPrimaryEmail(),
                                    "Expected ### to be '1', but was some other value."),

                            () -> assertEquals(null, capturedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), capturedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId()
                                );
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod capturedMobileContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(capturedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), capturedMobileContactMethod.getId(),
                                    "Expected id to be '"+ expectedMobileContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), capturedMobileContactMethod.getType(),
                                    "Expected type to be '"+ expectedMobileContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), capturedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedMobileContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), capturedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedMobileContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '"+ 1 +"', but was some other value."),

                            () -> assertEquals((byte) 1, capturedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), capturedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ 1 +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedMobileContactMethod.getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."
                                );
                            }
                    );

                },
                () -> {
                    assertTrue(capturedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), capturedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+ expectedContactRole.getId() +"', but was some other value." ),
                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), capturedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+ expectedContactRole.getCustomerAccountId() +"', but was some other value." ),

                            () -> assertEquals("ACS User", capturedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be 'ACS USer', but was some other value." ),

                            () -> assertEquals(Timestamp.valueOf("2019-11-07 10:14:49"), capturedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '2019-11-07 10:14:49', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."
                                );
                            }
                    );
                }
        );

    }

    @Test
    public void persistContactSnapshot_createsContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist_contactRoleIsMyGciUserAndTriggersCiamApiCall () throws JsonProcessingException {

        //setup incoming contact
        contact.setOperationType("Update");

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(contact);
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(false);
        when(cimContactRepository.save(any(ContactSnapshotData.class))).thenReturn(null);
        when(cimContactRepository.findById(contact.getSnapshotData().getId())).thenReturn(ofNullable(contact.getSnapshotData())); //**
        //doNothing().when(ciamapiRegistrationHelper).sendInviteRequest(any(String.class));

        //save customer account
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .save() was invoked 1 time
        verify(cimContactRepository, times(1)).save(any(ContactSnapshotData.class));

        //verify ciam api registration helper maethod was invoked 1 time
        //verify(ciamapiRegistrationHelper, times(1)).sendInviteRequest(any(String.class));

        //capture contact sent to .save()
        verify(cimContactRepository).save(savedContact.capture());
        ContactSnapshotData capturedContact = savedContact.getValue();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), capturedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), capturedContact.getStatus(),
                        "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), capturedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), capturedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getPreferredContactMethod(), capturedContact.getPreferredContactMethod(),
                        "Expected preferredContactMethod to be '"+ expectedContact.getSnapshotData().getPreferredContactMethod() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getNotificationEmail(), capturedContact.getNotificationEmail(),
                        "Expected notificationEmail to be '"+ expectedContact.getSnapshotData().getNotificationEmail() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), capturedContact.getCreatedWhen(),
                        "Expected creatdWhen to be '"+ 1 +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), capturedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), capturedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getIsTest(),
                        "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value." ),

                () -> {
                    assertTrue(capturedContact.getContactMethods().size() == 2);

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod capturedEmailContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(capturedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), capturedEmailContactMethod.getId(),
                                    "Expected id to be '"+expectedEmailContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), capturedEmailContactMethod.getType(),
                                    "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), capturedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), capturedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, capturedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), capturedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId()
                                );
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod capturedMobileContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(capturedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), capturedMobileContactMethod.getId(),
                                    "Expected id to be '"+ expectedMobileContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), capturedMobileContactMethod.getType(),
                                    "Expected type to be '"+ expectedMobileContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), capturedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedMobileContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), capturedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedMobileContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), capturedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedMobileContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedMobileContactMethod.getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."
                                );
                            }
                    );

                },
                () -> {
                    assertTrue(capturedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), capturedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+ expectedContactRole.getId() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), capturedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+ expectedContactRole.getCustomerAccountId() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getRole(), capturedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+ expectedContactRole.getRole() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), capturedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedContactRole.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactRoles().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );
                }
        );

    }

    @Test
    public void persistContactSnapshot_createsContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist_contactRoleIsNotMyGciUserAndDoesNotTriggersCiamApiCall () throws IOException {

        //setup contact
        contact.setOperationType("Update");
        contact.getSnapshotData().setContactRoles(new ArrayList<ContactRole>(){{
            add(ContactRole.builder()
                    .id("9155603678013761475")
                    .customerAccountId("9155603678013761469")
                    .role("ACS User")
                    .createdWhen(Timestamp.valueOf("2019-11-07 10:14:49"))
                    .build());
        }});

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(contact);
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(false);
        when(cimContactRepository.save(any(ContactSnapshotData.class))).thenReturn(null);
        when(cimContactRepository.findById(contact.getSnapshotData().getId())).thenReturn(ofNullable(contact.getSnapshotData())); //**

        //save customer account
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .save() was invoked 1 time
        verify(cimContactRepository, times(1)).save(any(ContactSnapshotData.class));

        //verify ciam api registation helper method was invoked 1 time
        //verify(ciamapiRegistrationHelper, times(0)).sendInviteRequest(any(String.class));

        //capture contact sent to .save()
        verify(cimContactRepository).save(savedContact.capture());
        ContactSnapshotData capturedContact = savedContact.getValue();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), capturedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), capturedContact.getStatus(),
                        "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), capturedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), capturedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getPreferredContactMethod(), capturedContact.getPreferredContactMethod(),
                        "Expected preferredContactMethod to be '"+ expectedContact.getSnapshotData().getPreferredContactMethod() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getNotificationEmail(), capturedContact.getNotificationEmail(),
                        "Expected notificaitonEmail to be '"+ expectedContact.getSnapshotData().getNotificationEmail() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), capturedContact.getCreatedWhen(),
                        "Expected creatdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), capturedContact.getCustomerAccountId(),
                        "Expected customerAcocuntId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value.") ,

                /*title*/() -> assertEquals(expectedContact.getSnapshotData().getTitle(), capturedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getIsTest(),
                        "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value." ),

                () -> {
                    assertTrue(capturedContact.getContactMethods().size() == 2);

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod capturedEmailContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(capturedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), capturedEmailContactMethod.getId(),
                                    "Expected id to be '"+ expectedEmailContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), capturedEmailContactMethod.getType(),
                                    "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), capturedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), capturedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, capturedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), capturedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value." );
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod capturedMobileContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(capturedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), capturedMobileContactMethod.getId(),
                                    "Expected id to be '"+ expectedMobileContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), capturedMobileContactMethod.getType(),
                                    "Expected type to be '"+ expectedMobileContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), capturedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedMobileContactMethod.getPhoneNumber()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), capturedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedMobileContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '"+ 1 +"', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), capturedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedMobileContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedMobileContactMethod.getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest()+"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );

                },
                () -> {
                    assertTrue(capturedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), capturedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+ expectedContactRole.getId() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), capturedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+ expectedContactRole.getCustomerAccountId() +"', but was some other value."),

                            () -> assertEquals("ACS User", capturedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be 'ACS User', but was some other value."),

                            () -> assertEquals(Timestamp.valueOf("2019-11-07 10:14:49"), capturedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '2019-11-07 10:14:49', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactRoles().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value." );
                            }
                    );
                }
        );

    }

    //##UPDATE OPERATION
    @Test //**Happy Path
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleIsMyGciUserAndTriggersCiamApiCall () throws IOException {

        //setup existing account
        Contact existingContact =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
        existingContact.getSnapshotData().setChangeTimestamp(new Timestamp(contact.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        existingContact.getSnapshotData().setFirstName("Samewise");
        existingContact.getSnapshotData().setLastName("Gamgee");
        existingContact.getSnapshotData().getContactMethods().clear();
        existingContact.getSnapshotData().getContactRoles().clear();

        //setup incoming contact
        contact.setOperationType("Update");

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(contact);
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(true);
        when(cimContactRepository.findById(contact.getSnapshotData().getId())).thenReturn(ofNullable(existingContact.getSnapshotData()));
        when(cimContactRepository.save(any(ContactSnapshotData.class))).thenReturn(null);
        //doNothing().when(ciamapiRegistrationHelper).sendInviteRequest(any(String.class));

        //save contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .save() was invoked 1 time
        verify(cimContactRepository, times(1)).save(any(ContactSnapshotData.class));

        //verify ciam api registation helper method was invoked 1 time
        //verify(ciamapiRegistrationHelper, times(1)).sendInviteRequest(any(String.class));

        //capture contact sent to .save()
        verify(cimContactRepository).save(savedContact.capture());
        ContactSnapshotData capturedContact = savedContact.getValue();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), capturedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), capturedContact.getStatus(),
                        "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), capturedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), capturedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getPreferredContactMethod(), capturedContact.getPreferredContactMethod(),
                        "Expected preferredContactMethod to be '"+ expectedContact.getSnapshotData().getPreferredContactMethod()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getNotificationEmail(), capturedContact.getNotificationEmail(),
                        "Expected notificationEmail to be '"+ expectedContact.getSnapshotData().getNotificationEmail() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), capturedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), capturedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), capturedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getIsTest(),
                        "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                () -> {
                    assertTrue(capturedContact.getContactMethods().size() == 2);

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod capturedEmailContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(capturedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), capturedEmailContactMethod.getId(),
                                    "Expected id to be '"+ expectedEmailContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), capturedEmailContactMethod.getType(),
                                    "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), capturedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), capturedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, capturedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), capturedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod capturedMobileContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(capturedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), capturedMobileContactMethod.getId(),
                                    "Expected id to be '"+ expectedMobileContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), capturedMobileContactMethod.getType(),
                                    "Expected type to be '"+ expectedMobileContactMethod.getType()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), capturedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedMobileContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), capturedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedMobileContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedMobileContactMethod.getPrimaryPhone(),
                                    "primary phone"),

                            () -> assertEquals(null, capturedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), capturedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedMobileContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedMobileContactMethod.getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );

                },
                () -> {
                    assertTrue(capturedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), capturedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+ expectedContactRole.getId() +"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), capturedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+ expectedContactRole.getCustomerAccountId() +"', but was some other value.")
                            ,

                            () -> assertEquals(expectedContactRole.getRole(), capturedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+ expectedContactRole.getRole() +"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), capturedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedContactRole.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value." ),

                            () -> {
                                assertTrue(capturedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value." );
                            }
                    );
                }
        );

    }

    @Test
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleNotIsMyGciUserAndDoesNotTriggersCiamApiCall () throws IOException {

        //setup existing account
        Contact existingContact =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
        existingContact.getSnapshotData().setChangeTimestamp(new Timestamp(contact.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        existingContact.getSnapshotData().setFirstName("Samewise");
        existingContact.getSnapshotData().setLastName("Gamgee");
        existingContact.getSnapshotData().getContactMethods().clear();
        existingContact.getSnapshotData().getContactRoles().clear();

        //setup incoming contact
        contact.setOperationType("Update");
        contact.getSnapshotData().setContactRoles(new ArrayList<ContactRole>(){{
            add(ContactRole.builder()
                    .id("9155603678013761475")
                    .customerAccountId("9155603678013761469")
                    .role("ACS User")
                    .createdWhen(Timestamp.valueOf("2019-11-07 10:14:49"))
                    .build());
        }});

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(contact);
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(true);
        when(cimContactRepository.findById(contact.getSnapshotData().getId())).thenReturn(ofNullable(existingContact.getSnapshotData()));
        when(cimContactRepository.save(any(ContactSnapshotData.class))).thenReturn(null);

        //save contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .save() was invoked 1 time
        verify(cimContactRepository, times(1)).save(any(ContactSnapshotData.class));

        //capture contact sent to .save()
        verify(cimContactRepository).save(savedContact.capture());
        ContactSnapshotData capturedContact = savedContact.getValue();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), capturedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), capturedContact.getStatus(),
                        "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), capturedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), capturedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getPreferredContactMethod(), capturedContact.getPreferredContactMethod(),
                        "Expected preferredContactMethod to be '"+ expectedContact.getSnapshotData().getPreferredContactMethod() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getNotificationEmail(), capturedContact.getNotificationEmail(),
                        "Expected notificationEmail to be '"+ expectedContact.getSnapshotData().getNotificationEmail()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), capturedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), capturedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), capturedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getIsTest(),
                        "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value." ),

                () -> {
                    assertTrue(capturedContact.getContactMethods().size() == 2);

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod capturedEmailContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(capturedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), capturedEmailContactMethod.getId(),
                                    "Expected id to be '"+ expectedEmailContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), capturedEmailContactMethod.getType(),
                                    "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), capturedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), capturedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, capturedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), capturedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod capturedMobileContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(capturedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), capturedMobileContactMethod.getId(),
                                    "Expected id to be '"+ expectedMobileContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), capturedMobileContactMethod.getType(),
                                    "Expected type to be '"+ expectedMobileContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), capturedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedMobileContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), capturedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedMobileContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), capturedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedMobileContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedMobileContactMethod.getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);

                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );

                },
                () -> {
                    assertTrue(capturedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), capturedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+ expectedContactRole.getId() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), capturedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+expectedContactRole.getCustomerAccountId() +"', but was some other value." ),

                            () -> assertEquals("ACS User", capturedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be 'ACS User', but was some other value." ),

                            () -> assertEquals(Timestamp.valueOf("2019-11-07 10:14:49"), capturedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '2019-11-07 10:14:49', but was some other value." ),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value." ),

                            () -> {
                                assertTrue(capturedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );
                }
        );

    }

    @Test
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewContactMethodsNull_NewContactRolesNull_contactRoleDoesNotExistAndDoesNotTriggersCiamApiCall () throws IOException {

        //setup existing account
        Contact existingContact =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
        existingContact.getSnapshotData().setChangeTimestamp(new Timestamp(contact.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        existingContact.getSnapshotData().setFirstName("Samewise");
        existingContact.getSnapshotData().setLastName("Gamgee");

        //setup incoming contact
        contact.setOperationType("Update");
        contact.getSnapshotData().setContactMethods(null);
        contact.getSnapshotData().setContactRoles(null);

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(contact);
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(true);
        when(cimContactRepository.findById(contact.getSnapshotData().getId())).thenReturn(ofNullable(existingContact.getSnapshotData()));
        when(cimContactRepository.save(any(ContactSnapshotData.class))).thenReturn(null);

        //save contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .save() was invoked 1 time
        verify(cimContactRepository, times(1)).save(any(ContactSnapshotData.class));

        //capture contact sent to .save()
        verify(cimContactRepository).save(savedContact.capture());
        ContactSnapshotData capturedContact = savedContact.getValue();

        //perform contact tests.
        assertAll("Updated Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), capturedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), capturedContact.getStatus(),
                        "Expected status to be '"+expectedContact.getSnapshotData().getStatus() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), capturedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), capturedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getPreferredContactMethod(), capturedContact.getPreferredContactMethod(),
                        "Expected preferredContactMethod to be '"+ expectedContact.getSnapshotData().getPreferredContactMethod() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getNotificationEmail(), capturedContact.getNotificationEmail(),
                        "Expected notificationEmail to be '"+ expectedContact.getSnapshotData().getNotificationEmail() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), capturedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), capturedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value.") ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), capturedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getIsTest(),
                        "Expected isTest to be '"+expectedContact.getSnapshotData().getIsTest() +"', but was some other value." ),

                () -> assertTrue(capturedContact.getContactMethods().size() == 0),
                () -> assertTrue(capturedContact.getContactRoles().size() == 0)
        );
    }

    @Test
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleIsMyGciUserAndTriggersCiamApiCall () throws IOException {

        //setup existing account
        Contact existingContact =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
        existingContact.getSnapshotData().setChangeTimestamp(new Timestamp(contact.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        existingContact.getSnapshotData().setFirstName("Samewise");
        existingContact.getSnapshotData().setLastName("Gamgee");
        existingContact.getSnapshotData().getContactMethods().clear();
        existingContact.getSnapshotData().getContactRoles().clear();


        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(contact);
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(true);
        when(cimContactRepository.findById(contact.getSnapshotData().getId())).thenReturn(ofNullable(existingContact.getSnapshotData()));
        when(cimContactRepository.save(any(ContactSnapshotData.class))).thenReturn(null);

        //save contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .save() was invoked 1 time
        verify(cimContactRepository, times(1)).save(any(ContactSnapshotData.class));

        //capture contact sent to .save()
        verify(cimContactRepository).save(savedContact.capture());
        ContactSnapshotData capturedContact = savedContact.getValue();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), capturedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value."  ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), capturedContact.getStatus(),
                        "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), capturedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), capturedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getPreferredContactMethod(), capturedContact.getPreferredContactMethod(),
                        "Expected preferredContactMethod to be '"+ expectedContact.getSnapshotData().getPreferredContactMethod() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getNotificationEmail(), capturedContact.getNotificationEmail(),
                        "Expected notificationEmail to be '"+ expectedContact.getSnapshotData().getNotificationEmail() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), capturedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), capturedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId()+"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), capturedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getIsTest(),
                        "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest()+"', but was some other value." ),

                () -> {
                    assertTrue(capturedContact.getContactMethods().size() == 2);

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod capturedEmailContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(capturedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), capturedEmailContactMethod.getId(),
                                    "Expected id to be '"+ expectedEmailContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), capturedEmailContactMethod.getType(),
                                    "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), capturedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), capturedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, capturedEmailContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPreferred(),
                                    "Expected preferred to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), capturedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod capturedMobileContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(capturedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), capturedMobileContactMethod.getId(),
                                    "Expected id to be '"+ expectedMobileContactMethod.getId()+"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), capturedMobileContactMethod.getType(),
                                    "Expected type to be '"+ expectedMobileContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), capturedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedMobileContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), capturedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedMobileContactMethod.getEmailAddress()+"', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), capturedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedMobileContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedMobileContactMethod.getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );

                },
                () -> {
                    assertTrue(capturedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), capturedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+ expectedContactRole.getId() +"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), capturedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountId to be '"+ expectedContactRole.getCustomerAccountId() +"', but was some other value." ),

                            () -> assertEquals(expectedContactRole.getRole(), capturedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be '"+ expectedContactRole.getRole() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCreatedWhen(), capturedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedContactRole.getCreatedWhen() +"', but was some other value." ),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId()+"', but was some other value." );
                            }
                    );
                }
        );

    }

    @Test
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewContactMethodsNotNull_NewContactRolesNotNull_contactRoleIsNotMyGciUserAndDoesNotTriggersCiamApiCall () throws IOException {

        //setup existing account
        Contact existingContact =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
        existingContact.getSnapshotData().setChangeTimestamp(new Timestamp(contact.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        existingContact.getSnapshotData().setFirstName("Samewise");
        existingContact.getSnapshotData().setLastName("Gamgee");
        existingContact.getSnapshotData().getContactMethods().clear();
        existingContact.getSnapshotData().getContactRoles().clear();

        contact.getSnapshotData().setContactRoles(new ArrayList<ContactRole>(){{
            add(ContactRole.builder()
                    .id("9155603678013761475")
                    .customerAccountId("9155603678013761469")
                    .role("ACS User")
                    .createdWhen(Timestamp.valueOf("2019-11-07 10:14:49"))
                    .build());
        }});

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(contact);
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(true);
        when(cimContactRepository.findById(contact.getSnapshotData().getId())).thenReturn(ofNullable(existingContact.getSnapshotData()));
        when(cimContactRepository.save(any(ContactSnapshotData.class))).thenReturn(null);

        //save contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .save() was invoked 1 time
        verify(cimContactRepository, times(1)).save(any(ContactSnapshotData.class));

        //capture contact sent to .save()
        verify(cimContactRepository).save(savedContact.capture());
        ContactSnapshotData capturedContact = savedContact.getValue();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), capturedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), capturedContact.getStatus(),
                        "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), capturedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), capturedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getPreferredContactMethod(), capturedContact.getPreferredContactMethod(),
                        "Expected preferredContactMethod to be '"+ expectedContact.getSnapshotData().getPreferredContactMethod() +"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getNotificationEmail(), capturedContact.getNotificationEmail(),
                        "Expected notificationEmail to be '"+ expectedContact.getSnapshotData().getNotificationEmail() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), capturedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), capturedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), capturedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle()+"', but was some other value."),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getIsTest(),
                        "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value." ),

                () -> {
                    assertTrue(capturedContact.getContactMethods().size() == 2);

                    //Email contact method
                    final ContactMethod expectedEmailContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    final ContactMethod capturedEmailContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Email")).findFirst().orElse(null);
                    assertTrue(capturedEmailContactMethod != null);

                    assertAll("Email Contact Method (Preferred)",
                            () -> assertEquals(expectedEmailContactMethod.getId(), capturedEmailContactMethod.getId(),
                                    "Expected id to be '"+ expectedEmailContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getType(), capturedEmailContactMethod.getType(),
                                    "Expected type to be '"+ expectedEmailContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getPhoneNumber(), capturedEmailContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedEmailContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getEmailAddress(), capturedEmailContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedEmailContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be '1', but was some other value."),

                            () -> assertEquals(null, capturedEmailContactMethod.getPrimaryPhone(),
                                    "Expected ### to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedEmailContactMethod.getPreferred(),
                                    "Expected ### to be '1', but was some other value."),

                            () -> assertEquals(expectedEmailContactMethod.getCreatedWhen(), capturedEmailContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedEmailContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactMethods().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() + "', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );

                    //Mobile Contact Method
                    final ContactMethod expectedMobileContactMethod = expectedContact.getSnapshotData().getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    final ContactMethod capturedMobileContactMethod = capturedContact.getContactMethods()
                            .stream().filter(contactMethod -> contactMethod.getType().equals("Mobile")).findFirst().orElse(null);
                    assertTrue(capturedMobileContactMethod != null);

                    assertAll("Contact Mobile Method Tests",
                            () -> assertEquals(expectedMobileContactMethod.getId(), capturedMobileContactMethod.getId(),
                                    "Expected id to be '"+ expectedMobileContactMethod.getId() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getType(), capturedMobileContactMethod.getType(),
                                    "Expected type to be '"+ expectedMobileContactMethod.getType() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getPhoneNumber(), capturedMobileContactMethod.getPhoneNumber(),
                                    "Expected phoneNumber to be '"+ expectedMobileContactMethod.getPhoneNumber() +"', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getEmailAddress(), capturedMobileContactMethod.getEmailAddress(),
                                    "Expected emailAddress to be '"+ expectedMobileContactMethod.getEmailAddress() +"', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPrimaryEmail(),
                                    "Expected primaryEmail to be 'null', but was some other value."),

                            () -> assertEquals((byte) 1, capturedMobileContactMethod.getPrimaryPhone(),
                                    "Expected primaryPhone to be '1', but was some other value."),

                            () -> assertEquals(null, capturedMobileContactMethod.getPreferred(),
                                    "Expected preferred to be 'null', but was some other value."),

                            () -> assertEquals(expectedMobileContactMethod.getCreatedWhen(), capturedMobileContactMethod.getCreatedWhen(),
                                    "Expected createdWhen to be '"+ expectedMobileContactMethod.getCreatedWhen() +"', but was some other value."),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedMobileContactMethod.getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                            () -> {
                                assertTrue(capturedContact.getContactMethods().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactMethods().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );

                },
                () -> {
                    assertTrue(capturedContact.getContactRoles().size() == 1);

                    //Contact Roles tests (only tested if above assertion returns true)
                    final ContactRole expectedContactRole = expectedContact.getSnapshotData().getContactRoles().get(0);

                    assertAll("Contact Role Tests",
                            () -> assertEquals(expectedContactRole.getId(), capturedContact.getContactRoles().get(0).getId(),
                                    "Expected id to be '"+ expectedContactRole.getId() +"', but was some other value."),

                            () -> assertEquals(expectedContactRole.getCustomerAccountId(), capturedContact.getContactRoles().get(0).getCustomerAccountId(),
                                    "Expected customerAccountID to be '"+ expectedContactRole.getCustomerAccountId() +"', but was some other value."),

                            () -> assertEquals("ACS User", capturedContact.getContactRoles().get(0).getRole(),
                                    "Expected role to be 'ACS User', but was some other value."),

                            () -> assertEquals(Timestamp.valueOf("2019-11-07 10:14:49"), capturedContact.getContactRoles().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '2019-11-07 10:14:49', but was some other value." ),

                            () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getContactRoles().get(0).getIsTest(),
                                    "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value." ),

                            () -> {
                                assertTrue(capturedContact.getContactRoles().get(0).getContactSnapshotData() != null);
                                assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getContactRoles().get(0).getContactSnapshotData().getId(),
                                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value.");
                            }
                    );
                }
        );

    }

    @Test
    public void persistContactSnapshot_updatesContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewContactMethodsNull_NewContactRolesNull_contactRoleDoesNotExistAndDoesNotTriggersCiamApiCall () throws IOException {

        //setup existing account
        Contact existingContact =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/contact.json"), Contact.class);
        existingContact.getSnapshotData().setChangeTimestamp(new Timestamp(contact.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        existingContact.getSnapshotData().setFirstName("Samewise");
        existingContact.getSnapshotData().setLastName("Gamgee");

        //setup incoming contact
        contact.getSnapshotData().setContactMethods(null);
        contact.getSnapshotData().setContactRoles(null);

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(contact);
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(true);
        when(cimContactRepository.findById(contact.getSnapshotData().getId())).thenReturn(ofNullable(existingContact.getSnapshotData()));
        when(cimContactRepository.save(any(ContactSnapshotData.class))).thenReturn(null);

        //save contact
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .save() was invoked 1 time
        verify(cimContactRepository, times(1)).save(any(ContactSnapshotData.class));

        //capture contact sent to .save()
        verify(cimContactRepository).save(savedContact.capture());
        ContactSnapshotData capturedContact = savedContact.getValue();

        //perform contact tests.
        assertAll("Saved Contact",
                () -> assertEquals(expectedContact.getSnapshotData().getId(), capturedContact.getId(),
                        "Expected id to be '"+ expectedContact.getSnapshotData().getId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getContactId(), capturedContact.getContactId(),
                        "Expected contactId to be '"+ expectedContact.getSnapshotData().getContactId() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getStatus(), capturedContact.getStatus(),
                        "Expected status to be '"+ expectedContact.getSnapshotData().getStatus() +"', but was some other value."  ),

                () -> assertEquals(expectedContact.getSnapshotData().getFirstName(), capturedContact.getFirstName(),
                        "Expected firstName to be '"+ expectedContact.getSnapshotData().getFirstName() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getLastName(), capturedContact.getLastName(),
                        "Expected lastName to be '"+ expectedContact.getSnapshotData().getLastName()+"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getPreferredContactMethod(), capturedContact.getPreferredContactMethod(),
                        "Expected preferredContactMethod to be '"+ expectedContact.getSnapshotData().getPreferredContactMethod() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getNotificationEmail(), capturedContact.getNotificationEmail(),
                        "Expected notificationEmail to be '"+ expectedContact.getSnapshotData().getNotificationEmail() +"', but was some other value." ),

                () -> assertEquals(expectedContact.getSnapshotData().getCreatedWhen(), capturedContact.getCreatedWhen(),
                        "Expected createdWhen to be '"+ expectedContact.getSnapshotData().getCreatedWhen()+"', but was some other value."  ),

                () -> assertEquals(expectedContact.getSnapshotData().getCustomerAccountId(), capturedContact.getCustomerAccountId(),
                        "Expected customerAccountId to be '"+ expectedContact.getSnapshotData().getCustomerAccountId() +"', but was some other value." ) ,

                () -> assertEquals(expectedContact.getSnapshotData().getTitle(), capturedContact.getTitle(),
                        "Expected title to be '"+ expectedContact.getSnapshotData().getTitle() +"', but was some other value."   ),

                () -> assertEquals(expectedContact.getSnapshotData().getIsTest(), capturedContact.getIsTest(),
                        "Expected isTest to be '"+ expectedContact.getSnapshotData().getIsTest() +"', but was some other value."),

                () -> assertTrue(capturedContact.getContactMethods().size() == 0),
                () -> assertTrue(capturedContact.getContactRoles().size() == 0)
        );

    }

    //##DELETE OPERATION
    @Test
    public void persistContactSnapshot_deletesContactSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        contact.setOperationType("Delete");

        //Mocks
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(true);

        //Delete customer account
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .delete() was invoked 1 time
        verify(cimContactRepository, times(1)).delete(any(ContactSnapshotData.class));

    }

    @Test
    public void persistContactSnapshot_deletesContactSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        contact.setOperationType("Delete");

        //Mocks
        when(cimContactRepository.existsById(contact.getSnapshotData().getId())).thenReturn(false);

        //Delete customer account
        contactPersistenceHelper.persistContactSnapshot(contact);

        //verify .delete() was invoked 1 time
        verify(cimContactRepository, times(0)).delete(any(ContactSnapshotData.class));

    }

}