package com.gci.cim.snapshot.unit.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfile;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfileSnapshotData;
import com.gci.cim.snapshot.helper.NotificationProfilePersistenceHelper;
import com.gci.cim.snapshot.repository.CIMNotificationProfileRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import static java.util.Optional.ofNullable;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class NotificationProfilePersistenceHelperUnitTests {

    private static NotificationProfile expectedNotificationProfile;
    private NotificationProfile notificationProfile;

    @Mock
    ObjectMapper objectMapper;

    @Mock //dependency to mock
    CIMNotificationProfileRepository cimNotificationProfileRepository;

    @InjectMocks //class you are testing
    NotificationProfilePersistenceHelper notificationProfilePersistenceHelper;

    @Captor //used to capture the value sent to a method into a pojo.
    private ArgumentCaptor<NotificationProfileSnapshotData> savedNotificationProfile;

    @BeforeAll
    public static void setupExpectedValues() throws Exception{
        expectedNotificationProfile =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_profile.json"), NotificationProfile.class);
    }

    @BeforeEach
    public void setUp() throws Exception {
        notificationProfile =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_profile.json"), NotificationProfile.class);
    }

    //##VALIDATION
    @Test //null checking
    public void persistNotificationProfileSnapshot_doesNothing_whenNotificationProfileInputIsNull() throws JsonProcessingException {

        //save notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(null);

        //verify .save() was never invoked
        verify(cimNotificationProfileRepository, times(0)).save(any(NotificationProfileSnapshotData.class));

        //verify .delete() was never invoked
        verify(cimNotificationProfileRepository, times(0)).delete(any(NotificationProfileSnapshotData.class));

    }

    //##CREATE OPERATION
    @Test //**Happy Path
    public void persistContactSnapshot_createsContactSnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws IOException {

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(notificationProfile);
        when(cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId())).thenReturn(false);
        when(cimNotificationProfileRepository.save(any(NotificationProfileSnapshotData.class))).thenReturn(null);

        //save customer account
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //verify .save() was invoked 1 time
        verify(cimNotificationProfileRepository, times(1)).save(any(NotificationProfileSnapshotData.class));

        //capture notification profile sent to .save()
        verify(cimNotificationProfileRepository).save(savedNotificationProfile.capture());
        NotificationProfileSnapshotData capturedNotificationProfile = savedNotificationProfile.getValue();

        //perform notification profile tests.
        assertAll("Saved Notification Profile",
            () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), capturedNotificationProfile.getId(),
            "Expected id to be " + expectedNotificationProfile.getSnapshotData().getId() + " but was some other value."),

            () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), capturedNotificationProfile.getContact(),
                    "Expected contact to be " + expectedNotificationProfile.getSnapshotData().getContact() + " but was some other value."),

            () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), capturedNotificationProfile.getNotificationType(),
                    "Expected notificationType to be " + expectedNotificationProfile.getSnapshotData().getNotificationType() + " but was some other value."),

            () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), capturedNotificationProfile.getEmailChannel(),
                    "Expected emailChannel to be " + expectedNotificationProfile.getSnapshotData().getEmailChannel() + " but was some other value."),

            () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), capturedNotificationProfile.getMobileChannel(),
                    "Expected mobileChannel to be " + expectedNotificationProfile.getSnapshotData().getMobileChannel() + " but was some other value."),

            () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), capturedNotificationProfile.getCreatedWhen(),
                    "Expected createdWhen to be " + expectedNotificationProfile.getSnapshotData().getCreatedWhen() + " but was some other value."),


            () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), capturedNotificationProfile.getChangeTimestamp(),
                    "change time stamp"),

            () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), capturedNotificationProfile.getIsTest(),
                    "Expected isTest to be " + expectedNotificationProfile.getSnapshotData().getIsTest() + " but was some other value.")

        );

    }

    @Test
    public void persistContactSnapshot_createsContactSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws IOException {

        //setup incoming contact
        notificationProfile.setOperationType("Update");

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(notificationProfile);
        when(cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId())).thenReturn(false);
        when(cimNotificationProfileRepository.save(any(NotificationProfileSnapshotData.class))).thenReturn(null);

        //save notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //verify .save() was invoked 1 time
        verify(cimNotificationProfileRepository, times(1)).save(any(NotificationProfileSnapshotData.class));

        //capture notification profile sent to .save()
        verify(cimNotificationProfileRepository).save(savedNotificationProfile.capture());
        NotificationProfileSnapshotData capturedNotificationProfile = savedNotificationProfile.getValue();

        //perform notification profile tests.
        assertAll("Saved Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), capturedNotificationProfile.getId(),
                        "Expected id to be " + expectedNotificationProfile.getSnapshotData().getId() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), capturedNotificationProfile.getContact(),
                        "Expected contact to be " + expectedNotificationProfile.getSnapshotData().getContact() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), capturedNotificationProfile.getNotificationType(),
                        "Expected notificationType to be " + expectedNotificationProfile.getSnapshotData().getNotificationType() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), capturedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to be " + expectedNotificationProfile.getSnapshotData().getEmailChannel() + " but was some other value." ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), capturedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to be " + expectedNotificationProfile.getSnapshotData().getMobileChannel() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), capturedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to be " + expectedNotificationProfile.getSnapshotData().getCreatedWhen() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), capturedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), capturedNotificationProfile.getIsTest(),
                        "Expected isTest to be " + expectedNotificationProfile.getSnapshotData().getIsTest() + " but was some other value.")

        );

    }


    //##UPDATE OPERATION
    @Test //**Happy Path
    public void persistNotificationProfileSnapshot_updatesNotificationProfileSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist () throws IOException {

        final int ONE_YEAR = (1000 * 60 * 60 * 24 * 365);

        //setup existing account
        NotificationProfile existingNotificationProfile =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_profile.json"), NotificationProfile.class);
        existingNotificationProfile.getSnapshotData().setContact("78901234567890");
        existingNotificationProfile.getSnapshotData().setNotificationType("Telegraph");
        existingNotificationProfile.getSnapshotData().setEmailChannel("NBC");
        existingNotificationProfile.getSnapshotData().setMobileChannel("Hallmark");
        existingNotificationProfile.getSnapshotData().setCreatedWhen(new Timestamp(notificationProfile.getSnapshotData().getCreatedWhen().getTime() - ONE_YEAR));
        existingNotificationProfile.getSnapshotData().setChangeTimestamp(new Timestamp(notificationProfile.getChangeTimestamp().getTime() - ONE_YEAR));

        //Mocks
        when(cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId())).thenReturn(true);
        when(cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId())).thenReturn(java.util.Optional.ofNullable(ofNullable(existingNotificationProfile.getSnapshotData()).orElse(null)));
        when(cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId())).thenReturn(ofNullable(existingNotificationProfile.getSnapshotData()));
        when(cimNotificationProfileRepository.save(any(NotificationProfileSnapshotData.class))).thenReturn(null);

        //setup incoming notification profile
        notificationProfile.setOperationType("Update");

        //save notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //verify .save() was invoked 1 time
        verify(cimNotificationProfileRepository, times(1)).save(any(NotificationProfileSnapshotData.class));

        //capture notification profile sent to .save()
        verify(cimNotificationProfileRepository).save(savedNotificationProfile.capture());
        NotificationProfileSnapshotData capturedNotificationProfile = savedNotificationProfile.getValue();

        //perform notification profile tests.
        assertAll("Saved Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), capturedNotificationProfile.getId(),
                        "Expected id to be " + expectedNotificationProfile.getSnapshotData().getId() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), capturedNotificationProfile.getContact(),
                        "Expected contact to be " + expectedNotificationProfile.getSnapshotData().getContact() + " but was some other value." ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), capturedNotificationProfile.getNotificationType(),
                        "Expected notificationType to be " + expectedNotificationProfile.getSnapshotData().getNotificationType() + " but was some other value." ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), capturedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to be " + expectedNotificationProfile.getSnapshotData().getEmailChannel() + " but was some other value." ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), capturedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to be " + expectedNotificationProfile.getSnapshotData().getMobileChannel() + " but was some other value." ),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), capturedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to be " + expectedNotificationProfile.getSnapshotData().getCreatedWhen() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), capturedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), capturedNotificationProfile.getIsTest(),
                        "Expected isTest to be " + expectedNotificationProfile.getSnapshotData().getIsTest() + " but was some other value.")

        );

    }


    @Test
    public void persistNotificationProfileSnapshot_updatesNotificationProfileSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist () throws IOException {

        final int ONE_YEAR = (1000 * 60 * 60 * 24 * 365);

        //setup existing account
        NotificationProfile existingNotificationProfile =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_profile.json"), NotificationProfile.class);
        existingNotificationProfile.getSnapshotData().setContact("78901234567890");
        existingNotificationProfile.getSnapshotData().setNotificationType("Telegraph");
        existingNotificationProfile.getSnapshotData().setEmailChannel("NBC");
        existingNotificationProfile.getSnapshotData().setMobileChannel("Hallmark");
        existingNotificationProfile.getSnapshotData().setCreatedWhen(new Timestamp(notificationProfile.getSnapshotData().getCreatedWhen().getTime() - ONE_YEAR));
        existingNotificationProfile.getSnapshotData().setChangeTimestamp(new Timestamp(notificationProfile.getChangeTimestamp().getTime() - ONE_YEAR));

        //Mocks
        when(cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId())).thenReturn(true);
        when(cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId())).thenReturn(java.util.Optional.ofNullable(ofNullable(existingNotificationProfile.getSnapshotData()).orElse(null)));
        when(cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId())).thenReturn(ofNullable(existingNotificationProfile.getSnapshotData()));
        when(cimNotificationProfileRepository.save(any(NotificationProfileSnapshotData.class))).thenReturn(null);

        //save notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //verify .save() was invoked 1 time
        verify(cimNotificationProfileRepository, times(1)).save(any(NotificationProfileSnapshotData.class));

        //capture notification profile sent to .save()
        verify(cimNotificationProfileRepository).save(savedNotificationProfile.capture());
        NotificationProfileSnapshotData capturedNotificationProfile = savedNotificationProfile.getValue();

        //perform notification profile tests.
        assertAll("Saved Notification Profile",
                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getId(), capturedNotificationProfile.getId(),
                        "Expected id to be " + expectedNotificationProfile.getSnapshotData().getId() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getContact(), capturedNotificationProfile.getContact(),
                        "Expected contact to be " + expectedNotificationProfile.getSnapshotData().getContact() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getNotificationType(), capturedNotificationProfile.getNotificationType(),
                        "Expected notificationType to be " + expectedNotificationProfile.getSnapshotData().getNotificationType() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getEmailChannel(), capturedNotificationProfile.getEmailChannel(),
                        "Expected emailChannel to be " + expectedNotificationProfile.getSnapshotData().getEmailChannel() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getMobileChannel(), capturedNotificationProfile.getMobileChannel(),
                        "Expected mobileChannel to be " + expectedNotificationProfile.getSnapshotData().getMobileChannel() + " but was some other value."),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getCreatedWhen(), capturedNotificationProfile.getCreatedWhen(),
                        "Expected createdWhen to be " + expectedNotificationProfile.getSnapshotData().getCreatedWhen() + " but was some other value." ),

                () -> assertEquals(expectedNotificationProfile.getChangeTimestamp(), capturedNotificationProfile.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationProfile.getSnapshotData().getIsTest(), capturedNotificationProfile.getIsTest(),
                        "Expected isTest to be " + expectedNotificationProfile.getSnapshotData().getIsTest() + " but was some other value." )

        );

    }

    //##DELETE OPERATION
    @Test
    public void persistNotificationProfileSnapshot_deletesNotificationProfileSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        notificationProfile.setOperationType("Delete");

        //Mocks
        when(cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId())).thenReturn(true);

        //Delete notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //verify .delete() was invoked 1 time
        verify(cimNotificationProfileRepository, times(1)).delete(any(NotificationProfileSnapshotData.class));

    }

    @Test
    public void persistNotificationProfileSnapshot_deletesNotificationProfileSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        notificationProfile.setOperationType("Delete");

        //Mocks
        when(cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId())).thenReturn(false);

        //Delete notification profile
        notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);

        //verify .delete() was invoked 1 time
        verify(cimNotificationProfileRepository, times(0)).delete(any(NotificationProfileSnapshotData.class));

    }

}
