package com.gci.cim.snapshot.unit.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.customerAccount.BillingAccount;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccount;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccountSnapshotData;
import com.gci.cim.snapshot.helper.CustomerAccountPersistenceHelper;
import com.gci.cim.snapshot.repository.CimCustomerAccountRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import static java.util.Optional.ofNullable;

@ExtendWith(MockitoExtension.class)
public class CustomerAccountPersistenceHelperUnitTests {

    private static CustomerAccount expectedCustomerAccount;
    private CustomerAccount customerAccount;

    @Mock
    ObjectMapper objectMapper;

    @Mock //dependency to mock
    CimCustomerAccountRepository cimCustomerAccountRepository;

    @InjectMocks //class you are testing
    CustomerAccountPersistenceHelper customerAccountPersistenceHelper;

    @Captor //used to capture the value sent to a method into a pojo.
    private ArgumentCaptor<CustomerAccountSnapshotData> savedCustomerAccount;

    @BeforeAll
    public static void setupExpectedValues() throws Exception{
        expectedCustomerAccount =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/customer_account.json"), CustomerAccount.class);
    }

    @BeforeEach
    public void setUp() throws Exception {
        customerAccount =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/customer_account.json"), CustomerAccount.class);
    }

    //##VALIDATION
    @Test //null checking
    public void persistCustomerAccountSnapshot_doesNothing_whenCustomerAccountInputIsNull() throws JsonProcessingException {

        //save customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(null);

        //verify .save() was never invoked
        verify(cimCustomerAccountRepository, times(0)).save(any(CustomerAccountSnapshotData.class));

        //verify .delete() was never invoked
        verify(cimCustomerAccountRepository, times(0)).delete(any(CustomerAccountSnapshotData.class));
    }

    //##CREATE OPERATION
    @Test //**Happy Path
    public void persistCustomerAccountSnapshot_createsCustomerAccountSnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws IOException {

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(customerAccount);
        when(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())).thenReturn(false);
        when(cimCustomerAccountRepository.save(any(CustomerAccountSnapshotData.class))).thenReturn(null);

        //save customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //verify .save() was invoked 1 time
        verify(cimCustomerAccountRepository, times(1)).save(any(CustomerAccountSnapshotData.class));

        //capture customer account sent to .save()
        verify(cimCustomerAccountRepository).save(savedCustomerAccount.capture());
        CustomerAccountSnapshotData capturedCustomerAccount = savedCustomerAccount.getValue();

        //perform customer account tests.
        assertAll("Saved Customer Account",
            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getId(), capturedCustomerAccount.getId(),
           "Expected id to be '" + expectedCustomerAccount.getSnapshotData().getId() +"' but was some other value. " ),

            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getName(), capturedCustomerAccount.getName(),
                    "Expected name to be '" + expectedCustomerAccount.getSnapshotData().getName() +"' but was some other value. " ),

            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber(), capturedCustomerAccount.getCustomerAccountNumber(),
                    "Expected customerAccountNumber to be '" + expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber() +"' but was some other value. " ),

            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getStatus(), capturedCustomerAccount.getStatus(),
                    "Expected status to be '" + expectedCustomerAccount.getSnapshotData().getStatus() +"' but was some other value. " ),

            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getParentCustomerCategory(), capturedCustomerAccount.getParentCustomerCategory(),
                    "Expected parentCustomerCategory to be '" + expectedCustomerAccount.getSnapshotData().getParentCustomerCategory() +"' but was some other value. " ),

            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getPrimaryContact(), capturedCustomerAccount.getPrimaryContact(),
                    "Expected primaryContact to be '" + expectedCustomerAccount.getSnapshotData().getPrimaryContact() +"' but was some other value. "),

            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerCategory(), capturedCustomerAccount.getCustomerCategory(),
                    "Expected customerCategory to be '" + expectedCustomerAccount.getSnapshotData().getCustomerCategory() +"' but was some other value. "  ),

            () -> assertEquals(expectedCustomerAccount.getChangeTimestamp(), capturedCustomerAccount.getChangeTimestamp(),
                    "Expected changeTimestamp to be '" + expectedCustomerAccount.getChangeTimestamp() +"' but was some other value. " ),

            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCreatedWhen(), capturedCustomerAccount.getCreatedWhen(),
                    "Expected createdWhen to be '" + expectedCustomerAccount.getSnapshotData().getCreatedWhen()+"' but was some other value. " ),

            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getIsTest(), capturedCustomerAccount.getIsTest(),
                    "Expected isTest to be '" + expectedCustomerAccount.getSnapshotData().getIsTest() +"' but was some other value. " ),

            () -> assertEquals(null, capturedCustomerAccount.getSnapshot(),
                    "Expected ### to be 'null' but was some other value. "),

            () -> {
                assertTrue(capturedCustomerAccount.getBillingAccounts().size() == 1);

                //Billing account tests (only tested if above assertion returns true)
                final BillingAccount expectedBillingAccount = expectedCustomerAccount.getSnapshotData().getBillingAccounts().get(0);

                assertAll("Billing Account Test",
                    () -> assertEquals(expectedBillingAccount.getId(), capturedCustomerAccount.getBillingAccounts().get(0).getId(),
                            "Expected id to be '" + expectedBillingAccount.getId() +"' but was some other value. "),

                    () -> assertEquals(expectedBillingAccount.getBillingAccountNumber(), capturedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                            "Expected billingAccountNumber to be '" + expectedBillingAccount.getBillingAccountNumber() +"' but was some other value. "),

                    () -> assertEquals(expectedCustomerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), capturedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                            "Expected contact to be '" + expectedCustomerAccount.getSnapshotData().getBillingContacts().get(0).getContact() +"' but was some other value. " ),

                    () -> assertEquals(expectedBillingAccount.getStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                            "Expected status to be '" + expectedBillingAccount.getStatus() +"' but was some other value. " ),

                    () -> assertEquals(expectedBillingAccount.getAutoPaymentStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                            "Expected autoPaymentStatus to be '" + expectedBillingAccount.getAutoPaymentStatus() +"' but was some other value. " ),

                    () -> assertEquals(expectedBillingAccount.getBillHandlingCode(), capturedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                            "Expected billhandlingCode to be '" + expectedBillingAccount.getBillHandlingCode() +"' but was some other value. "),

                    () -> assertEquals(expectedBillingAccount.getAutoPaymentStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                            "Expected autoPaymentStatus to be '" + expectedBillingAccount.getAutoPaymentStatus() +"' but was some other value. "),

                    () -> assertEquals(expectedBillingAccount.getCreatedWhen(), capturedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                            "Expected createdWhen to be '" + expectedBillingAccount.getCreatedWhen() +"' but was some other value. "),

                    () -> assertEquals(expectedCustomerAccount.getSnapshotData().getIsTest(), capturedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                            "Expected isTest to be '" + expectedCustomerAccount.getSnapshotData().getIsTest() +"' but was some other value. "),

                    () -> {
                        assertTrue(capturedCustomerAccount.getBillingAccounts().get(0).getCustomerAccountSnapshotData() != null);
                        assertEquals(expectedCustomerAccount.getSnapshotData().getId(),capturedCustomerAccount.getBillingAccounts().get(0).getCustomerAccountSnapshotData().getId(),
                                "Expected id to be '" + expectedCustomerAccount.getSnapshotData().getId() +"' but was some other value. "  );
                    }
                );
            }
        );

    }

    @Test
    public void persistCustomerAccountSnapshot_createsCustomerAccountSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws JsonProcessingException {

        //setup incoming customer account
        customerAccount.setOperationType("Update");

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(customerAccount);
        when(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())).thenReturn(false);
        when(cimCustomerAccountRepository.save(any(CustomerAccountSnapshotData.class))).thenReturn(null);

        //save customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //verify .save() was invoked 1 time
        verify(cimCustomerAccountRepository, times(1)).save(any(CustomerAccountSnapshotData.class));

        //capture customer account sent to .save()
        verify(cimCustomerAccountRepository).save(savedCustomerAccount.capture());
        CustomerAccountSnapshotData capturedCustomerAccount = savedCustomerAccount.getValue();

        //perform customer account tests.
        assertAll("Saved Customer Account",
                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getId(), capturedCustomerAccount.getId(),
                        "Expected id to be '" + expectedCustomerAccount.getSnapshotData().getId() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getName(), capturedCustomerAccount.getName(),
                        "Expected name to be '" + expectedCustomerAccount.getSnapshotData().getName()+"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber(), capturedCustomerAccount.getCustomerAccountNumber(),
                        "Expected customerAccountNumber to be '" + expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getStatus(), capturedCustomerAccount.getStatus(),
                        "Expected status to be '" + expectedCustomerAccount.getSnapshotData().getStatus() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getParentCustomerCategory(), capturedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be '" + expectedCustomerAccount.getSnapshotData().getParentCustomerCategory() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getPrimaryContact(), capturedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be '" + expectedCustomerAccount.getSnapshotData().getPrimaryContact() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerCategory(), capturedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be '" + expectedCustomerAccount.getSnapshotData().getCustomerCategory() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getChangeTimestamp(), capturedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be '" + expectedCustomerAccount.getChangeTimestamp() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCreatedWhen(), capturedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be '" + expectedCustomerAccount.getSnapshotData().getCreatedWhen() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getIsTest(), capturedCustomerAccount.getIsTest(),
                        "Expected isTest to be '" + expectedCustomerAccount.getSnapshotData().getIsTest() +"' but was some other value. "),

                () -> assertEquals(null, capturedCustomerAccount.getSnapshot(),
                        "Expected ### to be 'null' but was some other value. "),

                () -> {
                    assertTrue(capturedCustomerAccount.getBillingAccounts().size() == 1);

                    //Billing account tests (only tested if above assertion returns true)
                    final BillingAccount expectedBillingAccount = expectedCustomerAccount.getSnapshotData().getBillingAccounts().get(0);

                    assertAll("Billing Account Test",
                            () -> assertEquals(expectedBillingAccount.getId(), capturedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected id to be '" + expectedBillingAccount.getId() +"' but was some other value. "),

                            () -> assertEquals(expectedBillingAccount.getBillingAccountNumber(), capturedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expected billingAccountNumber to be '" + expectedBillingAccount.getBillingAccountNumber() +"' but was some other value. " ),

                            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), capturedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expected contact to be '" + expectedCustomerAccount.getSnapshotData().getBillingContacts().get(0).getContact() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expected status to be '" + expectedBillingAccount.getStatus() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getAutoPaymentStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expected autoPaymentStatus to be '" + expectedBillingAccount.getAutoPaymentStatus() +"' but was some other value. "),

                            () -> assertEquals(expectedBillingAccount.getBillHandlingCode(), capturedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expected billHandlingCode to be '" + expectedBillingAccount.getBillHandlingCode() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getAutoPaymentStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expected autoPaymentStatus to be '" + expectedBillingAccount.getAutoPaymentStatus() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getCreatedWhen(), capturedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '" + expectedBillingAccount.getCreatedWhen() +"' but was some other value. " ),

                            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getIsTest(), capturedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expected isTest to be '" + expectedCustomerAccount.getSnapshotData().getIsTest() +"' but was some other value. " ),

                            () -> {
                                assertTrue(capturedCustomerAccount.getBillingAccounts().get(0).getCustomerAccountSnapshotData() != null);
                                assertEquals(expectedCustomerAccount.getSnapshotData().getId(),capturedCustomerAccount.getBillingAccounts().get(0).getCustomerAccountSnapshotData().getId(),
                                        "Expected id to be '" + expectedCustomerAccount.getSnapshotData().getId() +"' but was some other value. "      );

                            }
                    );
                }
        );

    }

    //##UPDATE OPERATION
    @Test //**Happy Path
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewBillingAccountsNotNull () throws IOException {

        //setup existing account
        CustomerAccount existingcustomerAccount =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/customer_account.json"), CustomerAccount.class);
        existingcustomerAccount.getSnapshotData().setChangeTimestamp(new Timestamp(customerAccount.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        existingcustomerAccount.getSnapshotData().setName("Samewise Gamgee");
        existingcustomerAccount.getSnapshotData().setCustomerCategory("Business");
        existingcustomerAccount.getSnapshotData().getBillingAccounts().clear();

        //setup incoming customer account
        customerAccount.setOperationType("Update");

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(customerAccount);
        when(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())).thenReturn(true);
        when(cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId())).thenReturn(ofNullable(existingcustomerAccount.getSnapshotData()));
        when(cimCustomerAccountRepository.save(any(CustomerAccountSnapshotData.class))).thenReturn(null);

        //update customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //verify .save() was invoked 1 time
        verify(cimCustomerAccountRepository, times(1)).save(any(CustomerAccountSnapshotData.class));

        //capture customer account sent to .save()
        verify(cimCustomerAccountRepository).save(savedCustomerAccount.capture());
        CustomerAccountSnapshotData capturedCustomerAccount = savedCustomerAccount.getValue();

        //perform customer account tests.
        assertAll("Saved Customer Account",
                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getId(), capturedCustomerAccount.getId(),
                        "Expected id to be '" + expectedCustomerAccount.getSnapshotData().getId() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getName(), capturedCustomerAccount.getName(),
                        "Expected name to be '" + expectedCustomerAccount.getSnapshotData().getName() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber(), capturedCustomerAccount.getCustomerAccountNumber(),
                        "Expected customerAccountNumber to be '" + expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getStatus(), capturedCustomerAccount.getStatus(),
                        "Expected status to be '" + expectedCustomerAccount.getSnapshotData().getStatus() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getParentCustomerCategory(), capturedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be '" + expectedCustomerAccount.getSnapshotData().getParentCustomerCategory() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getPrimaryContact(), capturedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be '" + expectedCustomerAccount.getSnapshotData().getPrimaryContact() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerCategory(), capturedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be '" + expectedCustomerAccount.getSnapshotData().getCustomerCategory() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getChangeTimestamp(), capturedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be '" + expectedCustomerAccount.getChangeTimestamp() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCreatedWhen(), capturedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be '" + expectedCustomerAccount.getSnapshotData().getCreatedWhen() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getIsTest(), capturedCustomerAccount.getIsTest(),
                        "Expected isTest to be '" + expectedCustomerAccount.getSnapshotData().getIsTest() +"' but was some other value. " ),

                () -> assertEquals(null, capturedCustomerAccount.getSnapshot(),
                        "Expected snapshot to be 'null' but was some other value. " ),

                () -> {
                    assertTrue(capturedCustomerAccount.getBillingAccounts().size() == 1);

                    //Billing account tests (only tested if above assertion returns true)
                    final BillingAccount expectedBillingAccount = expectedCustomerAccount.getSnapshotData().getBillingAccounts().get(0);

                    assertAll("Billing Account Test",
                            () -> assertEquals(expectedBillingAccount.getId(), capturedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected id to be '" + expectedBillingAccount.getId() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getBillingAccountNumber(), capturedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expected billingAccountNumber to be '" + expectedBillingAccount.getBillingAccountNumber() +"' but was some other value. " ),

                            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), capturedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expected contact to be '" + expectedCustomerAccount.getSnapshotData().getBillingContacts().get(0).getContact() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expected status to be '" + expectedBillingAccount.getStatus() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getAutoPaymentStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expected autoPaymentStatus to be '" + expectedBillingAccount.getAutoPaymentStatus() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getBillHandlingCode(), capturedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expected billHandlingCode to be '" + expectedBillingAccount.getBillHandlingCode() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getAutoPaymentStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expected autoPaymentStatus to be '" + expectedBillingAccount.getAutoPaymentStatus() +"' but was some other value. "),

                            () -> assertEquals(expectedBillingAccount.getCreatedWhen(), capturedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '" + expectedBillingAccount.getCreatedWhen() +"' but was some other value. " ),

                            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getIsTest(), capturedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expected isTest to be '" + 1 +"' but was some other value. " ),

                            () -> {
                                assertTrue(capturedCustomerAccount.getBillingAccounts().get(0).getCustomerAccountSnapshotData() != null);
                                assertEquals(expectedCustomerAccount.getSnapshotData().getId(),capturedCustomerAccount.getBillingAccounts().get(0).getCustomerAccountSnapshotData().getId(),
                                        "Expected id to be '" + expectedCustomerAccount.getSnapshotData().getId() +"' but was some other value. " );
                            }
                    );
                }
        );

    }

    @Test
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist_NewBillingAccountsNull () throws IOException {

        //setup existing account
        CustomerAccount existingcustomerAccount =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/customer_account.json"), CustomerAccount.class);
        existingcustomerAccount.getSnapshotData().setChangeTimestamp(new Timestamp(customerAccount.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        existingcustomerAccount.getSnapshotData().setName("Samewise Gamgee");
        existingcustomerAccount.getSnapshotData().setCustomerCategory("Business");

        //setup incoming customer account
        customerAccount.setOperationType("Update");
        customerAccount.getSnapshotData().setBillingAccounts(null);

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(customerAccount);
        when(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())).thenReturn(true);
        when(cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId())).thenReturn(ofNullable(existingcustomerAccount.getSnapshotData()));
        when(cimCustomerAccountRepository.save(any(CustomerAccountSnapshotData.class))).thenReturn(null);

        //update customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //verify .save() was invoked 1 time
        verify(cimCustomerAccountRepository, times(1)).save(any(CustomerAccountSnapshotData.class));

        //test customer account values sent to .save() method
        verify(cimCustomerAccountRepository).save(savedCustomerAccount.capture());

        //capture customer account sent to .save()
        verify(cimCustomerAccountRepository).save(savedCustomerAccount.capture());
        CustomerAccountSnapshotData capturedCustomerAccount = savedCustomerAccount.getValue();

        //perform customer account tests.
        assertAll("Saved Customer Account",
                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getId(), capturedCustomerAccount.getId(),
                        "Expected id to be '" + expectedCustomerAccount.getSnapshotData().getId() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getName(), capturedCustomerAccount.getName(),
                        "Expected name to be '" + expectedCustomerAccount.getSnapshotData().getName() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber(), capturedCustomerAccount.getCustomerAccountNumber(),
                        "Expected customerAccountNumber to be '" + expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getStatus(), capturedCustomerAccount.getStatus(),
                        "Expected status to be '" + expectedCustomerAccount.getSnapshotData().getStatus() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getParentCustomerCategory(), capturedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be '" + expectedCustomerAccount.getSnapshotData().getParentCustomerCategory() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getPrimaryContact(), capturedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be '" + expectedCustomerAccount.getSnapshotData().getPrimaryContact() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerCategory(), capturedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be '" + expectedCustomerAccount.getSnapshotData().getCustomerCategory() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getChangeTimestamp(), capturedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be '" + expectedCustomerAccount.getChangeTimestamp() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCreatedWhen(), capturedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be '" + expectedCustomerAccount.getSnapshotData().getCreatedWhen() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getIsTest(), capturedCustomerAccount.getIsTest(),
                        "Expected isTest to be '" + expectedCustomerAccount.getSnapshotData().getIsTest() +"' but was some other value. " ),

                () -> assertEquals(null, capturedCustomerAccount.getSnapshot(),
                        "Expected snapshot to be 'null' but was some other value. " ),

                () -> assertTrue(capturedCustomerAccount.getBillingAccounts().size() == 0)
        );

    }

    @Test
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewBillingAccountsNotNull () throws IOException {

        //setup existing account
        CustomerAccount existingcustomerAccount =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/customer_account.json"), CustomerAccount.class);
        existingcustomerAccount.getSnapshotData().setChangeTimestamp(new Timestamp(customerAccount.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        existingcustomerAccount.getSnapshotData().setName("Samewise Gamgee");
        existingcustomerAccount.getSnapshotData().setCustomerCategory("Business");
        existingcustomerAccount.getSnapshotData().getBillingAccounts().clear();


        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(customerAccount);
        when(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())).thenReturn(true);
        when(cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId())).thenReturn(ofNullable(existingcustomerAccount.getSnapshotData()));
        when(cimCustomerAccountRepository.save(any(CustomerAccountSnapshotData.class))).thenReturn(null);

        //update customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //verify .save() was invoked 1 time
        verify(cimCustomerAccountRepository, times(1)).save(any(CustomerAccountSnapshotData.class));

        //test customer account values sent to .save() method
        verify(cimCustomerAccountRepository).save(savedCustomerAccount.capture());
        CustomerAccountSnapshotData capturedCustomerAccount = savedCustomerAccount.getValue();

        //perform customer account tests.
        assertAll("Saved Customer Account",
                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getId(), capturedCustomerAccount.getId(),
                        "Expected id to be '" + expectedCustomerAccount.getSnapshotData().getId() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getName(), capturedCustomerAccount.getName(),
                        "Expected name to be '" + expectedCustomerAccount.getSnapshotData().getName() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber(), capturedCustomerAccount.getCustomerAccountNumber(),
                        "Expected customerAccountNumber to be '" + expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getStatus(), capturedCustomerAccount.getStatus(),
                        "Expected status to be '" + expectedCustomerAccount.getSnapshotData().getStatus() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getParentCustomerCategory(), capturedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be '" + expectedCustomerAccount.getSnapshotData().getParentCustomerCategory() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getPrimaryContact(), capturedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be '" + expectedCustomerAccount.getSnapshotData().getPrimaryContact()+"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerCategory(), capturedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be '" + expectedCustomerAccount.getSnapshotData().getCustomerCategory() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getChangeTimestamp(), capturedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be '" + expectedCustomerAccount.getChangeTimestamp() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCreatedWhen(), capturedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be '" + expectedCustomerAccount.getSnapshotData().getCreatedWhen() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getIsTest(), capturedCustomerAccount.getIsTest(),
                        "Expected isTest to be '" + expectedCustomerAccount.getSnapshotData().getIsTest() +"' but was some other value. " ),

                () -> assertEquals(null, capturedCustomerAccount.getSnapshot(),
                        "Expected snapshot to be 'null' but was some other value. " ),

                () -> {
                    assertTrue(capturedCustomerAccount.getBillingAccounts().size() == 1);

                    //Billing account tests (only tested if above assertion returns true)
                    final BillingAccount expectedBillingAccount = expectedCustomerAccount.getSnapshotData().getBillingAccounts().get(0);

                    assertAll("Billing Account Test",
                            () -> assertEquals(expectedBillingAccount.getId(), capturedCustomerAccount.getBillingAccounts().get(0).getId(),
                                    "Expected id to be '" + expectedBillingAccount.getId() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getBillingAccountNumber(), capturedCustomerAccount.getBillingAccounts().get(0).getBillingAccountNumber(),
                                    "Expected billingAccountNumber to be '" + expectedBillingAccount.getBillingAccountNumber() +"' but was some other value. "),

                            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getBillingContacts().get(0).getContact(), capturedCustomerAccount.getBillingAccounts().get(0).getBillingContactId(),
                                    "Expected contact to be '" + expectedCustomerAccount.getSnapshotData().getBillingContacts().get(0).getContact() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getStatus(),
                                    "Expected status to be '" + expectedBillingAccount.getStatus() +"' but was some other value. " ),

                            () -> assertEquals(expectedBillingAccount.getAutoPaymentStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expected autoPaymentStatus to be '" + expectedBillingAccount.getAutoPaymentStatus() +"' but was some other value. "),

                            () -> assertEquals(expectedBillingAccount.getBillHandlingCode(), capturedCustomerAccount.getBillingAccounts().get(0).getBillHandlingCode(),
                                    "Expected billHandlingCode to be '" + expectedBillingAccount.getBillHandlingCode()+"' but was some other value. "),

                            () -> assertEquals(expectedBillingAccount.getAutoPaymentStatus(), capturedCustomerAccount.getBillingAccounts().get(0).getAutoPaymentStatus(),
                                    "Expected autoPaymentStatus to be '" + expectedBillingAccount.getAutoPaymentStatus() +"' but was some other value. "),

                            () -> assertEquals(expectedBillingAccount.getCreatedWhen(), capturedCustomerAccount.getBillingAccounts().get(0).getCreatedWhen(),
                                    "Expected createdWhen to be '" + expectedBillingAccount.getCreatedWhen() +"' but was some other value. "),

                            () -> assertEquals(expectedCustomerAccount.getSnapshotData().getIsTest(), capturedCustomerAccount.getBillingAccounts().get(0).getIsTest(),
                                    "Expected isTest to be '" + expectedCustomerAccount.getSnapshotData().getIsTest() +"' but was some other value. " ),

                            () -> {
                                assertTrue(capturedCustomerAccount.getBillingAccounts().get(0).getCustomerAccountSnapshotData() != null);
                                assertEquals(expectedCustomerAccount.getSnapshotData().getId(),capturedCustomerAccount.getBillingAccounts().get(0).getCustomerAccountSnapshotData().getId(),
                                        "Expected id to be '" + expectedCustomerAccount.getSnapshotData().getId() +"' but was some other value. " );
                            }
                    );
                }
        );

    }

    @Test
    public void persistCustomerAccountSnapshot_updatesCustomerAccountSnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist_NewBillingAccountsNull () throws IOException {

        //setup existing account
        CustomerAccount existingcustomerAccount =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/customer_account.json"), CustomerAccount.class);
        existingcustomerAccount.getSnapshotData().setChangeTimestamp(new Timestamp(customerAccount.getChangeTimestamp().getTime() - (1000 * 60 * 60 * 24 * 365)));  //one year, I hope
        existingcustomerAccount.getSnapshotData().setName("Samewise Gamgee");
        existingcustomerAccount.getSnapshotData().setCustomerCategory("Business");

        //setup incoming customer account
        customerAccount.getSnapshotData().setBillingAccounts(null);

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(customerAccount);
        when(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())).thenReturn(true);
        when(cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId())).thenReturn(ofNullable(existingcustomerAccount.getSnapshotData()));
        when(cimCustomerAccountRepository.save(any(CustomerAccountSnapshotData.class))).thenReturn(null);

        //update customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //verify .save() was invoked 1 time
        verify(cimCustomerAccountRepository, times(1)).save(any(CustomerAccountSnapshotData.class));

        //test customer account values sent to .save() method
        verify(cimCustomerAccountRepository).save(savedCustomerAccount.capture());
        CustomerAccountSnapshotData capturedCustomerAccount = savedCustomerAccount.getValue();

        //perform customer account tests.
        assertAll("Saved Customer Account",
                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getId(), capturedCustomerAccount.getId(),
                        "Expected id to be '" + expectedCustomerAccount.getSnapshotData().getId() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getName(), capturedCustomerAccount.getName(),
                        "Expected name to be '" + expectedCustomerAccount.getSnapshotData().getName() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber(), capturedCustomerAccount.getCustomerAccountNumber(),
                        "Expected customerAccountNumber to be '" + expectedCustomerAccount.getSnapshotData().getCustomerAccountNumber() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getStatus(), capturedCustomerAccount.getStatus(),
                        "Expected status to be '" + expectedCustomerAccount.getSnapshotData().getStatus() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getParentCustomerCategory(), capturedCustomerAccount.getParentCustomerCategory(),
                        "Expected parentCustomerCategory to be '" + expectedCustomerAccount.getSnapshotData().getParentCustomerCategory() +"' but was some other value. " ),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getPrimaryContact(), capturedCustomerAccount.getPrimaryContact(),
                        "Expected primaryContact to be '" + expectedCustomerAccount.getSnapshotData().getPrimaryContact() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCustomerCategory(), capturedCustomerAccount.getCustomerCategory(),
                        "Expected customerCategory to be '" + expectedCustomerAccount.getSnapshotData().getCustomerCategory() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getChangeTimestamp(), capturedCustomerAccount.getChangeTimestamp(),
                        "Expected changeTimestamp to be '" + expectedCustomerAccount.getChangeTimestamp() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getCreatedWhen(), capturedCustomerAccount.getCreatedWhen(),
                        "Expected createdWhen to be '" + expectedCustomerAccount.getSnapshotData().getCreatedWhen() +"' but was some other value. "),

                () -> assertEquals(expectedCustomerAccount.getSnapshotData().getIsTest(), capturedCustomerAccount.getIsTest(),
                        "Expected isTest to be '" + expectedCustomerAccount.getSnapshotData().getIsTest() +"' but was some other value. " ),

                () -> assertEquals(null, capturedCustomerAccount.getSnapshot(),
                        "Expected snapshot to be 'null' but was some other value. " ),

                () -> assertTrue(capturedCustomerAccount.getBillingAccounts().size() == 0)
        );

    }

    //##DELETE OPERATION
    @Test
    public void persistCustomerAccountSnapshot_deletesCustomerAccountSnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        customerAccount.setOperationType("Delete");

        //Mocks
        when(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())).thenReturn(true);

        //Delete customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //verify .delete() was invoked 1 time
        verify(cimCustomerAccountRepository, times(1)).delete(any(CustomerAccountSnapshotData.class));

    }

    @Test
    public void persistCustomerAccountSnapshot_deletesCustomerAccountSnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        customerAccount.setOperationType("Delete");

        //Mocks
        when(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())).thenReturn(false);

        //Delete customer account
        customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);

        //verify .delete() was never invoked
        verify(cimCustomerAccountRepository, times(0)).delete(any(CustomerAccountSnapshotData.class));

    }

}