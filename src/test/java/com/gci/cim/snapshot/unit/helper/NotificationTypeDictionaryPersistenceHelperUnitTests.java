package com.gci.cim.snapshot.unit.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionary;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionarySnapshotData;
import com.gci.cim.snapshot.helper.NotificationTypeDictionaryPersistenceHelper;
import com.gci.cim.snapshot.repository.CIMNotificationTypeDictionaryRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import static java.util.Optional.ofNullable;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class NotificationTypeDictionaryPersistenceHelperUnitTests {

    private static NotificationTypeDictionary expectedNotificationTypeDictionary;
    private NotificationTypeDictionary notificationTypeDictionary;

    @Mock
    private ObjectMapper objectMapper;

    @Mock //dependency to mock
    private CIMNotificationTypeDictionaryRepository cimNotificationTypeDictionaryRepository;

    @InjectMocks //class you are testing
    private NotificationTypeDictionaryPersistenceHelper notificationTypeDictionaryPersistenceHelper;

    @Captor //used to capture the value sent to a method into a pojo.
    private ArgumentCaptor<NotificationTypeDictionarySnapshotData> savedNotificationTypeDictionary;

    @BeforeAll
    public static void setupExpectedValues() throws Exception{
        expectedNotificationTypeDictionary =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_type_dictionary.json"), NotificationTypeDictionary.class);
    }

    @BeforeEach
    public void setUp() throws Exception {
        notificationTypeDictionary =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_type_dictionary.json"), NotificationTypeDictionary.class);
    }

    //##VALIDATION
    @Test //null checking
    public void persistNotificationTypeDictionarySnapshot_doesNothing_whenNotificationTypeDictionaryInputIsNull() throws JsonProcessingException {

        //save notification profile
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(null);

        //verify .save() was never invoked
        verify(cimNotificationTypeDictionaryRepository, times(0)).save(any(NotificationTypeDictionarySnapshotData.class));

        //verify .delete() was never invoked
        verify(cimNotificationTypeDictionaryRepository, times(0)).delete(any(NotificationTypeDictionarySnapshotData.class));

    }

    //##CREATE OPERATION
    @Test //**Happy Path
    public void persistNotificationTypeDictionarySnapshot_createsNotificationTypeDictionarySnapshot_whenOperationTypeIsCreateAndSnapshotDoesNotExist () throws IOException {

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(notificationTypeDictionary);
        String data = new ObjectMapper().writeValueAsString(notificationTypeDictionary.getSnapshotData());
        when(cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId())).thenReturn(false);
        when(objectMapper.writeValueAsString(notificationTypeDictionary.getSnapshotData())).thenReturn(data);
        when(cimNotificationTypeDictionaryRepository.save(any(NotificationTypeDictionarySnapshotData.class))).thenReturn(null);

        //save customer account
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //verify .save() was invoked 1 time
        verify(cimNotificationTypeDictionaryRepository, times(1)).save(any(NotificationTypeDictionarySnapshotData.class));

        //capture notification profile sent to .save()
        verify(cimNotificationTypeDictionaryRepository).save(savedNotificationTypeDictionary.capture());
        NotificationTypeDictionarySnapshotData capturedNotificationTypeDictionary = savedNotificationTypeDictionary.getValue();

        assertAll("Saved Notification Type Dictionary",
            () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), capturedNotificationTypeDictionary.getId(),
            "Expected id to be '" + expectedNotificationTypeDictionary.getSnapshotData().getId() + "' but was some other value. "),

            () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(capturedNotificationTypeDictionary.getData()),
                    "Expected snapshotData to be '" + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + "' but was some other value. "),

            () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), capturedNotificationTypeDictionary.getChangeTimestamp(),
                    "change time stamp"),

            () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), capturedNotificationTypeDictionary.getIsTest(),
                    "Expected isTest to be '" + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + "' but was some other value. ")
        );

    }



    @Test
    public void persistNotificationTypeDictionarySnapshot_createsNotificationTypeDictionarySnapshot_whenOperationTypeIsUpdateAndSnapshotDoesNotExist () throws IOException {

        //setup incoming contact
        notificationTypeDictionary.setOperationType("Update");

        //Mocks
        String snapshot = new ObjectMapper().writeValueAsString(notificationTypeDictionary);
        String data = new ObjectMapper().writeValueAsString(notificationTypeDictionary.getSnapshotData());
        when(cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId())).thenReturn(false);
        when(objectMapper.writeValueAsString(notificationTypeDictionary.getSnapshotData())).thenReturn(data);
        when(cimNotificationTypeDictionaryRepository.save(any(NotificationTypeDictionarySnapshotData.class))).thenReturn(null);

        //save customer account
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //verify .save() was invoked 1 time
        verify(cimNotificationTypeDictionaryRepository, times(1)).save(any(NotificationTypeDictionarySnapshotData.class));

        //capture notification profile sent to .save()
        verify(cimNotificationTypeDictionaryRepository).save(savedNotificationTypeDictionary.capture());
        NotificationTypeDictionarySnapshotData capturedNotificationTypeDictionary = savedNotificationTypeDictionary.getValue();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), capturedNotificationTypeDictionary.getId(),
                        "Expected id to be '" + expectedNotificationTypeDictionary.getSnapshotData().getId() + "' but was some other value. "),

                //() -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary)), objectMapper.readTree(capturedNotificationTypeDictionary.getSnapshot())),
                () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(capturedNotificationTypeDictionary.getData()),
                        "Expected snapshotData to be '" + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + "' but was some other value. "),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), capturedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), capturedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + "' but was some other value. ")
        );

    }


    //##UPDATE OPERATION
    @Test //**Happy Path
    public void persistNotificationTypeDictionarySnapshot_updatesNotificationTypeDictionarySnapshot_whenOperationTypeIsUpdateAndSnapshotDoesExist () throws IOException {

        final int ONE_YEAR = (1000 * 60 * 60 * 24 * 365);

        //setup incoming notification profile
        notificationTypeDictionary.setOperationType("Update");

        //setup existing account
        NotificationTypeDictionary existingNotificationTypeDictionary =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_type_dictionary.json"), NotificationTypeDictionary.class);
        existingNotificationTypeDictionary.getSnapshotData().setData(null);
        existingNotificationTypeDictionary.getSnapshotData().setSnapshot(null);
        existingNotificationTypeDictionary.getSnapshotData().setChangeTimestamp(new Timestamp(notificationTypeDictionary.getChangeTimestamp().getTime() - ONE_YEAR));

        //Mocks
        when(cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId())).thenReturn(true);
        when(cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId())).thenReturn(ofNullable(ofNullable(existingNotificationTypeDictionary.getSnapshotData()).orElse(null)));
        when(cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId())).thenReturn(ofNullable(existingNotificationTypeDictionary.getSnapshotData()));
        when(cimNotificationTypeDictionaryRepository.save(any(NotificationTypeDictionarySnapshotData.class))).thenReturn(null);

        //save notification profile
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //verify .save() was invoked 1 time
        verify(cimNotificationTypeDictionaryRepository, times(1)).save(any(NotificationTypeDictionarySnapshotData.class));

        //capture notification profile sent to .save()
        verify(cimNotificationTypeDictionaryRepository).save(savedNotificationTypeDictionary.capture());
        NotificationTypeDictionarySnapshotData capturedNotificationTypeDictionary = savedNotificationTypeDictionary.getValue();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), capturedNotificationTypeDictionary.getId(),
                        "Expected id to be '" + expectedNotificationTypeDictionary.getSnapshotData().getId() + "' but was some other value. "),

                //() -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary)), objectMapper.readTree(capturedNotificationTypeDictionary.getSnapshot())),
                () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(capturedNotificationTypeDictionary.getData()),
                        "Expected snapshotData to be '" + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + "' but was some other value. "),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), capturedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), capturedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + "' but was some other value. ")

        );

    }

    @Test
    public void persistNotificationTypeDictionarySnapshot_updatesNotificationTypeDictionarySnapshot_whenOperationTypeIsCreateAndSnapshotDoesExist () throws IOException {

        final int ONE_YEAR = (1000 * 60 * 60 * 24 * 365);

        //setup existing account
        NotificationTypeDictionary existingNotificationTypeDictionary =
                new ObjectMapper().readValue(new File("src/test/resources/fixtures/notification_type_dictionary.json"), NotificationTypeDictionary.class);
        existingNotificationTypeDictionary.getSnapshotData().setData(null);
        existingNotificationTypeDictionary.getSnapshotData().setSnapshot(null);
        existingNotificationTypeDictionary.getSnapshotData().setChangeTimestamp(new Timestamp(notificationTypeDictionary.getChangeTimestamp().getTime() - ONE_YEAR));

        //Mocks
        when(cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId())).thenReturn(true);
        when(cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId())).thenReturn(ofNullable(ofNullable(existingNotificationTypeDictionary.getSnapshotData()).orElse(null)));
        when(cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId())).thenReturn(ofNullable(existingNotificationTypeDictionary.getSnapshotData()));
        when(cimNotificationTypeDictionaryRepository.save(any(NotificationTypeDictionarySnapshotData.class))).thenReturn(null);

        //save notification profile
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //verify .save() was invoked 1 time
        verify(cimNotificationTypeDictionaryRepository, times(1)).save(any(NotificationTypeDictionarySnapshotData.class));

        //capture notification profile sent to .save()
        verify(cimNotificationTypeDictionaryRepository).save(savedNotificationTypeDictionary.capture());
        NotificationTypeDictionarySnapshotData capturedNotificationTypeDictionary = savedNotificationTypeDictionary.getValue();

        assertAll("Saved Notification Type Dictionary",
                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getId(), capturedNotificationTypeDictionary.getId(),
                        "Expected id to be '" + expectedNotificationTypeDictionary.getSnapshotData().getId() + "' but was some other value. "),

                () -> assertEquals(objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())), objectMapper.readTree(capturedNotificationTypeDictionary.getData()),
                        "Expected snapshotData to be '" + objectMapper.readTree(objectMapper.writeValueAsString(expectedNotificationTypeDictionary.getSnapshotData())) + "' but was some other value. "),

                () -> assertEquals(expectedNotificationTypeDictionary.getChangeTimestamp(), capturedNotificationTypeDictionary.getChangeTimestamp(),
                        "change time stamp"),

                () -> assertEquals(expectedNotificationTypeDictionary.getSnapshotData().getIsTest(), capturedNotificationTypeDictionary.getIsTest(),
                        "Expected isTest to be '" + expectedNotificationTypeDictionary.getSnapshotData().getIsTest() + "' but was some other value. ")

        );

    }

    //##DELETE OPERATION
    @Test
    public void persistNotificationTypeDictionarySnapshot_deletesNotificationTypeDictionarySnapshot_whenOperationTypeIsDeleteAndSnapshotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        notificationTypeDictionary.setOperationType("Delete");

        //Mocks
        when(cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId())).thenReturn(true);

        //Delete notification profile
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //verify .delete() was invoked 1 time
        verify(cimNotificationTypeDictionaryRepository, times(1)).delete(any(NotificationTypeDictionarySnapshotData.class));

    }

    @Test
    public void persistNotificationTypeDictionarySnapshot_deletesNotificationTypeDictionarySnapshot_whenOperationTypeIsDeleteAndSnapshotDoesNotExists () throws JsonProcessingException {

        //change operation type to "Delete"
        notificationTypeDictionary.setOperationType("Delete");

        //Mocks
        when(cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId())).thenReturn(false);

        //Delete notification profile
        notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);

        //verify .delete() was invoked 1 time
        verify(cimNotificationTypeDictionaryRepository, times(0)).delete(any(NotificationTypeDictionarySnapshotData.class));

    }

}
