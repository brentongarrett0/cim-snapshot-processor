package com.gci.cim.snapshot.unit.service;
import com.gci.cim.snapshot.service.CIMSnapshotAdminService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.info.BuildProperties;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CIMSnapshotAdminServiceUnitTests {

    @Mock
    BuildProperties buildProperties;

    @InjectMocks
    CIMSnapshotAdminService cimSnapshotAdminService;

    @Test
    public void getVersion_getsVersion_whenMethodIsCalled ()  {

        //Mocks
        when(buildProperties.getVersion()).thenReturn("1.0.0");

        //Get version
        String actualVersion = cimSnapshotAdminService.getVersion();

        //Perform tests
        assertEquals("v1.0.0", actualVersion, "Expected version to be v1.0.0 but was some other value");

    }

}
