package com.gci.cim.snapshot.unit.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.helper.CIAMAPIRegistrationHelper;
import com.gci.cim.snapshot.properties.CIMSnapshotCiamApiProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CIAMAPIRegistrationHelperUnitTests {

    private final String TEST_URL = "http://www.test.com";

    @Captor
    private ArgumentCaptor<String> savedUrl;

    @Captor
    private ArgumentCaptor<HttpMethod> savedHttpMethod;

    @Captor
    private ArgumentCaptor<HttpEntity<String>> savedRequestEntity;

    @Captor
    private ArgumentCaptor<Class<String>> savedResponseType;

    @Mock
    CIMSnapshotCiamApiProperties cimSnapshotCiamApiProperties;

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private RestTemplate restCall;

    @InjectMocks
    CIAMAPIRegistrationHelper ciamapiRegistrationHelper;

    @Test
    public void sendInviteRequest_doesNothing_whenContactIdIsNull() throws JsonProcessingException {

        //send invitation request
        ciamapiRegistrationHelper.sendInviteRequest(null);

        //verify rest call was never invoked.
        verify(restCall, times(0)).exchange(
                any(),
                any(),
                any(),
                (Class<Object>) any()
        );
    }

    @Test
    public void sendInviteRequest_registersMyGCIUser_whenContactIdIsValid() throws JsonProcessingException {

        final String  CONTACT_ID = "1234567";
        final String REQUEST_BODY = "{\"contactId\":\"1234567\"}";

        //setup response entity
        HttpHeaders headers = new HttpHeaders(){{ add("Content-Type", "application/json"); }};
        Map<String , String> responseBody = new HashMap<String, String>(){{put("hello", "world");}};
        ResponseEntity<String> response = ResponseEntity
            .accepted()
            .headers(headers)
            .body(new ObjectMapper().writeValueAsString(responseBody));

        //Mocks
        when(objectMapper.writeValueAsString(any(Map.class))).thenReturn("{\"contactId\":\"" + CONTACT_ID + "\"}");
        when(cimSnapshotCiamApiProperties.getUrl()).thenReturn(this.TEST_URL);
        when(restCall.exchange(
                eq(this.TEST_URL),
                eq(HttpMethod.POST),
                ArgumentMatchers.<HttpEntity<String>>any(),
                eq(String.class)
        )).thenReturn(response);

        //send invitation request
        ciamapiRegistrationHelper.sendInviteRequest(CONTACT_ID);

        //verify rest call was invoked exactly 1 one time.
        verify(restCall, times(1)).exchange(
                eq(this.TEST_URL),
                eq(HttpMethod.POST),
                ArgumentMatchers.<HttpEntity<String>>any(),
                eq(String.class)
        );

        //capture rest call sent to .exchange()
        verify(restCall).exchange(
                savedUrl.capture(),
                savedHttpMethod.capture(),
                savedRequestEntity.capture(),
                savedResponseType.capture()
        );

        //captured arguments
        String capturedUrl = savedUrl.getValue();
        HttpMethod capturedHTTPMethod = savedHttpMethod.getValue();
        HttpEntity<String> capturedRequestEntity = savedRequestEntity.getValue();
        Class<String> capturedResponseType = savedResponseType.getValue();

        //Perform tests
        assertAll("Response Tests",
                () -> assertEquals(TEST_URL, capturedUrl),
                () -> assertEquals("POST", capturedHTTPMethod.name()),
                () -> assertEquals(REQUEST_BODY, capturedRequestEntity.getBody()),
                () -> assertEquals("java.lang.String", capturedResponseType.getName())
        );
    }

}