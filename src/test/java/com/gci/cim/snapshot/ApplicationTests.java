package com.gci.cim.snapshot;
import com.gci.sdk.logging.EIPLogger;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class ApplicationTests {

    @MockBean
    EIPLogger eipLogger;

    @Test
    void contextLoads() {
    }

}
