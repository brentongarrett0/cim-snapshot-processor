package com.gci.cim.snapshot.controller;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gci.cim.snapshot.service.CIMSnapshotProducerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@Profile("!test")
@RequestMapping(path = "/kafka/producer")
public class CIMSnapshotProducerController {

    @Autowired
    private CIMSnapshotProducerService cimSnapshotProducerService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(path = "/v1/publish")
    public void publishSnapshot(@RequestBody String snapshot) throws JsonProcessingException {
        log.info("**New snapshot published by 'POST /v1/publish' endpoint\nMessage: {}", snapshot);
    	cimSnapshotProducerService.publishMessage(snapshot);
    }

}
