package com.gci.cim.snapshot.controller;
import com.gci.cim.snapshot.service.CIMSnapshotAdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(path = "/snapshot/admin")
public class CIMSnapshotAdminController {

    @Autowired
    CIMSnapshotAdminService cimSnapshotAdminService;

    @GetMapping(path = "/v1/version")
    public String getVersion(){
            return cimSnapshotAdminService.getVersion();
    }

}
