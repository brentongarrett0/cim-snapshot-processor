package com.gci.cim.snapshot.controller;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gci.cim.snapshot.model.ValidationResponse;
import com.gci.cim.snapshot.service.CIMSnapshotService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@Slf4j
@RestController
@RequestMapping(path = "/snapshot")
public class CIMSnapshotController {

    @Autowired
    private CIMSnapshotService cimSnapshotService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(path="/v1/submit")
    public void submitSnapshot(@RequestBody String snapshot) throws JsonProcessingException, ParseException {
        log.info("**New snapshot received by 'POST /v1/submit' endpoint\nMessage: {}", snapshot);
        cimSnapshotService.submitSnapshot(snapshot);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(path = "/v1/validate" , produces = "application/json")
    public ValidationResponse validateSnapshot(@RequestBody String snapshot) throws JsonProcessingException {
        log.info("**New validation request received by 'POST /v1/validate' endpoint\nMessage: {}", snapshot);
        return cimSnapshotService.validateSnapshot(snapshot);
    }

}

