package com.gci.cim.snapshot;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.context.annotation.ComponentScan;

@EnableJpaRepositories(basePackages = "com.gci.cim.snapshot")
@SpringBootApplication
@ComponentScan(basePackages = {"com.gci.sdk", "com.gci.cim.snapshot"})
public class Application { public static void main(String[] args) { SpringApplication.run(Application.class, args); }}
