package com.gci.cim.snapshot.properties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix="com.gci.cim.kafka.producer")
public class CIMSnapshotKafkaProducerProperties { private String isEnabled; }







