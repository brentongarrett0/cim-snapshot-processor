package com.gci.cim.snapshot.model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidationResponse{
    private String snapshotType;
    private Boolean isValid;
    private List<Violations> violations;
}
