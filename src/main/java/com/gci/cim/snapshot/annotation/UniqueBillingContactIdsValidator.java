package com.gci.cim.snapshot.annotation;

import com.gci.cim.snapshot.entity.customerAccount.BillingContact;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccountSnapshotData;
import com.gci.cim.snapshot.repository.CimCustomerAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.*;
import java.util.stream.Collectors;


//<custom annotation, expected field value type>
public class UniqueBillingContactIdsValidator implements ConstraintValidator<UniqueBillingContactIds, List<BillingContact>> {

    @Autowired
    private CimCustomerAccountRepository cimCustomerAccountRepository;

    @Override
    public boolean isValid(List<BillingContact> value, ConstraintValidatorContext context) {

        if(value == null) return true;

        // Collect ids into arraylist out of 'value'
        List<String> billingContactIds = value.stream().map(billingContact -> billingContact.getId()).collect(Collectors.toList());

        //check for duplicates
        Set<String> uniqueBillingContactIds = new HashSet<>(billingContactIds);
        if(uniqueBillingContactIds.size() != billingContactIds.size()) return false ; else return true;

    }

}