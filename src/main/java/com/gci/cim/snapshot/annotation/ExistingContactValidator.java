package com.gci.cim.snapshot.annotation;

import com.gci.cim.snapshot.entity.contact.ContactSnapshotData;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccountSnapshotData;
import com.gci.cim.snapshot.repository.CIMContactRepository;
import com.gci.cim.snapshot.repository.CimCustomerAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.NoSuchElementException;

//<custom annotation, expected field value type>
public class ExistingContactValidator implements ConstraintValidator<ExistingContact, String> {

    @Autowired
    private CIMContactRepository cimContactRepository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        try {
            if(value != null) {
                final ContactSnapshotData existingContact = cimContactRepository.findById(value).get();
            }
            return true;
        }
       catch (NoSuchElementException e){
            return false;
       }
    }

}
