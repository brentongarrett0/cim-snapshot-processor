package com.gci.cim.snapshot.annotation;
import com.gci.cim.snapshot.entity.customerAccount.BillingAccount;
import com.gci.cim.snapshot.repository.CIMBillingAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.NoSuchElementException;

public class ExistingBillingAccountValidator implements ConstraintValidator<ExistingBillingAccount, String> {

    @Autowired
    private CIMBillingAccountRepository cimBillingAccountRepository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        try {
            if(value != null) {
                final BillingAccount billingAccount = cimBillingAccountRepository.findById(value).get();
            }
            return true;
        }
        catch (NoSuchElementException e){
            return false;
        }
    }

}
