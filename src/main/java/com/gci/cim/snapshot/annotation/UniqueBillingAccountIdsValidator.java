package com.gci.cim.snapshot.annotation;

import com.gci.cim.snapshot.entity.customerAccount.BillingAccount;
import com.gci.cim.snapshot.entity.customerAccount.BillingContact;
import com.gci.cim.snapshot.repository.CimCustomerAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


//<custom annotation, expected field value type>
public class UniqueBillingAccountIdsValidator implements ConstraintValidator<UniqueBillingAccountIds, List<BillingAccount>> {

    @Override
    public boolean isValid(List<BillingAccount> value, ConstraintValidatorContext context) {

        // Collect ids into arraylist out of 'value'
        List<String> billingAccountIds = value.stream().map(billingAccount -> billingAccount.getId()).collect(Collectors.toList());

        //check for duplicates
        Set<String> uniqueBillingAccountIds = new HashSet<>(billingAccountIds);
        if(uniqueBillingAccountIds.size() != billingAccountIds.size()) return false ; else return true;

    }

}