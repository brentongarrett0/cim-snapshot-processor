package com.gci.cim.snapshot.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = ExistingCustomerAccountValidator.class) //<-- indicates which class is being used to implement validaiton
@Retention(RetentionPolicy.RUNTIME) //<-- this annotation will be available after runtime
@Target({ ElementType.FIELD }) //<-- indicates what the annotation can be applied to (field in our case)
public @interface ExistingCustomerAccount {

    public String message() default "The value for 'customerAccountId' does not exist in the database. Contact role must reference an existing customer account.";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default{};

}

//this class is the custome annotation called @UniqueEmail