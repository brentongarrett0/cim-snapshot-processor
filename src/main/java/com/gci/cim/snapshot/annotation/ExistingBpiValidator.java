package com.gci.cim.snapshot.annotation;

import com.gci.cim.snapshot.entity.bpi.BpiSnapshotData;
import com.gci.cim.snapshot.repository.CIMBpiRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.NoSuchElementException;

public class ExistingBpiValidator implements ConstraintValidator<ExistingBpi, String> {

    @Autowired
    private CIMBpiRepository cimBpiRepository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        try {
            if(value != null) {
                final BpiSnapshotData existingBpi = cimBpiRepository.findById(value).get();
            }
            return true;
        }
        catch (NoSuchElementException e){
            return false;
        }
    }

}

