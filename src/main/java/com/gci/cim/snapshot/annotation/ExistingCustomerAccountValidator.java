package com.gci.cim.snapshot.annotation;

import com.gci.cim.snapshot.entity.customerAccount.CustomerAccountSnapshotData;
import com.gci.cim.snapshot.repository.CimCustomerAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.NoSuchElementException;

public class ExistingCustomerAccountValidator implements ConstraintValidator<ExistingCustomerAccount, String> {

    @Autowired
    private CimCustomerAccountRepository cimCustomerAccountRepository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        try {
            if(value != null) {
                final CustomerAccountSnapshotData existingCustomerAccount = cimCustomerAccountRepository.findById(value).get();
            }
            return true;
        }
       catch (NoSuchElementException e){
            return false;
       }
    }

}
