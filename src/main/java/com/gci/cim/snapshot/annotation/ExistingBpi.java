package com.gci.cim.snapshot.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = ExistingBpiValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface ExistingBpi {

    public String message() default "The value for 'parentId' does not return an existing BPI from the database.";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default{};

}

