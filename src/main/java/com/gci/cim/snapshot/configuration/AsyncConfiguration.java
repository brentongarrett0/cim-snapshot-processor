package com.gci.cim.snapshot.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@EnableAsync
@ConditionalOnProperty(value="com.gci.cim.rest.async", havingValue = "true", matchIfMissing = true)
public class AsyncConfiguration { }

