package com.gci.cim.snapshot.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.bpi.Bpi;
import com.gci.cim.snapshot.entity.bpi.BpiSnapshotData;
import com.gci.cim.snapshot.repository.CIMBillingAccountRepository;
import com.gci.cim.snapshot.repository.CIMBpiRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@Transactional
public class BpiPersistenceHelper {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CIMBpiRepository cimBpiRepository;

    @Autowired
    CIMBillingAccountRepository cimBillingAccountRepository;

    private String childString;
    private BpiSnapshotData childBpiCopy;

    public void persistBpiSnapshot(Bpi bpi) throws JsonProcessingException, ParseException {

        //null check
        if (bpi == null) {
            log.debug("**Null 'bpi' input sent to persistBpiSnapshot() method.");
            return;
        }

        bpi.getSnapshotData().setChangeTimestamp(bpi.getChangeTimestamp());

        //create operation
        if(bpi.getOperationType().equals("Create")) {
            if (!cimBpiRepository.existsById(bpi.getSnapshotData().getId())) {
                bpi.getSnapshotData().setCustomerAccountId(bpi.getSnapshotData().getParentId());
                saveSnapshot(bpi.getSnapshotData().getParentId(), bpi.getSnapshotData());
                log.debug("**Saving new BPI snapshot with ID: {} ", bpi.getSnapshotData().getId());
            }else{
                // Only Update if new snapshot date is later than existing row
                BpiSnapshotData existingBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).orElse(null);
                if ((existingBpi != null) && ((existingBpi.getChangeTimestamp() == null) || (existingBpi.getChangeTimestamp().getTime() < bpi.getChangeTimestamp().getTime()))) {
                    bpi.getSnapshotData().setCustomerAccountId(bpi.getSnapshotData().getParentId());
                    updateSnapshot(bpi.getSnapshotData().getParentId(), bpi.getSnapshotData());
                    log.debug("**Updating existing bpi snapshot for ID: {}", bpi.getSnapshotData().getId());
                } else {
                    log.debug("**Ignoring Snapshot that is older than row for BPI: {}", existingBpi.getId());
                }
            }
        }

        //update operation
        if(bpi.getOperationType().equals("Update")) {
            if(cimBpiRepository.existsById(bpi.getSnapshotData().getId())){
                // Only Update if new snapshot date is later than existing row
                BpiSnapshotData existingBpi = cimBpiRepository.findById(bpi.getSnapshotData().getId()).orElse(null);
                System.out.println("**bpi is 'Update', and exists: " + existingBpi.getId());
                if ((existingBpi != null) && ((existingBpi.getChangeTimestamp() == null) || (existingBpi.getChangeTimestamp().getTime() < bpi.getChangeTimestamp().getTime()))) {
                    bpi.getSnapshotData().setCustomerAccountId(bpi.getSnapshotData().getParentId());
                    updateSnapshot(bpi.getSnapshotData().getParentId(), bpi.getSnapshotData());
                    log.debug("**Updating existing bpi snapshot for ID: {}", bpi.getSnapshotData().getId());
                } else {
                    System.out.println("**Ignoring Snapshot that is older than row for BPI: " + existingBpi.getId());
                    log.debug("**Ignoring Snapshot that is older than row for BPI: {}", existingBpi.getId());
                }
            }else{
                bpi.getSnapshotData().setCustomerAccountId(bpi.getSnapshotData().getParentId());
                saveSnapshot(bpi.getSnapshotData().getParentId(), bpi.getSnapshotData());
                log.debug("**Saving new BPI snapshot with ID: {} ", bpi.getSnapshotData().getId());
            }
        }

        //delete operation
        if(bpi.getOperationType().equals("Delete")) {
            if(cimBpiRepository.existsById(bpi.getSnapshotData().getId())){
                deleteSnapshot(bpi.getSnapshotData());
                log.debug("**Deleting BPI snapshot with ID: {} ", bpi.getSnapshotData().getId());
            }else{
                log.debug("**BPI with id '{}' does not exist ", bpi.getSnapshotData().getId());
            }
        }
    }

    private void saveSnapshot(String parentBpiId, BpiSnapshotData bpi) {

        List<BpiSnapshotData> nextBpiChildren = new ArrayList<>();

        //set parent id for bpi
        if (parentBpiId != null) {
            if(cimBpiRepository.existsById(parentBpiId)) {
                BpiSnapshotData existingParentBpi = cimBpiRepository.findById(parentBpiId).get();
                bpi.setBpiSnapshotData(existingParentBpi);
                log.debug("**Parent ID for BPI set");
            }
        }

        //Load parent bpi into each child bpi
        if(bpi.getChildBpis() != null) {
            bpi.getChildBpis().stream().forEach(childBpi -> {
                childBpi.setBpiSnapshotData(bpi);

                //add child's children to 'nextBpiChildren'
                try {
                    this.childString = objectMapper.writeValueAsString(childBpi);
                    System.out.println("**children of child bpi: " + this.childString);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                try {
                    this.childBpiCopy = objectMapper.readValue(this.childString, BpiSnapshotData.class);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                nextBpiChildren.add(this.childBpiCopy);

                //clear child's children
                childBpi.getChildBpis().clear();
            });
        }

        //save snapshot
        BpiSnapshotData savedBpi = cimBpiRepository.save(bpi);
        log.info("**Persisted new BPI snapshot to database\nMessage: {}", bpi.getId());

        //iterate to process next child(ren)
        nextBpiChildren.stream().forEach(child -> {
            child.setIsTest(bpi.getIsTest());
            child.setCustomerAccountId(bpi.getCustomerAccountId());
            saveSnapshot(savedBpi.getId(), child);
        });
    }

    private void updateSnapshot(String parentBpiId, BpiSnapshotData bpi){
        List<BpiSnapshotData> nextBpiChildren = new ArrayList<>();

        //retrieve existing customer account
        BpiSnapshotData existingBpi = cimBpiRepository.findById(bpi.getId()).get();

        //set parent id for bpi
        if (parentBpiId != null) {
            if(cimBpiRepository.existsById(parentBpiId)) {
                BpiSnapshotData existingParentBpi = cimBpiRepository.findById(parentBpiId).get();
                existingBpi.setBpiSnapshotData(existingParentBpi);
                log.debug("**Parent BPI set.");
            }
        }

        //update mutable fields
        existingBpi.setBillingAccountId(bpi.getBillingAccountId());
        existingBpi.setServiceId(bpi.getServiceId());
        existingBpi.setServiceName(bpi.getServiceName());
        existingBpi.setSandvineId(bpi.getSandvineId());
        existingBpi.setStatus(bpi.getStatus());
        existingBpi.setProductCategory(bpi.getProductCategory());
        existingBpi.setOfferingId(bpi.getOfferingId());
        existingBpi.setOfferingName(bpi.getOfferingName());
        existingBpi.setMarketId(bpi.getMarketId());
        existingBpi.setMarketName(bpi.getMarketName());
        existingBpi.setCustomerAccountId(bpi.getCustomerAccountId());
        existingBpi.setName(bpi.getName());
        existingBpi.setSandvinePlanId(bpi.getSandvinePlanId());
        existingBpi.setIsLifeline(bpi.getIsLifeline());
        existingBpi.setProductInstance(bpi.getProductInstance());
        existingBpi.setSeachangeSubscriptionProduct(bpi.getSeachangeSubscriptionProduct());
        existingBpi.setSeachangeSubscriberId(bpi.getSeachangeSubscriberId());
        existingBpi.setIncludedUsageAmountGb(bpi.getIncludedUsageAmountGb());
        existingBpi.setSnapshot(bpi.getSnapshot());
        existingBpi.setCreatedWhen(bpi.getCreatedWhen());
        existingBpi.setChangeTimestamp(bpi.getChangeTimestamp());
        existingBpi.setIsTest(bpi.getIsTest());
        log.debug("**Mutable fields for BPI set.");

        //load parent id into child bpis
        if(bpi.getChildBpis() == null){
            existingBpi.getChildBpis().clear();
            log.debug("**Cleared child BPIs for updated BPI");
        }
        else {
            bpi.getChildBpis().stream().forEach(childBpi -> {
                childBpi.setBpiSnapshotData(existingBpi);
                childBpi.setIsTest(bpi.getIsTest());

                //add child's children to 'nextBpiChildren'
                try {
                    this.childString = objectMapper.writeValueAsString(childBpi);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                try {
                    this.childBpiCopy = objectMapper.readValue(this.childString, BpiSnapshotData.class);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                nextBpiChildren.add(this.childBpiCopy);

                //clear child's children
                childBpi.getChildBpis().clear();
            });

            //load new child bpi's into existing bpi
            existingBpi.getChildBpis().clear();
            existingBpi.getChildBpis().addAll(bpi.getChildBpis());
        }

        //update snapshot
        BpiSnapshotData updatedBpi = cimBpiRepository.save(existingBpi);
        log.info("**Persisted updated BPI snapshot to database\nBPI ID: {}", existingBpi.getId());

        //iterate to process next child(ren)
        nextBpiChildren.stream().forEach(child -> {
            child.setIsTest(bpi.getIsTest());
            child.setCustomerAccountId(bpi.getCustomerAccountId());
            updateSnapshot(updatedBpi.getId(), child);
        });

    }

    private void deleteSnapshot(BpiSnapshotData bpi){

        cimBpiRepository.deleteById(bpi.getId());
        log.info("**Deleted BPI snapshot from database ID: ", bpi.getId());

    }

}


