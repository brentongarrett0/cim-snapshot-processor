package com.gci.cim.snapshot.helper;
import com.gci.cim.snapshot.entity.BadSnapshot;
import com.gci.cim.snapshot.repository.CIMBadSnapshotRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class BadSnapshotPersistenceHelper {

    @Autowired
    CIMBadSnapshotRepository cimBadSnapshotRepository;

    public void persistBadSnapshot(BadSnapshot badSnapshot){
        log.info("**Persisting bad snapshot to database\nMessage: {}", badSnapshot.getId());
        cimBadSnapshotRepository.save(badSnapshot);
    }
}
