package com.gci.cim.snapshot.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionary;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionarySnapshotData;
import com.gci.cim.snapshot.repository.CIMNotificationTypeDictionaryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;

@Slf4j
@Component
@Transactional
public class NotificationTypeDictionaryPersistenceHelper {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CIMNotificationTypeDictionaryRepository cimNotificationTypeDictionaryRepository;

    public void persistNotificationTypeDictionarySnapshot(NotificationTypeDictionary notificationTypeDictionary) throws JsonProcessingException {

        //null check
        if (notificationTypeDictionary == null) {
            log.debug("**Null 'notificationTypeDictionary' input sent to persistNotificationTypeDictionarySnapshot() method.");
            return;
        }

        //create operation
        if (notificationTypeDictionary.getOperationType().equals("Create")) {
            if (!cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId())) {
                saveSnapshot(notificationTypeDictionary);
                log.debug("Saving incoming notification type dictionary snapshot for ID: {}", notificationTypeDictionary.getSnapshotData().getId());
            }else{
                // Only Update if new snapshot date is later than existing row
                NotificationTypeDictionarySnapshotData existingNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).orElse(null);
                 if ((existingNotificationTypeDictionary != null) && ((existingNotificationTypeDictionary.getChangeTimestamp() == null) || (existingNotificationTypeDictionary.getChangeTimestamp().getTime() < notificationTypeDictionary.getChangeTimestamp().getTime()))) {
                    updateSnapshot(notificationTypeDictionary);
                    log.debug("Updating incoming notification type dictionary snapshot for ID: {}", notificationTypeDictionary.getSnapshotData().getId());
                } else {
                    log.debug("Ignoring Snapshot that is older than row for NotificationTypeDictionary: {} ", existingNotificationTypeDictionary.getId());
                    //System.out.println("Ignoring Snapshot that is older than row for NotificationTypeDictionary: " + existingNotificationTypeDictionary.getId());
                }
            }
        }

        //update operation
        if (notificationTypeDictionary.getOperationType().equals("Update")) {
            if (cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId())) {
                // Only Update if new snapshot date is later than existing row
                NotificationTypeDictionarySnapshotData existingNotificationTypeDictionary = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).orElse(null);
                if ((existingNotificationTypeDictionary != null) && ((existingNotificationTypeDictionary.getChangeTimestamp() == null) || (existingNotificationTypeDictionary.getChangeTimestamp().getTime() < notificationTypeDictionary.getChangeTimestamp().getTime()))) {
                    updateSnapshot(notificationTypeDictionary);
                    log.debug("Updating incoming notification type dictionary snapshot for ID: {}", notificationTypeDictionary.getSnapshotData().getId());
                } else {
                    log.debug("Ignoring Snapshot that is older than row for NotificationTypeDictionary: {} ", existingNotificationTypeDictionary.getId());
                    //System.out.println("Ignoring Snapshot that is older than row for NotificationTypeDictionary: " + existingNotificationTypeDictionary.getId());
                }
            }else{
                saveSnapshot(notificationTypeDictionary);
                log.debug("Saving incoming notification type dictionary snapshot for ID: {}", notificationTypeDictionary.getSnapshotData().getId());
            }
        }

        //delete operation
        if (notificationTypeDictionary.getOperationType().equals("Delete")) {
            if (cimNotificationTypeDictionaryRepository.existsById(notificationTypeDictionary.getSnapshotData().getId())) {
                deleteSnapshot(notificationTypeDictionary);
                log.debug("Deleting incoming notification type dictionary snapshot for ID: {}", notificationTypeDictionary.getSnapshotData().getId());
            }else{
                log.debug("**notification type dictionary with id '{}' does not exist", notificationTypeDictionary.getSnapshotData().getId());
            }
        }

    }

    private void saveSnapshot(NotificationTypeDictionary notificationTypeDictionary) throws JsonProcessingException {

        //set changetimestamp and data
        notificationTypeDictionary.getSnapshotData().setData(objectMapper.writeValueAsString(notificationTypeDictionary.getSnapshotData()));
        notificationTypeDictionary.getSnapshotData().setChangeTimestamp(notificationTypeDictionary.getChangeTimestamp());

        //persist snapshot
        cimNotificationTypeDictionaryRepository.save(notificationTypeDictionary.getSnapshotData());
        log.info("**Persisted new Notification Type Dictionary snapshot to database\nNOTIFICATION TYPE DICTIONARY ID: {}", notificationTypeDictionary.getSnapshotData().getId());
    }

    private void updateSnapshot(NotificationTypeDictionary notificationTypeDictionary) throws JsonProcessingException {

        //retrieve notification type dictionary
        NotificationTypeDictionarySnapshotData existingNotificationTypeDictionary
                = cimNotificationTypeDictionaryRepository.findById(notificationTypeDictionary.getSnapshotData().getId()).get();

        //update mutable fields
        existingNotificationTypeDictionary.setData(objectMapper.writeValueAsString(notificationTypeDictionary.getSnapshotData()));
        existingNotificationTypeDictionary.setSnapshot(notificationTypeDictionary.getSnapshotData().getSnapshot());
        existingNotificationTypeDictionary.setChangeTimestamp(notificationTypeDictionary.getChangeTimestamp());
        existingNotificationTypeDictionary.setIsTest(notificationTypeDictionary.getSnapshotData().getIsTest());
        log.debug("**mutable fields for notification type dictionary snapshot have been set.");

        //update snapshot
        cimNotificationTypeDictionaryRepository.save(existingNotificationTypeDictionary);
        log.info("**Persisted updated Notification Type Dictionary snapshot to database\nNOTIFICATION TYPE DICTIONARY ID: {}", notificationTypeDictionary.getSnapshotData().getId());
    }
    private void deleteSnapshot(NotificationTypeDictionary notificationTypeDictionary){
        cimNotificationTypeDictionaryRepository.delete(notificationTypeDictionary.getSnapshotData());
        log.info("**Deleted Notification Type Dictionary snapshot from database\nNOTIFICATION TYPE DICTIONARY ID: {}", notificationTypeDictionary.getSnapshotData().getId());
    }

}
