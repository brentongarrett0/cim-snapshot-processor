package com.gci.cim.snapshot.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfile;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionary;
import com.gci.cim.snapshot.model.ValidationResponse;
import com.gci.cim.snapshot.model.Violations;
import com.gci.cim.snapshot.entity.bpi.Bpi;
import com.gci.cim.snapshot.entity.contact.Contact;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@Component
public class ValidationHelper {

    @Autowired
    private Validator validator;

    public ValidationResponse validateContact(Map<String,Object> payload) throws JsonProcessingException {

        //set variables
        ValidationResponse contactValidationResult;
        ValidationResponse validContact = ValidationResponse.builder()
                .snapshotType("Contact")
                .isValid(true)
                .violations(new ArrayList<>())
                .build();
        ValidationResponse invalidContact = ValidationResponse.builder()
                .snapshotType("Contact")
                .isValid(false)
                .build();

        if (payload == null ) return invalidContact;

        //validate contact object
        final ObjectMapper mapper = new ObjectMapper();
        final Contact contact = mapper.convertValue(payload, Contact.class);
        Set<ConstraintViolation<Contact>> violations = validator.validate(contact);

        //loop through violations and build response object
        if (violations.isEmpty()){
            contactValidationResult = validContact;
        }else{
            Iterator<ConstraintViolation<Contact>> it = violations.iterator();
            ArrayList<Violations> violationProperties = new ArrayList<>();
            while(it.hasNext()){
                ConstraintViolation<Contact> constraintViolation = it.next();
                String propertyPath = constraintViolation.getPropertyPath().toString();
                String message = constraintViolation.getMessage();
                violationProperties.add(new Violations(propertyPath,message));
            }
            invalidContact.setViolations(violationProperties);
            contactValidationResult = invalidContact;
        }

        return contactValidationResult;
    }

    public ValidationResponse validateCustomerAccount(Map<String,Object> payload){

        //set variables
        ValidationResponse customerAccountValidationResult;
        ValidationResponse validCustomerAccount = ValidationResponse.builder()
                .snapshotType("Customer Account")
                .isValid(true)
                .violations(new ArrayList<>())
                .build();
        ValidationResponse invalidCustomerAccount = ValidationResponse.builder()
                .snapshotType("Customer Account")
                .isValid(false)
                .build();

        //validate customer account object
        final ObjectMapper mapper = new ObjectMapper();
        final CustomerAccount customerAccount = mapper.convertValue(payload, CustomerAccount.class);
        Set<ConstraintViolation<CustomerAccount>> violations = validator.validate(customerAccount);

        //loop through violations and build response object
        if (violations.isEmpty()){
            customerAccountValidationResult = validCustomerAccount;
        }else{
            Iterator<ConstraintViolation<CustomerAccount>>it = violations.iterator();
            ArrayList<Violations> violationProperties = new ArrayList<>();
            while(it.hasNext()){
                ConstraintViolation<CustomerAccount> constraintViolation = it.next();
                String propertyPath = constraintViolation.getPropertyPath().toString();
                String message = constraintViolation.getMessage();
                violationProperties.add(new Violations(propertyPath,message));
            }
            invalidCustomerAccount.setViolations(violationProperties);
            customerAccountValidationResult = invalidCustomerAccount;
        }

        return customerAccountValidationResult;
    }

    public ValidationResponse validateBpi(Map<String,Object> payload){

        //set variables
        ValidationResponse bpiValidationResult;
        ValidationResponse validBpi = ValidationResponse.builder()
                .snapshotType("BPI")
                .isValid(true)
                .violations(new ArrayList<>())
                .build();
        ValidationResponse invalidBpi = ValidationResponse.builder()
                .snapshotType("BPI")
                .isValid(false)
                .build();

        //validate BPI object, capture violations
        final ObjectMapper mapper = new ObjectMapper();
        final Bpi bpi = mapper.convertValue(payload, Bpi.class);
        Set<ConstraintViolation<Bpi>> violations = validator.validate(bpi);

        //loop through violations and build response object
        if (violations.isEmpty()){
            bpiValidationResult = validBpi;
        }else{
            Iterator<ConstraintViolation<Bpi>>it = violations.iterator();
            ArrayList<Violations> violationProperties = new ArrayList<>();
            while(it.hasNext()){
                ConstraintViolation<Bpi> constraintViolation = it.next();
                String propertyPath = constraintViolation.getPropertyPath().toString();
                String message = constraintViolation.getMessage();
                violationProperties.add(new Violations(propertyPath,message));
            }
            invalidBpi.setViolations(violationProperties);
            bpiValidationResult = invalidBpi;
        }

        return bpiValidationResult;
    }

    public ValidationResponse validateNotificationProfile(Map<String,Object> payload){

        //set variables
        ValidationResponse notificationProfileValidationResult;
        ValidationResponse validNotificationProfile = ValidationResponse.builder()
                .snapshotType("Notification Profile")
                .isValid(true)
                .violations(new ArrayList<>())
                .build();
        ValidationResponse invalidNotificationProfile = ValidationResponse.builder()
                .snapshotType("Notification Profile")
                .isValid(false)
                .build();

        //validate BPI object, capture violations
        final ObjectMapper mapper = new ObjectMapper();
        final NotificationProfile notificationProfile = mapper.convertValue(payload, NotificationProfile.class);
        Set<ConstraintViolation<NotificationProfile>> violations = validator.validate(notificationProfile);

        //loop through violations and build response object
        if (violations.isEmpty()){
            notificationProfileValidationResult = validNotificationProfile;
        }else{
            Iterator<ConstraintViolation<NotificationProfile>>it = violations.iterator();
            ArrayList<Violations> violationProperties = new ArrayList<>();
            while(it.hasNext()){
                ConstraintViolation<NotificationProfile> constraintViolation = it.next();
                String propertyPath = constraintViolation.getPropertyPath().toString();
                String message = constraintViolation.getMessage();
                violationProperties.add(new Violations(propertyPath,message));
            }
            invalidNotificationProfile.setViolations(violationProperties);
            notificationProfileValidationResult = invalidNotificationProfile;
        }

        return notificationProfileValidationResult;
    }

    public ValidationResponse validateNotificationTypeDictionary(Map<String,Object> payload){

        //set variables
        ValidationResponse notificationTypeDictionaryValidationResult;
        ValidationResponse validNotificationTypeDictionary = ValidationResponse.builder()
                .snapshotType("Notification Type Dictionary")
                .isValid(true)
                .violations(new ArrayList<>())
                .build();
        ValidationResponse invalidNotificationTypeDictionary = ValidationResponse.builder()
                .snapshotType("Notification Type Dictionary")
                .isValid(false)
                .build();

        //validate notification type dictionary object, capture violations
        final ObjectMapper mapper = new ObjectMapper();
        final NotificationTypeDictionary notificationTypeDictionary = mapper.convertValue(payload, NotificationTypeDictionary.class);
        Set<ConstraintViolation<NotificationTypeDictionary>> violations = validator.validate(notificationTypeDictionary);

        //loop through violations and build response object
        if (violations.isEmpty()){
            notificationTypeDictionaryValidationResult = validNotificationTypeDictionary;
        }else{
            Iterator<ConstraintViolation<NotificationTypeDictionary>>it = violations.iterator();
            ArrayList<Violations> violationProperties = new ArrayList<>();
            while(it.hasNext()){
                ConstraintViolation<NotificationTypeDictionary> constraintViolation = it.next();
                String propertyPath = constraintViolation.getPropertyPath().toString();
                String message = constraintViolation.getMessage();
                violationProperties.add(new Violations(propertyPath,message));
            }
            invalidNotificationTypeDictionary.setViolations(violationProperties);
            notificationTypeDictionaryValidationResult = invalidNotificationTypeDictionary;
        }

        return notificationTypeDictionaryValidationResult;
    }

}

