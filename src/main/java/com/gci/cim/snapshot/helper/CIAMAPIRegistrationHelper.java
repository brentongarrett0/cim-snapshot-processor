package com.gci.cim.snapshot.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.properties.CIMSnapshotCiamApiProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpStatusCodeException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class CIAMAPIRegistrationHelper {

    @Autowired
    CIMSnapshotCiamApiProperties cimSnapshotCiamApiProperties;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RestTemplate restCall;

    @Async
    public void sendInviteRequest(String contactId) throws JsonProcessingException {

        //null handling
        if(contactId == null) return;

        //setup request entity
        Map<String, Object> request = new HashMap<String, Object>(){{ put("contactId", contactId);}};
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>(objectMapper.writeValueAsString(request), headers);

        //sent invitation request
        String response = "";
        try {
            response =
                    restCall.exchange(
                    cimSnapshotCiamApiProperties.getUrl(),
                    HttpMethod.POST,
                    requestEntity,
                    String.class
                ).getStatusCode().toString();
        } catch (HttpStatusCodeException exception) {
            // do nothing, we are handling http status ourselves
        }

        if(response != null) System.out.println("**Response code: " + response);
        log.info("**REST request for contact id '{}' sent to CIAM API endpoint {}", contactId, cimSnapshotCiamApiProperties.getUrl());
    }


}
