package com.gci.cim.snapshot.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gci.cim.snapshot.entity.contact.*;
import com.gci.cim.snapshot.repository.CIMContactRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;

@Slf4j
@Component
@Transactional
public class ContactPersistenceHelper {

    private boolean isMyGCIUser = false;

    @Autowired
    private CIAMAPIRegistrationHelper ciamapiRegistrationHelper;

    @Autowired
    private CIMContactRepository cimContactRepository;


    public void persistContactSnapshot(Contact contact) throws JsonProcessingException {

        //null check
        if (contact == null) {
            log.debug("**Null 'contact' input sent to persistCustomerAccountSnapshot() method.");
            return;
        }

        //create operation
        if(contact.getOperationType().equals("Create")) {
            if (!cimContactRepository.existsById(contact.getSnapshotData().getId())) {
                saveSnapshot(contact);
                log.debug("**Saving new contact snapshot for ID: {}", contact.getSnapshotData().getId());
            }else {
                // Only Update if new snapshot date is later than existing row
                ContactSnapshotData existingContact = cimContactRepository.findById(contact.getSnapshotData().getId()).orElse(null);
                if ((existingContact != null) && ((existingContact.getChangeTimestamp() == null) || (existingContact.getChangeTimestamp().getTime() < contact.getChangeTimestamp().getTime()))) {
                    updateSnapshot(contact);
                    log.debug("**Updating new contact snapshot for ID: {}", contact.getSnapshotData().getId());
                } else {
                    System.out.println("Ignoring Snapshot that is older than row for Contact: " + existingContact.getId());
                    log.debug("Ignoring Snapshot that is older than row for Contact: {}", existingContact.getId());
                }
            }
        }

        //update operation
        if(contact.getOperationType().equals("Update")) {
            if(cimContactRepository.existsById(contact.getSnapshotData().getId())){
                // Only Update if new snapshot date is later than existing row
                ContactSnapshotData existingContact = cimContactRepository.findById(contact.getSnapshotData().getId()).orElse(null);
                if ((existingContact != null) && ((existingContact.getChangeTimestamp() == null) || (existingContact.getChangeTimestamp().getTime() < contact.getChangeTimestamp().getTime()))) {
                    updateSnapshot(contact);
                    log.debug("Ignoring Snapshot that is older than row for Contact: {}", contact.getSnapshotData().getId());
                } else {
                    log.debug("Ignoring Snapshot that is older than row for Contact: {}", contact.getSnapshotData().getId());
                }
            }else{
                saveSnapshot(contact);
                log.debug("Saving Snapshot that is older than row for Contact: {}", contact.getSnapshotData().getId());
            }
        }

        //delete operation
        if(contact.getOperationType().equals("Delete")){
            if(cimContactRepository.existsById(contact.getSnapshotData().getId())){
                deleteSnapshot(contact);
                log.debug("Deleting contact snapshot with ID: {}", contact.getSnapshotData().getId());
            }else{
                log.debug("contact with ID '{}' does not exist", contact.getSnapshotData().getId());
            }
        }
    }

    private void saveSnapshot(Contact contact) throws JsonProcessingException {

        System.out.println("** mygci user: " + isMyGCIUser);

        //set changeTimestamp for contact
        contact.getSnapshotData().setChangeTimestamp(contact.getChangeTimestamp());

        //load contact into contact method(s), as well as cascade isTest
        contact.getSnapshotData().getContactMethods().forEach(contactMethod -> {
            contactMethod.setContactSnapshotData(contact.getSnapshotData());
            contactMethod.setIsTest(contact.getSnapshotData().getIsTest());

            //PREFERRED CONTACT METHOD LOGIC
            if(contact.getSnapshotData().getPreferredContactMethod() != null){
                String preferredContactMethod = contact.getSnapshotData().getPreferredContactMethod();
                if(contactMethod.getId().equals(preferredContactMethod)) contactMethod.setPreferred((byte)1);
            }

            //NOTIFICATION EMAIL LOGIC
            if(contact.getSnapshotData().getNotificationEmail() != null) {
                String notificationEmail = contact.getSnapshotData().getNotificationEmail();
                if(contactMethod.getId().equals(notificationEmail)) contactMethod.setPrimaryEmail((byte) 1);
            }

            //NOTIFICATION PHONE LOGIC
            if(contact.getSnapshotData().getNotificationPhone() != null) {
                String notificationPhone = contact.getSnapshotData().getNotificationPhone();
                if(contactMethod.getId().equals(notificationPhone)) contactMethod.setPrimaryPhone((byte) 1);
            }

        });

        //load contact into contact role(s), as well as cascade isTest
        contact.getSnapshotData().getContactRoles().forEach(contactRole -> {
            contactRole.setContactSnapshotData(contact.getSnapshotData());
            contactRole.setIsTest(contact.getSnapshotData().getIsTest());

            //check if the contact has mygci contact user role.
            if(contactRole.getRole().equalsIgnoreCase("MyGCI User")){
                isMyGCIUser = true;
            }
        });

        //save snapshot
        log.info("**Persisted new Contact snapshot to database\nCONTACT ID: {}", contact.getSnapshotData().getId());
        cimContactRepository.save(contact.getSnapshotData());
        ContactSnapshotData savedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();
        log.info("**contact saved: {}", savedContact.getId());

    }

    private void updateSnapshot(Contact contact) throws JsonProcessingException {

        //retrieve existing contact
        ContactSnapshotData existingContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();

        //update mutable fields
        existingContact.setContactId(contact.getSnapshotData().getContactId());
        existingContact.setStatus(contact.getSnapshotData().getStatus());
        existingContact.setFirstName(contact.getSnapshotData().getFirstName());
        existingContact.setLastName(contact.getSnapshotData().getLastName());
        existingContact.setCreatedWhen(contact.getSnapshotData().getCreatedWhen());
        existingContact.setCustomerAccountId(contact.getSnapshotData().getCustomerAccountId());
        existingContact.setTitle(contact.getSnapshotData().getTitle());
        existingContact.setChangeTimestamp(contact.getChangeTimestamp());
        existingContact.setIsTest(contact.getSnapshotData().getIsTest());
        existingContact.setSnapshot(contact.getSnapshotData().getSnapshot());
        log.debug("**Mutable fields for contact snapshot updated for ID: {}", contact.getSnapshotData().getId());

        //load existing contact into new contact method(s)
        if(contact.getSnapshotData().getContactMethods() == null){
            existingContact.getContactMethods().clear();
            System.out.println("**new contact methods null, cleared");
            log.debug("**new contact methods null, cleared");
        }
        else {
            contact.getSnapshotData().getContactMethods().stream().forEach(
                 contactMethod -> {
                     log.debug("**new contact methods not null, setting parent");
                     contactMethod.setContactSnapshotData(existingContact);
                     contactMethod.setIsTest(contact.getSnapshotData().getIsTest());

                     //PREFERRED CONTACT METHOD LOGIC
                     if(contact.getSnapshotData().getPreferredContactMethod() != null){
                         String preferredContactMethod = contact.getSnapshotData().getPreferredContactMethod();
                         //System.out.println("**preferred contact method is: " + preferredContactMethod);
                         if(contactMethod.getId().equals(preferredContactMethod)) contactMethod.setPreferred((byte)1);
                     }

                     //NOTIFICATION EMAIL LOGIC
                     if(contact.getSnapshotData().getNotificationEmail() != null) {
                         String notificationEmail = contact.getSnapshotData().getNotificationEmail();
                         if(contactMethod.getId().equals(notificationEmail)) contactMethod.setPrimaryEmail((byte) 1);
                     }

                     //NOTIFICATION PHONE LOGIC
                     if(contact.getSnapshotData().getNotificationPhone() != null) {
                         String notificationPhone = contact.getSnapshotData().getNotificationPhone();
                         if(contactMethod.getId().equals(notificationPhone)) contactMethod.setPrimaryPhone((byte) 1);
                     }

                 });

            //load new contact method(s) into existing contact
            existingContact.getContactMethods().clear();
            existingContact.getContactMethods().addAll(contact.getSnapshotData().getContactMethods());
        }

        //load contact into contact role(s)
        if(contact.getSnapshotData().getContactRoles() == null){
            existingContact.getContactRoles().clear();
            System.out.println("**new contact roles null, cleared");
            log.debug("**new contact roles null, cleared");
        }
        else {
            contact.getSnapshotData().getContactRoles().stream().forEach(
                    contactRole -> {
                        log.debug("**new contact roles not null, setting parent");
                        contactRole.setContactSnapshotData(existingContact);
                        contactRole.setIsTest(contact.getSnapshotData().getIsTest());

                        //check if the contact has mygci contact user role.
                        if((contactRole.getRole() != null) && (contactRole.getRole().equalsIgnoreCase("MyGCI User"))){
                            isMyGCIUser = true;
                        }

                    });

            //load new contact role(s) into existing contact
            existingContact.getContactRoles().clear();
            existingContact.getContactRoles().addAll(contact.getSnapshotData().getContactRoles());
        }

        //update snapshot
        cimContactRepository.save(existingContact);
        log.info("**Persisted updated Contact snapshot to database\nCONTACT ID: {}", existingContact.getId());
        ContactSnapshotData updatedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).get();
        log.info("**contact saved: {}", updatedContact.getId());

    }

    private void deleteSnapshot(Contact contact){
        cimContactRepository.delete(contact.getSnapshotData());
        log.info("**Deleted Contact snapshot from database\nCONTACT ID: {}", contact.getSnapshotData().getId());
    }

    
}
