package com.gci.cim.snapshot.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccount;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccountSnapshotData;
import com.gci.cim.snapshot.repository.CimCustomerAccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;


@Slf4j
@Component
@Transactional
public class CustomerAccountPersistenceHelper {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private CimCustomerAccountRepository cimCustomerAccountRepository;

    public void persistCustomerAccountSnapshot(CustomerAccount customerAccount) throws JsonProcessingException {

        //null check
        if (customerAccount == null) {
            log.debug("**Null 'customerAccount' input sent to persistCustomerAccountSnapshot() method.");
            return;
        }

        //create operation
        if(customerAccount.getOperationType().equals("Create")) {
            if (!cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())) {
                saveSnapshot(customerAccount);
                log.debug("**Saving incoming customer account snapshot for ID: {} ", customerAccount.getSnapshotData().getId());
            }else{
                // Only Update if new snapshot date is later than existing row
                CustomerAccountSnapshotData existingCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).orElse(null);

                if ((existingCustomerAccount != null) && ((existingCustomerAccount.getChangeTimestamp() == null) || (existingCustomerAccount.getChangeTimestamp().getTime() < customerAccount.getChangeTimestamp().getTime()))) {
                    updateSnapshot(customerAccount);
                    log.debug("**Updating incoming customer account snapshot for ID: {} ", customerAccount.getSnapshotData().getId());
                } else {
                    log.debug("**Ignoring Snapshot that is older than row for CustomerAccount: {} ", existingCustomerAccount.getId());
                }
            }
        }

        //update operation
        if(customerAccount.getOperationType().equals("Update")) {
            if(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())){

                // Only Update if new snapshot date is later than existing row
                CustomerAccountSnapshotData existingCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).orElse(null);

                if ((existingCustomerAccount != null) && ((existingCustomerAccount.getChangeTimestamp() == null) || (existingCustomerAccount.getChangeTimestamp().getTime() < customerAccount.getChangeTimestamp().getTime()))) {
                    updateSnapshot(customerAccount);
                    log.debug("**Updating incoming customer account snapshot for ID: {} ", customerAccount.getSnapshotData().getId());
                } else {
                    log.debug("**Ignoring Snapshot that is older than row for CustomerAccount: {} ", existingCustomerAccount.getId());
                    System.out.println("Ignoring Snapshot that is older than row for CustomerAccount: " + existingCustomerAccount.getId());
                }
            }else{
                saveSnapshot(customerAccount);
                log.debug("**Saving incoming customer account snapshot for ID: {} ", customerAccount.getSnapshotData().getId());
            }
        }

        //delete operation
        if(customerAccount.getOperationType().equals("Delete")){
            if(cimCustomerAccountRepository.existsById(customerAccount.getSnapshotData().getId())){
                deleteSnapshot(customerAccount);
                log.debug("**Deleting incoming customer account snapshot for ID: {} ", customerAccount.getSnapshotData().getId());
            }else{
                log.info("**customer account with ID '{}' does not exist", customerAccount.getSnapshotData().getId());
            }
        }
    }

    private void saveSnapshot(CustomerAccount customerAccount) throws JsonProcessingException {

        //set changeTimeStamp for customer account
        customerAccount.getSnapshotData().setChangeTimestamp(customerAccount.getChangeTimestamp());

        //Load customer account into each new billing accounts, and set isTest from parent
        if(customerAccount.getSnapshotData().getBillingAccounts() != null) {
            customerAccount.getSnapshotData().getBillingAccounts().stream().forEach(billingAccount -> {
                billingAccount.setCustomerAccountSnapshotData(customerAccount.getSnapshotData());
                billingAccount.setIsTest(customerAccount.getSnapshotData().getIsTest());

                //set billing contact id for billing account
                customerAccount.getSnapshotData().getBillingContacts().forEach(billingContact -> {
                    if (billingAccount.getBillingContact().equals(billingContact.getId())) billingAccount.setBillingContactId(billingContact.getContact());
                });
            });
        }

        //save snapshot
        cimCustomerAccountRepository.save(customerAccount.getSnapshotData());
        log.info("**Persisted new Customer Account snapshot to database\nCUSTOMER ACCOUNT ID: {}", customerAccount.getSnapshotData().getId());

    }

    private void updateSnapshot(CustomerAccount customerAccount) throws JsonProcessingException {

        //retrieve existing customer account
        CustomerAccountSnapshotData existingCustomerAccount = cimCustomerAccountRepository.findById(customerAccount.getSnapshotData().getId()).get();

        //update mutable fields
        existingCustomerAccount.setName(customerAccount.getSnapshotData().getName());
        existingCustomerAccount.setCustomerAccountNumber(customerAccount.getSnapshotData().getCustomerAccountNumber());
        existingCustomerAccount.setStatus(customerAccount.getSnapshotData().getStatus());
        existingCustomerAccount.setParentCustomerCategory(customerAccount.getSnapshotData().getParentCustomerCategory());
        existingCustomerAccount.setPrimaryContact(customerAccount.getSnapshotData().getPrimaryContact());
        existingCustomerAccount.setCustomerCategory(customerAccount.getSnapshotData().getCustomerCategory());
        existingCustomerAccount.setCreatedWhen(customerAccount.getSnapshotData().getCreatedWhen());
        existingCustomerAccount.setChangeTimestamp(customerAccount.getChangeTimestamp());
        existingCustomerAccount.setIsTest(customerAccount.getSnapshotData().getIsTest());
        existingCustomerAccount.setSnapshot(customerAccount.getSnapshotData().getSnapshot());
        log.debug("**Mutable fields for existing customer account set.");

        //load customer account into new billing accounts, along with isTest
        if(customerAccount.getSnapshotData().getBillingAccounts() == null){
            existingCustomerAccount.getBillingAccounts().clear();
            log.debug("**existing billing account cleared");
        }
        else {
            customerAccount.getSnapshotData().getBillingAccounts().stream().forEach(billingAccount -> {
                billingAccount.setCustomerAccountSnapshotData(existingCustomerAccount);
                billingAccount.setIsTest(customerAccount.getSnapshotData().getIsTest());

                //set billing contact id for billing account
                customerAccount.getSnapshotData().getBillingContacts().forEach(billingContact -> {
                    if (billingAccount.getBillingContact().equals(billingContact.getId())) billingAccount.setBillingContactId(billingContact.getContact());
                });
            });

            //load new billing accounts into existing customer account
            existingCustomerAccount.getBillingAccounts().clear();
            existingCustomerAccount.getBillingAccounts().addAll(customerAccount.getSnapshotData().getBillingAccounts());
        }

        //update snapshot
        cimCustomerAccountRepository.save(existingCustomerAccount);
        log.info("**Persisted updated Customer Account snapshot to database\nCUSTOMER ACCOUNT ID: {}", existingCustomerAccount.getId());
    }

    private void deleteSnapshot(CustomerAccount customerAccount) {
        cimCustomerAccountRepository.delete(customerAccount.getSnapshotData());
        log.info("**Deleted Customer Account snapshot from database\nCUSTOMER ACCOUNT ID: {}", customerAccount.getSnapshotData().getId());
    }

}
