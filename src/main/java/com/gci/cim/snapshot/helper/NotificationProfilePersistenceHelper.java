package com.gci.cim.snapshot.helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfile;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfileSnapshotData;
import com.gci.cim.snapshot.repository.CIMNotificationProfileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;

@Slf4j
@Component
@Transactional
public class NotificationProfilePersistenceHelper {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CIMNotificationProfileRepository cimNotificationProfileRepository;

    public void persistNotificationProfileSnapshot(NotificationProfile notificationProfile) throws JsonProcessingException {

        //null check
        if (notificationProfile == null) {
            log.debug("**Null 'notificationProfile' input sent to persistNotificationProfileSnapshot() method.");
            return;
        }

        //create operation
        if(notificationProfile.getOperationType().equals("Create")) {
            if (!cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId())) {
                saveSnapshot(notificationProfile);
                log.debug("**Saving incoming notification profile snapshot for ID: {}", notificationProfile.getSnapshotData().getId());
            }else{
                // Only Update if new snapshot date is later than existing row
                NotificationProfileSnapshotData existingNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).orElse(null);
                if ((existingNotificationProfile != null) && ((existingNotificationProfile.getChangeTimestamp() == null) || (existingNotificationProfile.getChangeTimestamp().getTime() < notificationProfile.getChangeTimestamp().getTime()))) {
                    updateSnapshot(notificationProfile);
                    log.debug("**Updating incoming notification profile snapshot for ID: {}", notificationProfile.getSnapshotData().getId());
                } else {
                    log.debug("Ignoring Snapshot that is older than row for NotificationProfile: {} ", existingNotificationProfile.getId());
                }
            }
        }

        //update operation
        if (notificationProfile.getOperationType().equals("Update")){
            if (cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId())) {
                // Only Update if new snapshot date is later than existing row
                NotificationProfileSnapshotData existingNotificationProfile = cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).orElse(null);
                if ((existingNotificationProfile != null) && ((existingNotificationProfile.getChangeTimestamp() == null) || (existingNotificationProfile.getChangeTimestamp().getTime() < notificationProfile.getChangeTimestamp().getTime()))) {
                    updateSnapshot(notificationProfile);
                    log.debug("**Updating incoming notification profile snapshot for ID: {}", notificationProfile.getSnapshotData().getId());
                } else {
                    log.debug("Ignoring Snapshot that is older than row for NotificationProfile: {} ", existingNotificationProfile.getId());
                    System.out.println("Ignoring Snapshot that is older than row for NotificationProfile: " + existingNotificationProfile.getId());
                }
            }else{
                saveSnapshot(notificationProfile);
                log.debug("**Saving incoming notification profile snapshot for ID: {}", notificationProfile.getSnapshotData().getId());
            }
        }

        //delete operation
        if(notificationProfile.getOperationType().equals("Delete")){
            if(cimNotificationProfileRepository.existsById(notificationProfile.getSnapshotData().getId())){
                deleteSnapshot(notificationProfile);
                log.debug("**Deleting incoming notification profile snapshot for ID: {}", notificationProfile.getSnapshotData().getId());
            }else{
                log.debug("notification profile with id '{}' does not exist", notificationProfile.getSnapshotData().getId());
                //System.out.println("**notification profile does not exist");
            }
        }
    }

    private void saveSnapshot(NotificationProfile notificationProfile) throws JsonProcessingException {

        //set changeTimestamp for notification profile
        notificationProfile.getSnapshotData().setChangeTimestamp(notificationProfile.getChangeTimestamp());

        //Persist new notification profile
        NotificationProfileSnapshotData newNotificationProfile = cimNotificationProfileRepository.save(notificationProfile.getSnapshotData());
        log.info("**Persisted new Notification Profile snapshot to database\nNOTIFICATION PROFILE ID: {}", notificationProfile.getSnapshotData().getId());

    }

    private void updateSnapshot(NotificationProfile notificationProfile) throws JsonProcessingException {

        //find existing notification profile
        NotificationProfileSnapshotData existingNotificationProfile =
                cimNotificationProfileRepository.findById(notificationProfile.getSnapshotData().getId()).get();

        //set mutable fields
        existingNotificationProfile.setEmailChannel(notificationProfile.getSnapshotData().getEmailChannel());
        existingNotificationProfile.setMobileChannel(notificationProfile.getSnapshotData().getMobileChannel());
        existingNotificationProfile.setNotificationType(notificationProfile.getSnapshotData().getNotificationType());
        existingNotificationProfile.setContact(notificationProfile.getSnapshotData().getContact());
        existingNotificationProfile.setIsTest(notificationProfile.getSnapshotData().getIsTest());
        existingNotificationProfile.setChangeTimestamp(notificationProfile.getChangeTimestamp());
        existingNotificationProfile.setCreatedWhen(notificationProfile.getSnapshotData().getCreatedWhen());
        existingNotificationProfile.setSnapshot(notificationProfile.getSnapshotData().getSnapshot());
        existingNotificationProfile.setIsTest(notificationProfile.getSnapshotData().getIsTest());
        log.debug("**Mutable fields for notification profile have been set.");

        //update notification profile
        cimNotificationProfileRepository.save(existingNotificationProfile);
        log.info("**Persisted updated Notification Profile snapshot to database\nNOTIFICATION PROFILE ID: {}", existingNotificationProfile.getId());
    }

    private void deleteSnapshot(NotificationProfile notificationProfile){
        cimNotificationProfileRepository.delete(notificationProfile.getSnapshotData());
        log.info("**Deleted Notification Profile snapshot from database\nNOTIFICATION PROFILE ID: {}", notificationProfile.getSnapshotData().getId());
    }

}
