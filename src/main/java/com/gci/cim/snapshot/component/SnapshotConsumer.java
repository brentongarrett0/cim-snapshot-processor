package com.gci.cim.snapshot.component;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gci.cim.snapshot.properties.CIMSnapshotKafkaConsumerProperties;
import com.gci.cim.snapshot.properties.CIMSnapshotKafkaProperties;
import com.gci.cim.snapshot.service.CIMSnapshotService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import com.gci.cim.snapshot.metrics.Metrics;

import java.text.ParseException;

@Slf4j
@Profile("!test")
@Component("snapshotConsumer")
public class SnapshotConsumer {

    @Autowired
    private CIMSnapshotService cimSnapshotService;

    @Autowired
    public CIMSnapshotKafkaProperties cimSnapshotKafkaProperties;

    @Autowired
    public CIMSnapshotKafkaConsumerProperties cimSnapshotKafkaConsumerProperties;

    @Autowired
    private Metrics metrics;

    @KafkaListener(topics = "#{snapshotConsumer.cimSnapshotKafkaProperties.getTopics()}",
            groupId = "#{snapshotConsumer.cimSnapshotKafkaProperties.getGroupId()}",
            autoStartup = "#{snapshotConsumer.cimSnapshotKafkaConsumerProperties.getAutoStartup()}")
    public void consume(@Payload String message, Acknowledgment acknowledgment) throws JsonProcessingException, ParseException {

        log.info("**Message consumed from Kafka topic\nTOPIC: {}\nGROUPID: {}\nMESSAGE: {}",
                cimSnapshotKafkaProperties.getTopics(),
                cimSnapshotKafkaProperties.getGroupId(),
                message
        );

        // Try to process, if successful, Acknowledge... otherwisde Nack with a delay
        try {
            cimSnapshotService.submitSnapshot(message);
            log.info("Acknowledging successful submit");
            acknowledgment.acknowledge();
            metrics.getProcessingFailed().set(0);
        } catch (Exception e) {
            log.info("Nack unsuccessful submit", e);
            acknowledgment.nack(cimSnapshotKafkaConsumerProperties.getAckRetryDelay()); // retry delay at 15 seconds
            metrics.getProcessingFailed().set(1);
       }
    }
}
