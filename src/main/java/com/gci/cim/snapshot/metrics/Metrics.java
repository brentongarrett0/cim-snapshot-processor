package com.gci.cim.snapshot.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Getter
public class Metrics
{
    private Counter customerAccountCounter;
    private Counter contactCounter;
    private Counter bpiCounter;
    private Counter notificationProfileCounter;
    private Counter notificationTypeDictionaryCounter;
    private Counter badSnapshotCounter;
    private AtomicInteger processingFailed;

    @Autowired
    public Metrics(MeterRegistry meterRegistry)
    {
        customerAccountCounter = Counter.builder("snapshots.processed")
                                          .description("Snapshot Processed")
                                          .tag("type", "customer_account")
                                          .register(meterRegistry);
        contactCounter = Counter.builder("snapshots.processed")
                                          .description("Snapshot Processed")
                                          .tag("type", "contact")
                                          .register(meterRegistry);
        bpiCounter = Counter.builder("snapshots.processed")
                                          .description("Snapshot Processed")
                                          .tag("type", "bpi")
                                          .register(meterRegistry);
        notificationProfileCounter = Counter.builder("snapshots.processed")
                                          .description("Snapshot Processed")
                                          .tag("type", "notification_profile")
                                          .register(meterRegistry);
        notificationTypeDictionaryCounter = Counter.builder("snapshots.processed")
                                          .description("Snapshot Processed")
                                          .tag("type", "notification_type_dictionary")
                                          .register(meterRegistry);
        badSnapshotCounter = Counter.builder("bad.snapshots")
                                          .description("Bad Snapshots")
                                          .register(meterRegistry);

        // Processing Failed / Retrying Gauge
        processingFailed = new AtomicInteger(0);
        Gauge.builder("snapshot.processing.failed", processingFailed, AtomicInteger::get)
            .description("Snapshot Processing Failed - Check the DB Connection") // optional
            .register(meterRegistry);
    }
}
