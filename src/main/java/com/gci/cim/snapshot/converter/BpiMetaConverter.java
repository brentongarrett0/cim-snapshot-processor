package com.gci.cim.snapshot.converter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.bpi.BpiMeta;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.AttributeConverter;

public class BpiMetaConverter implements AttributeConverter<BpiMeta, String> {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public String convertToDatabaseColumn(BpiMeta bpiMeta) {
        if (bpiMeta == null) return null;

        String bpiMetaJson = null;
        try {
            bpiMetaJson = objectMapper.writeValueAsString(bpiMeta);
        } catch (final JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return bpiMetaJson;
    }

    @Override
    public BpiMeta convertToEntityAttribute(String bpiMetaJson) {
        if (bpiMetaJson == null)
            return null;
        BpiMeta bpiMeta = null;
        try {
            bpiMeta = objectMapper.readValue(bpiMetaJson, BpiMeta.class);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return bpiMeta;
    }
}
