package com.gci.cim.snapshot.converter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.model.Violations;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.AttributeConverter;
import java.util.Arrays;
import java.util.List;

public class ViolationsConverter implements AttributeConverter<List<Violations>, String> {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public String convertToDatabaseColumn(List<Violations> violations) {
        if (violations == null) return null;

        String violationsJson = null;
        try {
            violationsJson = objectMapper.writeValueAsString(violations);
        } catch (final JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return violationsJson;
    }

    @Override
    public List<Violations> convertToEntityAttribute(String violationsJson) {
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

        if (violationsJson == null)
            return null;
        List<Violations> violations = null;
        try {
            violations = Arrays.asList(objectMapper.readValue(violationsJson, Violations[].class));
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return violations;
    }
}
