package com.gci.cim.snapshot.converter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfileMeta;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.AttributeConverter;

public class NotificationProfileMetaConverter implements AttributeConverter<NotificationProfileMeta, String> {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public String convertToDatabaseColumn(NotificationProfileMeta notificationProfileMeta) {
        if (notificationProfileMeta == null) return null;

        String notificationProfileMetaJson = null;
        try {
            notificationProfileMetaJson = objectMapper.writeValueAsString(notificationProfileMeta);
        } catch (final JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return notificationProfileMetaJson;
    }

    @Override
    public NotificationProfileMeta convertToEntityAttribute(String notificationProfileMetaJson) {
        if (notificationProfileMetaJson == null)
            return null;
        NotificationProfileMeta notificationProfileMeta = null;
        try {
            notificationProfileMeta = objectMapper.readValue(notificationProfileMetaJson, NotificationProfileMeta.class);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return notificationProfileMeta;
    }
}