package com.gci.cim.snapshot.converter;

import javax.persistence.AttributeConverter;

public class IsLifelineConverter implements AttributeConverter<String, Integer> {

    @Override
    public Integer convertToDatabaseColumn(String attribute) {
        if(attribute == null) return 0;
        if(attribute.equalsIgnoreCase("Yes")){
            return 1;
        }else{
            return  0;
        }
    }

    @Override
    public String convertToEntityAttribute(Integer dbData) {
        if(dbData == null) return "No";
        if(dbData == 1){
            return "Yes";
        }else{
            return "No";
        }
    }
}
