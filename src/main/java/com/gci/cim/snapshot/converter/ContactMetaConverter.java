package com.gci.cim.snapshot.converter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.contact.ContactMeta;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.AttributeConverter;

public class ContactMetaConverter implements AttributeConverter<ContactMeta, String> {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public String convertToDatabaseColumn(ContactMeta contactMeta) {
        if (contactMeta == null)
            return null;
        String contactMetaJson = null;
        try {
            contactMetaJson = objectMapper.writeValueAsString(contactMeta);
        } catch (final JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return contactMetaJson;
    }

    @Override
    public ContactMeta convertToEntityAttribute(String contactMetaJson) {
        if (contactMetaJson == null)
            return null;
        ContactMeta contactMeta = null;
        try {
            contactMeta = objectMapper.readValue(contactMetaJson, ContactMeta.class);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return contactMeta;
    }
}
