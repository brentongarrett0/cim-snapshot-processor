package com.gci.cim.snapshot.converter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccountMeta;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.AttributeConverter;

public class CustomerAccountMetaConverter implements AttributeConverter<CustomerAccountMeta, String> {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public String convertToDatabaseColumn(CustomerAccountMeta customerAccountMeta) {
        if (customerAccountMeta == null)
            return null;
        String customerAccountMetaJson = null;
        try {
            customerAccountMetaJson = objectMapper.writeValueAsString(customerAccountMeta);
        } catch (final JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return customerAccountMetaJson;
    }

    @Override
    public CustomerAccountMeta convertToEntityAttribute(String customerAccountMetaJson) {
        if (customerAccountMetaJson == null)
            return null;
        CustomerAccountMeta customerAccountMeta = null;
        try {
            customerAccountMeta = objectMapper.readValue(customerAccountMetaJson, CustomerAccountMeta.class);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return customerAccountMeta;
    }
}
