package com.gci.cim.snapshot.converter;
import javax.persistence.AttributeConverter;

public class MobileChannelConverter implements AttributeConverter<String, Integer>{

    @Override
    public Integer convertToDatabaseColumn(String attribute) {
        if(attribute.equals("Yes")){
            return 1;
        }else{
            return  0;
        }
    }

    @Override
    public String convertToEntityAttribute(Integer dbData) {
        if(dbData == 1){
            return "Yes";
        }else{
            return "No";
        }
    }
}

