package com.gci.cim.snapshot.converter;
import javax.persistence.AttributeConverter;

public class AutoPaymentStatusConverter  implements AttributeConverter<String, Integer> {

    @Override
    public Integer convertToDatabaseColumn(String attribute) {
        if(attribute.equals("On")){
            return 1;
        }else{
            return  0;
        }
    }

    @Override
    public String convertToEntityAttribute(Integer dbData) {
        if(dbData == 1){
            return "On";
        }else{
            return "Off";
        }
    }
}


