package com.gci.cim.snapshot.service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CIMSnapshotAdminService {

    @Autowired
    BuildProperties buildProperties;

    public String getVersion(){
        return "v" + buildProperties.getVersion();
    }

}
