package com.gci.cim.snapshot.service;
import com.gci.cim.snapshot.properties.CIMSnapshotKafkaProducerProperties;
import com.gci.cim.snapshot.properties.CIMSnapshotKafkaProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Profile("!test")
public class CIMSnapshotProducerService {

    @Autowired
    private CIMSnapshotKafkaProperties cimSnapshotKafkaProperties;

    @Autowired
    private CIMSnapshotKafkaProducerProperties cimSnapshotKafkaProducerProperties;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void publishMessage(String message) {
        if(cimSnapshotKafkaProducerProperties.getIsEnabled() == "true") {
            log.info("#### -> Producing message to topic '" + cimSnapshotKafkaProperties.getTopics() + "' -> " + message);
            kafkaTemplate.send(cimSnapshotKafkaProperties.getTopics(), message);
        }
    }

}
