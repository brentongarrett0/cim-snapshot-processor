package com.gci.cim.snapshot.service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gci.cim.snapshot.entity.BadSnapshot;
import com.gci.cim.snapshot.entity.bpi.Bpi;
import com.gci.cim.snapshot.entity.contact.Contact;
import com.gci.cim.snapshot.entity.contact.ContactRole;
import com.gci.cim.snapshot.entity.contact.ContactSnapshotData;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfile;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionary;
import com.gci.cim.snapshot.helper.*;
import com.gci.cim.snapshot.metrics.Metrics;
import com.gci.cim.snapshot.model.ValidationResponse;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccount;
import com.gci.cim.snapshot.model.Violations;
import com.gci.cim.snapshot.repository.CIMBadSnapshotRepository;
import com.gci.cim.snapshot.repository.CIMContactRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.gci.sdk.logging.EIPLogger;
import com.gci.sdk.logging.EIPReport;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import javax.transaction.Transactional;

@Slf4j
@Service
public class CIMSnapshotService {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ValidationHelper validationHelper;

    @Autowired
    private CustomerAccountPersistenceHelper customerAccountPersistenceHelper;

    @Autowired
    private ContactPersistenceHelper contactPersistenceHelper;

    @Autowired
    private BpiPersistenceHelper bpiPersistenceHelper;

    @Autowired
    private NotificationProfilePersistenceHelper notificationProfilePersistenceHelper;

    @Autowired
    private NotificationTypeDictionaryPersistenceHelper notificationTypeDictionaryPersistenceHelper;

    @Autowired
    private CIMBadSnapshotRepository cimBadSnapshotRepository;

    @Autowired
    private Metrics metrics;

    @Autowired
    private EIPLogger eipLogger;

    @Autowired
    private CIAMAPIRegistrationHelper ciamapiRegistrationHelper;

    @Autowired
    private CIMContactRepository cimContactRepository;

    public void submitSnapshot (String snapshot) throws JsonProcessingException, ParseException {
        
        // Start time for logging
        DateTime start = DateTime.now();
        eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_RECEIVED")
            .setStatus(EIPReport.Status.SUCCESS)
            .setProcessingTime(new Duration(start, DateTime.now()))
            .setKV("rawPayload", snapshot));

        //consume the  object type and validation status
        final ValidationResponse validationResponse = validateSnapshot(snapshot);
        final String snapshotType = validationResponse.getSnapshotType();
        final Boolean isValid = validationResponse.getIsValid();

        //process snapshots
        if(snapshotType.equals("Customer Account")) processCustomerAccountSnapshot(validationResponse, isValid, snapshot, start);
        if(snapshotType.equals("Contact")) processContactSnapshot(validationResponse, isValid, snapshot, start);
        if(snapshotType.equals("BPI")) processBPISnapshot(validationResponse, isValid, snapshot, start);
        if(snapshotType.equals("Notification Profile")) processNotificationProfileSnapshot(validationResponse, isValid, snapshot, start);
        if(snapshotType.equals("Notification Type Dictionary")) processNotificationTypeDictionarySnapshot(validationResponse, isValid, snapshot, start);
        if(snapshotType.equals("Unknown")) processUnknownSnapshot(validationResponse, isValid, snapshot, start);
        if(snapshotType.equals("Unparsable")) processUnparsableSnapshot(validationResponse, isValid, snapshot, start);
        if(snapshotType.equals("Unprovided")) processUnprovidedSnapshot(validationResponse, isValid, snapshot, start);

    }


    public ValidationResponse validateSnapshot(String snapshotString) throws JsonProcessingException {

        //parse snapshot to json
        Map<String,Object> payload = parseSnapshot(snapshotString);

        //return 'JSON parse' violation if payload cannot be parsed to json
        if (payload.containsKey("violations")) return objectMapper.convertValue(payload, ValidationResponse.class);

        //return 'snapshot type unprovided' violation response if payload doesn't contain snapshotType field
        if(!payload.containsKey("snapshotType") || payload.get("snapshotType") == null ) return ValidationResponse.builder()
                .snapshotType("Unprovided")
                .isValid(false)
                .violations(new ArrayList<Violations>(){{
                    add(new Violations(null,"JSON snapshot does not contain 'snapshotType' field"));
                }})
                .build();

        //dynamically validate payload
        switch (payload.get("snapshotType").toString()) {
            case "Contact":
                return validationHelper.validateContact(payload);
            case "Customer Account":
                return validationHelper.validateCustomerAccount(payload);
            case "BPI":
                return validationHelper.validateBpi(payload);
            case "Notification Profile":
                return validationHelper.validateNotificationProfile(payload);
            case "Notification Type Dictionary":
                return validationHelper.validateNotificationTypeDictionary(payload);
            default:
                return ValidationResponse.builder()
                        .snapshotType("Unknown")
                        .isValid(false)
                        .violations(new ArrayList<Violations>(){{
                            add(new Violations(null,"Unknown snapshot type"));
                        }})
                        .build();
        }
    }

    private Map<String, Object> parseSnapshot(String payloadString) {

        try {
            return objectMapper.readValue(payloadString, Map.class);
        } catch (JsonProcessingException e) {
            ValidationResponse validationResponse = ValidationResponse.builder()
                    .snapshotType("Unparsable")
                    .isValid(false)
                    .violations(new ArrayList<Violations>(){{
                        add(new Violations(null,"Cannot parse snapshot to JSON"));
                    }})
                    .build();
            return objectMapper.convertValue(validationResponse, Map.class);
        }

    }

    //## PROCESS SNAPSHOT METHODS
    private void processCustomerAccountSnapshot (ValidationResponse validationResponse, boolean isValid, String snapshot, DateTime start) throws JsonProcessingException {

            if(isValid) {
                CustomerAccount customerAccount = objectMapper.readValue(snapshot, CustomerAccount.class);
                customerAccount.getSnapshotData().setSnapshot(snapshot);
                log.debug("**saving valid Customer Account");
                customerAccountPersistenceHelper.persistCustomerAccountSnapshot(customerAccount);
                metrics.getCustomerAccountCounter().increment();
                eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                         .setStatus(EIPReport.Status.SUCCESS)
                         .setProcessingTime(new Duration(start, DateTime.now()))
                         .setKV("rawPayload", snapshot)
                         .setKV("snapshotType", "Customer Account"));
            }else{
                BadSnapshot badSnapshot = BadSnapshot.builder()
                        .snapshotType(validationResponse.getSnapshotType())
                        .violations(validationResponse.getViolations())
                        .snapshot(snapshot)
                        .violationCategory(ViolationCategories.INVALID_CUSTOMER_ACCOUNT.name())
                        .createdWhen(new Timestamp(System.currentTimeMillis()))
                        .build();

                log.warn("**Stashing invalid Customer Account to bad snapshot table\nMessage: {}", badSnapshot);
                cimBadSnapshotRepository.save(badSnapshot);
                metrics.getBadSnapshotCounter().increment();
                String errorMessage = "Invalid Customer Account.";
                eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                         .setStatus(EIPReport.Status.FAILURE)
                         .setProcessingTime(new Duration(start, DateTime.now()))
                         .setKV("rawPayload", snapshot)
                         .setKV("snapshotType", "Customer Account")
                         .setKV("errorMessage", errorMessage));
            }
    }

    private void processContactSnapshot (ValidationResponse validationResponse, boolean isValid, String snapshot, DateTime start) throws JsonProcessingException {

            if(isValid) {
                Contact contact = objectMapper.readValue(snapshot, Contact.class);
                contact.getSnapshotData().setSnapshot(snapshot);
                log.debug("**Saving valid Contact");

                //save
                contactPersistenceHelper.persistContactSnapshot(contact);
                metrics.getContactCounter().increment();
                eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                         .setStatus(EIPReport.Status.SUCCESS)
                         .setProcessingTime(new Duration(start, DateTime.now()))
                         .setKV("rawPayload", snapshot)
                         .setKV("snapshotType", "Contact"));

                ////call ciam api request
                ciamApiCall(contact);
                //ContactSnapshotData savedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).orElse(null);

                //pull mygciuser for contact that you just saved
                //if ((savedContact != null) && ((savedContact.getChangeTimestamp() == null) || (savedContact.getChangeTimestamp().getTime() == contact.getChangeTimestamp().getTime()))){

                    //System.out.println("**preparing to gather contact roles");
                    //List<ContactRole> contactRoles = savedContact.getContactRoles();

                    //This is to deal with lazy loading.
                    //contactRoles.size();

                    //boolean isMyGCIUser = contactRoles.stream().anyMatch(contactRole -> contactRole.getRole().equalsIgnoreCase("MyGCI User"));
                    //log.info("**[CIMSnapshotService] contact is mygci user: {}" , isMyGCIUser);

                    // if mygcuuser is true for the contact then send request
                    //if(isMyGCIUser) {
                    //    log.info("**making call to ciam api for contact id: {} ", savedContact.getContactId());
                    //    ciamapiRegistrationHelper.sendInviteRequest(savedContact.getContactId());
                    //}

                //}

            }else{
                BadSnapshot badSnapshot = BadSnapshot.builder()
                        .snapshotType(validationResponse.getSnapshotType())
                        .violations(validationResponse.getViolations())
                        .snapshot(snapshot)
                        .violationCategory(ViolationCategories.INVALID_CONTACT.name())
                        .createdWhen(new Timestamp(System.currentTimeMillis()))
                        .build();

                log.warn("**Stashing invalid Contact to bad snapshot table\nMessage: {}", badSnapshot);
                cimBadSnapshotRepository.save(badSnapshot);
                metrics.getBadSnapshotCounter().increment();
                String errorMessage = "Invalid Contact.";
                eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                         .setStatus(EIPReport.Status.FAILURE)
                         .setProcessingTime(new Duration(start, DateTime.now()))
                         .setKV("rawPayload", snapshot)
                         .setKV("snapshotType", "Contact")
                         .setKV("errorMessage", errorMessage));
            }
    }

    private void processBPISnapshot (ValidationResponse validationResponse, boolean isValid, String snapshot, DateTime start) throws JsonProcessingException, ParseException {

            if(isValid){
                Bpi bpi = objectMapper.readValue(snapshot, Bpi.class);
                bpi.getSnapshotData().setSnapshot(snapshot);
                log.debug("**Saving valid BPI");
                bpiPersistenceHelper.persistBpiSnapshot(bpi);
                metrics.getBpiCounter().increment();
                eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                         .setStatus(EIPReport.Status.SUCCESS)
                         .setProcessingTime(new Duration(start, DateTime.now()))
                         .setKV("rawPayload", snapshot)
                         .setKV("snapshotType", "BPI"));
            }else{
                BadSnapshot badSnapshot = BadSnapshot.builder()
                        .snapshotType(validationResponse.getSnapshotType())
                        .violations(validationResponse.getViolations())
                        .snapshot(snapshot)
                        .violationCategory(ViolationCategories.INVALID_BPI.name())
                        .createdWhen(new Timestamp(System.currentTimeMillis()))
                        .build();

                log.warn("**Stashing invalid BPI to bad snapshot table\nMessage: {}", badSnapshot);
                cimBadSnapshotRepository.save(badSnapshot);
                metrics.getBadSnapshotCounter().increment();
                String errorMessage = "Invalid BPI.";
                eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                         .setStatus(EIPReport.Status.FAILURE)
                         .setProcessingTime(new Duration(start, DateTime.now()))
                         .setKV("rawPayload", snapshot)
                         .setKV("snapshotType", "BPI")
                         .setKV("errorMessage", errorMessage));
            }

    }

    private void processNotificationProfileSnapshot (ValidationResponse validationResponse, boolean isValid, String snapshot, DateTime start) throws JsonProcessingException {

            if(isValid) {
                NotificationProfile notificationProfile = objectMapper.readValue(snapshot, NotificationProfile.class);
                notificationProfile.getSnapshotData().setSnapshot(snapshot);
                log.debug("**Saving valid Notification Profile");
                notificationProfilePersistenceHelper.persistNotificationProfileSnapshot(notificationProfile);
                metrics.getNotificationProfileCounter().increment();
                eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                         .setStatus(EIPReport.Status.SUCCESS)
                         .setProcessingTime(new Duration(start, DateTime.now()))
                         .setKV("rawPayload", snapshot)
                         .setKV("snapshotType", "Notification Profile"));
            }else{
                BadSnapshot badSnapshot = BadSnapshot.builder()
                        .snapshotType(validationResponse.getSnapshotType())
                        .violations(validationResponse.getViolations())
                        .snapshot(snapshot)
                        .violationCategory(ViolationCategories.INVALID_NOTIFICATION_PROFILE.name())
                        .createdWhen(new Timestamp(System.currentTimeMillis()))
                        .build();

                log.warn("**Stashing invalid Notification Profile to bad snapshot table\nMessage: {}", badSnapshot);
                cimBadSnapshotRepository.save(badSnapshot);
                metrics.getBadSnapshotCounter().increment();
                String errorMessage = "Invalid Notificaiton Profile.";
                eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                         .setStatus(EIPReport.Status.FAILURE)
                         .setProcessingTime(new Duration(start, DateTime.now()))
                         .setKV("rawPayload", snapshot)
                         .setKV("snapshotType", "Notification Profile")
                         .setKV("errorMessage", errorMessage));
            }

    }

    private void processNotificationTypeDictionarySnapshot (ValidationResponse validationResponse, boolean isValid, String snapshot, DateTime start) throws JsonProcessingException {

            if(isValid) {
                NotificationTypeDictionary notificationTypeDictionary = objectMapper.readValue(snapshot, NotificationTypeDictionary.class);
                notificationTypeDictionary.getSnapshotData().setSnapshot(snapshot);
                log.debug("**Saving valid Notification Type Dictionary");
                notificationTypeDictionaryPersistenceHelper.persistNotificationTypeDictionarySnapshot(notificationTypeDictionary);
                metrics.getNotificationTypeDictionaryCounter().increment();
                eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                         .setStatus(EIPReport.Status.SUCCESS)
                         .setProcessingTime(new Duration(start, DateTime.now()))
                         .setKV("rawPayload", snapshot)
                         .setKV("snapshotType", "Notification Type Dictionary"));
            }else{
                BadSnapshot badSnapshot = BadSnapshot.builder()
                        .snapshotType(validationResponse.getSnapshotType())
                        .violations(validationResponse.getViolations())
                        .snapshot(snapshot)
                        .violationCategory(ViolationCategories.INVALID_NOTIFICATION_TYPE_DICTIONARY.name())
                        .createdWhen(new Timestamp(System.currentTimeMillis()))
                        .build();

                log.warn("**Stashing invalid Notification Profile to bad snapshot table\nMessage: {}", badSnapshot);
                cimBadSnapshotRepository.save(badSnapshot);
                metrics.getBadSnapshotCounter().increment();
                String errorMessage = "Invalid Notification Type Dictionary.";
                eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                         .setStatus(EIPReport.Status.FAILURE)
                         .setProcessingTime(new Duration(start, DateTime.now()))
                         .setKV("rawPayload", snapshot)
                         .setKV("snapshotType", "Notification Type Dictionary")
                         .setKV("errorMessage", errorMessage));
            }

    }

    private void processUnknownSnapshot (ValidationResponse validationResponse, boolean isValid, String snapshot, DateTime start) throws JsonProcessingException {

        BadSnapshot badSnapshot = BadSnapshot.builder()
                .snapshotType(validationResponse.getSnapshotType())
                .violations(validationResponse.getViolations())
                .snapshot(snapshot)
                .violationCategory(ViolationCategories.UNKNOWN_SNAPSHOT_TYPE.name())
                .createdWhen(new Timestamp(System.currentTimeMillis()))
                .build();

        log.warn("**Stashing Unknown snapshot to bad snapshot table\nMessage: {}", badSnapshot);
        cimBadSnapshotRepository.save(badSnapshot);
        metrics.getBadSnapshotCounter().increment();
        String errorMessage = "Unknown Snapshot.";
        eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                    .setStatus(EIPReport.Status.FAILURE)
                    .setProcessingTime(new Duration(start, DateTime.now()))
                    .setKV("rawPayload", snapshot)
                    .setKV("errorMessage", errorMessage));
    }

    private void processUnparsableSnapshot (ValidationResponse validationResponse, boolean isValid, String snapshot, DateTime start) throws JsonProcessingException {

        BadSnapshot badSnapshot = BadSnapshot.builder()
                .snapshotType(validationResponse.getSnapshotType())
                .violations(validationResponse.getViolations())
                .snapshot(snapshot)
                .violationCategory(ViolationCategories.JSON_PARSE.name())
                .createdWhen(new Timestamp(System.currentTimeMillis()))
                .build();

        log.warn("**Stashing Unparsable snapshot to bad snapshot table\nMessage: {}", badSnapshot);
        cimBadSnapshotRepository.save(badSnapshot);
        metrics.getBadSnapshotCounter().increment();
        String errorMessage = "Unparsable Snapshot.";
        eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                    .setStatus(EIPReport.Status.FAILURE)
                    .setProcessingTime(new Duration(start, DateTime.now()))
                    .setKV("rawPayload", snapshot)
                    .setKV("errorMessage", errorMessage));
    }
    private void processUnprovidedSnapshot (ValidationResponse validationResponse, boolean isValid, String snapshot, DateTime start) throws JsonProcessingException {

        BadSnapshot badSnapshot = BadSnapshot.builder()
                .snapshotType(validationResponse.getSnapshotType())
                .violations(validationResponse.getViolations())
                .snapshot(snapshot)
                .violationCategory(ViolationCategories.MISSING_SNAPSHOT_TYPE.name())
                .createdWhen(new Timestamp(System.currentTimeMillis()))
                .build();

        log.warn("**Stashing unrecognized JSON object to bad snapshot table\nMessage: {}", badSnapshot);
        cimBadSnapshotRepository.save(badSnapshot);
        metrics.getBadSnapshotCounter().increment();
        String errorMessage = "Unprovided Snapshot.";
        eipLogger.asyncLog(new EIPReport(null, null, "SNAPSHOT_PROCESSED")
                    .setStatus(EIPReport.Status.FAILURE)
                    .setProcessingTime(new Duration(start, DateTime.now()))
                    .setKV("rawPayload", snapshot)
                    .setKV("errorMessage", errorMessage));
    }
    
    //## VIOLATION CATEGORIES
    enum ViolationCategories {
        JSON_PARSE,
        MISSING_SNAPSHOT_TYPE,
        UNKNOWN_SNAPSHOT_TYPE,
        INVALID_BPI,
        INVALID_CUSTOMER_ACCOUNT,
        INVALID_CONTACT,
        INVALID_NOTIFICATION_PROFILE ,
        INVALID_NOTIFICATION_TYPE_DICTIONARY
    }

    //@Transactional
    public void ciamApiCall(Contact contact) throws JsonProcessingException {

        log.info("**entering ciamApiCall(contact)");

        //call ciam api request
        ContactSnapshotData savedContact = cimContactRepository.findById(contact.getSnapshotData().getId()).orElse(null);

        //pull mygciuser for contact that you just saved
        if ((savedContact != null) && ((savedContact.getChangeTimestamp() == null) || (savedContact.getChangeTimestamp().getTime() == contact.getChangeTimestamp().getTime()))){

            log.info("**preparing to gather contact roles");
            List<ContactRole> contactRoles = savedContact.getContactRoles();
            log.info("**gathered contact roles: {}" ,objectMapper.writeValueAsString(contactRoles));

            boolean isMyGCIUser = contactRoles.stream().anyMatch(contactRole -> contactRole.getRole() == null ? false : contactRole.getRole().equalsIgnoreCase("MyGCI User"));
            log.info("**[CIMSnapshotService] contact is mygci user: {}" , isMyGCIUser);

            // if mygcuuser is true for the contact then send request
            if(isMyGCIUser) {
                log.info("**making call to ciam api for contact id: {} ", savedContact.getContactId());
                ciamapiRegistrationHelper.sendInviteRequest(savedContact.getContactId());
            }
    }

    }
}