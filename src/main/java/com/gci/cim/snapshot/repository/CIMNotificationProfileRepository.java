package com.gci.cim.snapshot.repository;
import com.gci.cim.snapshot.entity.notificationProfile.NotificationProfileSnapshotData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CIMNotificationProfileRepository extends CrudRepository<NotificationProfileSnapshotData, String> {}
