package com.gci.cim.snapshot.repository;
import com.gci.cim.snapshot.entity.BadSnapshot;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CIMBadSnapshotRepository extends CrudRepository<BadSnapshot,Long>{}
