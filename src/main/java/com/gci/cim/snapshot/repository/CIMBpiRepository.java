package com.gci.cim.snapshot.repository;
import com.gci.cim.snapshot.entity.bpi.BpiSnapshotData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CIMBpiRepository extends CrudRepository<BpiSnapshotData, String> {}
