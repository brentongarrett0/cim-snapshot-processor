package com.gci.cim.snapshot.repository;
import com.gci.cim.snapshot.entity.notificationTypeDictionary.NotificationTypeDictionarySnapshotData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CIMNotificationTypeDictionaryRepository extends CrudRepository<NotificationTypeDictionarySnapshotData, String> {}