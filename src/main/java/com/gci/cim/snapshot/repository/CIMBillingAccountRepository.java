package com.gci.cim.snapshot.repository;
import com.gci.cim.snapshot.entity.customerAccount.BillingAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CIMBillingAccountRepository extends CrudRepository<BillingAccount, String>{}
