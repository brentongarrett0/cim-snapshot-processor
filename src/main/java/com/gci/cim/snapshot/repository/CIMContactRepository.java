package com.gci.cim.snapshot.repository;
import com.gci.cim.snapshot.entity.contact.ContactSnapshotData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CIMContactRepository extends CrudRepository<ContactSnapshotData, String>{}