package com.gci.cim.snapshot.repository;
import com.gci.cim.snapshot.entity.customerAccount.CustomerAccountSnapshotData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CimCustomerAccountRepository extends CrudRepository<CustomerAccountSnapshotData, String>{}