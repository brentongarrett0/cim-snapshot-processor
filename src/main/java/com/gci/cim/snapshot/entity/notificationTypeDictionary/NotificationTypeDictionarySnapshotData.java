package com.gci.cim.snapshot.entity.notificationTypeDictionary;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="cim_notification_type_dictionary", uniqueConstraints = {@UniqueConstraint( name = "id_UNIQUE", columnNames={"id"})})
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationTypeDictionarySnapshotData {

    @Id
    @NotNull(message = "You must provide a value for id")
    @Column(name = "id", nullable = false, length = 255)
    private String id;

    @Transient
    private String name;

    @Transient
    private String eventId;

    @Transient
    private String description;

    @Transient
    private String notificationGroup;

    @Transient
    private String mandatoryNotification;

    @Transient
    private String preferencesManagement;

    @Transient
    private String status;

    @Transient
    private List<Recipient> sendTo;

    @Transient
    private String emailChannel;

    @Transient
    private String emailChannelDefaultSetting;

    @Transient
    private String mobileChannel;

    @Transient
    private String mobileChannelDefaultSetting;

    @Transient
    private Timestamp createdWhen;

    @JsonIgnore
    @Column(name = "snapshot", nullable = true, columnDefinition = "JSON")
    private String snapshot;

    @JsonIgnore
    @Column(name = "data", nullable = true, columnDefinition = "JSON")
    private String data;

    @Column(name = "change_timestamp", nullable = true, columnDefinition = "DATETIME")
    private Timestamp changeTimestamp;

    @Column(name = "is_test", nullable = true, columnDefinition = "tinyint(4)")
    private Byte isTest;

}