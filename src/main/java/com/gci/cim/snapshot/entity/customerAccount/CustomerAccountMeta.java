package com.gci.cim.snapshot.entity.customerAccount;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerAccountMeta {

    private String clientId;

    private String conversationId;

    private String requestId;

    private String messageId;

    private String correlationId;

}
