package com.gci.cim.snapshot.entity.contact;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact {

    @NotNull(message = "You must provide a value for snapshotType")
    private String snapshotType;

    @NotNull(message = "You must provide a changeTimestamp for snapshotType")
    private Timestamp changeTimestamp;

    @Valid
    @NotNull(message = "'snapshotData' section must be provided")
    private ContactSnapshotData snapshotData;

    @NotNull(message = "You must provide a value for operationType")
    @Pattern(regexp = "Create|Update|Delete", message = "Value for operationType must be 'Create' or 'Delete'")
    private String operationType;

}
