package com.gci.cim.snapshot.entity.customerAccount;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gci.cim.snapshot.converter.AutoPaymentStatusConverter;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

@Data
@Builder(toBuilder = true)
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cim_billing_account")
@JsonIgnoreProperties(ignoreUnknown = true)
public class BillingAccount {

    @Id
    @NotNull(message = "You must provide a value for 'id'")
    @Column(name = "id", nullable = false, length = 255)
    private String id;

    @NotNull(message = "You must provide a value for 'billingAccountNumber'")
    @Column(name = "billing_account_number", nullable = true, length = 255)
    private String billingAccountNumber;

    @Transient
    @NotNull(message = "You must provide a value for 'billingContact'")
    private String billingContact;

    @Column(name = "billing_contact_id", nullable = true, length = 255)
    private String billingContactId;

    @NotNull(message = "You must provide a value for 'status'")
    @Column(name = "status", nullable = true, length = 255)
    @NotNull(message = "You must provide a value for status")
    private String status;

    @NotNull(message = "You must provide a value for 'autoPaymentStatus'")
    @Pattern(regexp = "Off|On", message = "Value for operationType must either be 'Off' or 'On'")
    @Convert(converter =  AutoPaymentStatusConverter.class)
    @Column(name = "auto_payment_status", nullable = true, columnDefinition = "TINYINT(4)")
    private String autoPaymentStatus;

    @NotNull(message = "You must provide a value for 'billingHandingCode'")
    @Column(name = "bill_handling_code", nullable = true, length = 255)
    private String billHandlingCode;

    @NotNull(message = "You must provide a value for 'created_when'")
    @Column(name = "created_when", nullable = true, columnDefinition = "DATETIME")
    private Timestamp createdWhen;

    @ManyToOne
    @JoinColumn(name = "customer_account_id", referencedColumnName = "id",  nullable = true)
    @JsonIgnore
    CustomerAccountSnapshotData customerAccountSnapshotData;

    @Column(name = "is_test", nullable = true, columnDefinition = "TINYINT(4)")
    private Byte isTest;

}
