package com.gci.cim.snapshot.entity.contact;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cim_contact")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactSnapshotData {

    @Id
    @NotNull(message = "You must provide a value for id")
    @Column(name = "id", nullable = false, length = 255)
    private String id;

    @Column(name = "contact_guid", nullable = true, length = 255, unique = true)
    private String contactId;

    @Column(name = "status", nullable = true, length = 255)
    private String status;

    @Column(name = "first_name", nullable = true, length = 255)
    private String firstName;

    @Column(name = "last_name", nullable = true, length = 255)
    private String lastName;

    @Column(name = "title", nullable = true, length = 255)
    private String title;

    @Transient
    private String preferredContactMethod;

    @Transient
    private String notificationEmail;

    @Transient
    private String notificationPhone;

    @Column(name = "created_when", nullable = true, columnDefinition = "DATETIME")
    private Date createdWhen;

    @Column(name = "customer_account_id", nullable = true, length = 255)
    private String customerAccountId;

    @Valid
    @OneToMany(mappedBy = "contactSnapshotData", fetch = FetchType.LAZY,  cascade = CascadeType.ALL , orphanRemoval = true)
    private List<ContactMethod> contactMethods = new ArrayList<ContactMethod>();

    @Valid
    @OneToMany(mappedBy = "contactSnapshotData", fetch = FetchType.EAGER, cascade = CascadeType.ALL , orphanRemoval = true)
    private List<ContactRole> contactRoles = new ArrayList<ContactRole>();

    @Column(name = "snapshot", nullable = true, columnDefinition = "JSON")
    private String snapshot;

    @Column(name = "change_timestamp", nullable = true, columnDefinition = "DATETIME")
    private Timestamp changeTimestamp;

    @Column(name = "is_test", nullable = true, columnDefinition = "TINYINT(4) DEFAULT '0'")
    private Byte isTest;

}