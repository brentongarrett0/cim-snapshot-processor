package com.gci.cim.snapshot.entity.customerAccount;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerAccount {

    @NotNull(message = "You must provide a value for snapshotType")
    private String snapshotType;

    @NotNull(message = "You must provide a changeTimestamp for snapshotType")
    private Timestamp changeTimestamp;

    @Valid
    @NotNull(message = "SnapshotData section must be included.")
    private CustomerAccountSnapshotData snapshotData;

    @NotNull(message = "'operationType' cannot be null. Please provide a value of 'Create' or 'Update'")
    @Pattern(regexp = "Create|Update|Delete", message = "Value for operationType must either be 'Create', 'Update', or 'Delete'")
    private String operationType;

}
