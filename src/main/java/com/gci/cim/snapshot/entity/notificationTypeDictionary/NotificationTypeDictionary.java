package com.gci.cim.snapshot.entity.notificationTypeDictionary;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationTypeDictionary {

    @NotNull(message = "You must provide a value for snapshotType")
    private String snapshotType;

    @NotNull(message = "You must provide a changeTimestamp for snapshotType")
    private Timestamp changeTimestamp;

    @Valid
    @NotNull(message = "'snapshotData' section must be provided")
    private NotificationTypeDictionarySnapshotData snapshotData;

    @NotNull(message = "You must provide a value for 'operationType'")
    @Pattern(regexp = "Create|Update|Delete", message = "Value for operationType must be 'Create', 'Update', or 'Delete'")
    private String operationType;

}