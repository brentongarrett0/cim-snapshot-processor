package com.gci.cim.snapshot.entity.customerAccount;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder(toBuilder = true)
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="cim_customer_account")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerAccountSnapshotData  {

    @Id
    @NotNull(message = "You must provide a value for id")
    @Column(name = "id", nullable = false, length = 255)
    private String id;

    @Column(name = "name", nullable = true, length = 255)
    private String name;

    @Column(name = "customer_account_number", nullable = true, length = 255)
    private String customerAccountNumber;

    @Column(name = "status", nullable = true, length = 255)
    private String status;

    @Column(name = "parent_category", nullable = true, length = 255)
    private String parentCustomerCategory;

    @Column(name = "primary_contact_id", nullable = true, length = 255)
    private String primaryContact;

    @Column(name = "category", nullable = true, length = 255)
    private String customerCategory;

    @Column(name = "created_when", nullable = true, columnDefinition = "DATETIME")
    private Timestamp createdWhen;

    @Valid
    @OneToMany(mappedBy = "customerAccountSnapshotData", fetch = FetchType.EAGER, cascade = CascadeType.ALL , orphanRemoval = true)
    private List<BillingAccount> billingAccounts = new ArrayList<BillingAccount>();

    @Valid
    @Transient
    private List<BillingContact> billingContacts;

    @JsonIgnore
    @Column(name = "snapshot", nullable = true, columnDefinition = "JSON")
    private String snapshot;

    @Column(name = "change_timestamp", nullable = true, columnDefinition = "DATETIME")
    private Timestamp changeTimestamp;

    @Column(name = "is_test", nullable = true, columnDefinition = "TINYINT(4)")
    private Byte isTest;

}
