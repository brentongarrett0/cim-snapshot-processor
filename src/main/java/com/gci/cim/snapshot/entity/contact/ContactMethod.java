package com.gci.cim.snapshot.entity.contact;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cim_contact_method", uniqueConstraints= {@UniqueConstraint( name = "netcracker_id_UNIQUE", columnNames={"id"})})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactMethod {

    @Id
    @Column(name = "id", nullable = false, length = 255)
    @NotNull(message = "You must provide a value for object type")
    private String id;

    @ManyToOne
    @JoinColumn(name = "contact_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    ContactSnapshotData contactSnapshotData;

    @Column(name = "type", nullable = true, length = 255)
    @NotNull(message = "You must provide a value for type")
    private String type;

    @Column(name = "phone_number", nullable = true, length = 255)
    private String phoneNumber;

    @Column(name = "email_address", nullable = true, length = 255)
    private String emailAddress;

    @Column(name = "created_when", nullable = true, columnDefinition = "DATETIME")
    private Date createdWhen;

    @Column(name = "primary_phone", nullable = true, columnDefinition = "TINYINT(4)")
    private Byte primaryPhone;

    @Column(name = "primary_email", nullable = true, columnDefinition = "TINYINT(4)")
    private Byte primaryEmail;

    @Column(name = "preferred", nullable = true, columnDefinition = "TINYINT(4) DEFAULT '0'")
    private Byte preferred;

    @Column(name = "is_test", nullable = true, columnDefinition = "TINYINT(4) DEFAULT '0'")
    private Byte isTest;

}