package com.gci.cim.snapshot.entity;
import com.gci.cim.snapshot.model.Violations;
import com.gci.cim.snapshot.converter.ViolationsConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name="cim_bad_snapshot")
public class BadSnapshot {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="id")
        private Long id;

        @Column(name="snapshot_type", nullable = true, length = 255)
        private String snapshotType;

        @Column(name="violation_category", nullable = true, length = 255)
        private String violationCategory;

        @Convert(converter = ViolationsConverter.class)
        @Column(name = "violations", nullable = true, columnDefinition = "JSON")
        private List<Violations> violations;

        @Column(name = "snapshot", nullable = true, columnDefinition = "TEXT")
        private String snapshot;

        @Column(name = "created_when", nullable = true, columnDefinition = "DATETIME")
        private Timestamp createdWhen;

        @Column(name = "is_test", nullable = true, columnDefinition = "tinyint(4)")
        private Byte isTest;

}



