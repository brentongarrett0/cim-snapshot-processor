package com.gci.cim.snapshot.entity.notificationProfile;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gci.cim.snapshot.converter.EmailChannelConverter;
import com.gci.cim.snapshot.converter.MobileChannelConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="cim_notification_profile", uniqueConstraints= {@UniqueConstraint( name = "id_UNIQUE", columnNames={"id"})})
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationProfileSnapshotData {

    @Id
    @NotNull(message = "You must provide a value for id")
    @Column(name = "id", nullable = false, length = 255)
    private String id;

    @Column(name = "contact_id", nullable = true, length = 255)
    private String contact;

    @Column(name = "notification_type_id", nullable = true, length = 255)
    private String notificationType;

    @Pattern(regexp = "Yes|No", message = "Value for emailChannel must either be 'Yes' or 'No'")
    @Convert(converter =  EmailChannelConverter.class)
    @Column(name = "email_channel", nullable = true, columnDefinition = "tinyint(4)")
    private String emailChannel;

    @Pattern(regexp = "Yes|No", message = "Value for operationType must either be 'Yes' or 'No'")
    @Convert(converter =  MobileChannelConverter.class)
    @Column(name = "mobile_channel", nullable = true, columnDefinition = "tinyint(4)")
    private String mobileChannel;

    @Column(name = "created_when", nullable = true, columnDefinition = "DATETIME")
    private Timestamp createdWhen;

    @Column(name = "snapshot", nullable = true, columnDefinition = "JSON")
    private String snapshot;

    @Column(name = "change_timestamp", nullable = true, columnDefinition = "DATETIME")
    private Timestamp changeTimestamp;

    @Column(name = "is_test", nullable = true, columnDefinition = "varchar(45)")
    private Byte isTest;

}
