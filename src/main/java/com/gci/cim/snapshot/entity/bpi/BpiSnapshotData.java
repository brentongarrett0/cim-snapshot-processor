package com.gci.cim.snapshot.entity.bpi;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gci.cim.snapshot.converter.IsLifelineConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;
import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="cim_bpi", uniqueConstraints= {@UniqueConstraint( name = "id_UNIQUE", columnNames={"id"})})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BpiSnapshotData {

    @Id
    @NotNull(message = "You must provide a value for id")
    @Column(name = "id", nullable = false, length = 255)
    private String id;

    @Column(name = "billing_account_id", nullable = true, length = 255)
    private String billingAccountId;

    @Column(name = "customer_account_id", nullable = true, length = 255)
    private String customerAccountId;

    @Transient
    private String parentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_bpi_id", referencedColumnName = "id",  nullable = true)
    @JsonIgnore
    private BpiSnapshotData bpiSnapshotData;

    @Valid
    @OneToMany(mappedBy = "bpiSnapshotData", fetch = FetchType.LAZY, cascade = CascadeType.ALL , orphanRemoval = true)
    List<BpiSnapshotData> childBpis;

    @Column(name = "service_id", nullable = true, length = 255)
    private String serviceId;

    @Column(name = "name", nullable = true, length = 255)
    private String name;

    @Column(name = "service_name", nullable = true, length = 255)
    private String serviceName;

    @Column(name = "sandvine_id", nullable = true, length = 255)
    private String sandvineId;

    @Column(name = "sandvine_plan_id", nullable = true, length = 255)
    private String sandvinePlanId;

    @Column(name = "status", nullable = true, length = 255)
    private String status;

    @Column(name = "product_category", nullable = true, length = 255)
    private String productCategory;

    @Column(name = "offering_id", nullable = true, length = 255)
    private String offeringId;

    @Column(name = "offering_name", nullable = true, length = 255)
    private String offeringName;

    @Column(name = "market_id", nullable = true, length = 255)
    private String marketId;

    @Column(name = "market_name", nullable = true, length = 255)
    private String marketName;

    @Column(name = "created_when", nullable = true, columnDefinition = "DATETIME")
    private Timestamp createdWhen;

    @Pattern(regexp = "[Yy]es|[Nn]o", message = "Value for 'isLifeline' must either be 'Yes' or 'No'")
    @Convert(converter =  IsLifelineConverter.class)
    @Column(name = "is_lifeline", nullable = true, columnDefinition = "tinyint(4)")
    private String isLifeline;

    @Column(name = "product_instance", nullable = true, length = 255)
    private String productInstance;

    @Column(name = "seachange_subscription_product", nullable = true, length = 255)
    private String seachangeSubscriptionProduct;

    @Column(name = "seachange_subscriber_id", nullable = true, length = 255)
    private String seachangeSubscriberId;

    @Column(name = "included_usage_amount_gb", nullable = true, length = 255)
    private String includedUsageAmountGb;

    @Column(name = "snapshot", nullable = true, columnDefinition = "JSON")
    private String snapshot;

    @Column(name = "change_timestamp", nullable = true, columnDefinition = "DATETIME")
    private Timestamp changeTimestamp;

    @Column(name = "is_test", nullable = true, columnDefinition = "tinyint(4)")
    private Byte isTest;

}
