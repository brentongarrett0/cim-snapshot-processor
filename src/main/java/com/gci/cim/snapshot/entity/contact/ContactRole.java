package com.gci.cim.snapshot.entity.contact;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cim_contact_role", uniqueConstraints= {@UniqueConstraint( name = "netcracker_id_UNIQUE", columnNames={"id"})})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactRole {

    @Id
    @Column(name = "id", nullable = false, length = 255)
    @NotNull(message = "You must provide a value for object type")
    private String id;

    @Column(name = "customer_account_id", nullable = true, length = 45)
    private String customerAccountId;

    @ManyToOne
    @JoinColumn(name = "contact_id", referencedColumnName = "id",  nullable = true)
    @JsonIgnore
    ContactSnapshotData contactSnapshotData;

    @Column(name = "role_name", nullable = true, length = 255)
    private String role;

    @Column(name = "created_when", nullable = true, columnDefinition = "DATETIME")
    private Timestamp createdWhen;

    @Column(name = "is_test", nullable = true, columnDefinition = "TINYINT(4)")
    private Byte isTest;

}