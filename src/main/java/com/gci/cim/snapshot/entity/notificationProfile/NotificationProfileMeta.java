package com.gci.cim.snapshot.entity.notificationProfile;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationProfileMeta {

    @NotNull(message = "You must provide a value for clientId")
    private String clientId;

    @NotNull(message = "You must provide a value for conversationId")
    private String conversationId;

    @NotNull(message = "You must provide a value for requestId")
    private String requestId;

    @NotNull(message = "You must provide a value for messageId")
    private String messageId;

    @NotNull(message = "You must provide a value for correlationId")
    private String correlationId;

}



